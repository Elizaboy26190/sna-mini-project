
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Paint;
import java.awt.ScrollPane;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

import edu.uci.ics.jung.algorithms.shortestpath.DistanceStatistics;
import edu.uci.ics.jung.algorithms.metrics.Metrics;
import edu.uci.ics.jung.algorithms.shortestpath.DijkstraDistance;

import java.io.BufferedReader;
import java.io.FileNotFoundException;

import edu.uci.ics.jung.algorithms.importance.BetweennessCentrality;
import edu.uci.ics.jung.algorithms.importance.MarkovCentrality;
import edu.uci.ics.jung.algorithms.importance.KStepMarkov;//needtoadd
import edu.uci.ics.jung.algorithms.importance.Ranking;//needtoadd
import edu.uci.ics.jung.algorithms.importance.RelativeAuthorityRanker;//needtoadd
import edu.uci.ics.jung.algorithms.importance.WeightedNIPaths;//needtoadd
import edu.uci.ics.jung.algorithms.scoring.DistanceCentralityScorer;//needtoadd
import edu.uci.ics.jung.algorithms.scoring.EigenvectorCentrality;//needtoadd
import edu.uci.ics.jung.algorithms.scoring.HITS;//needtoadd
import edu.uci.ics.jung.algorithms.scoring.VoltageScorer;//needtoadd
import edu.uci.ics.jung.algorithms.scoring.PageRank;//needtoadd

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSlider;
import javax.swing.JTextPane;
import javax.swing.JToggleButton;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.functors.ChainedTransformer;
import org.apache.commons.collections15.functors.ConstantTransformer;
import org.apache.commons.collections15.functors.MapTransformer;
import org.apache.commons.collections15.map.LazyMap;

import edu.uci.ics.jung.algorithms.cluster.EdgeBetweennessClusterer;
import edu.uci.ics.jung.algorithms.cluster.VoltageClusterer;
import edu.uci.ics.jung.algorithms.layout.AggregateLayout;
import edu.uci.ics.jung.algorithms.layout.CircleLayout;
//import edu.uci.ics.jung.algorithms.cluster.ClusterSet;
import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.FRLayout2;
import edu.uci.ics.jung.algorithms.layout.ISOMLayout;
import edu.uci.ics.jung.algorithms.layout.KKLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.layout.util.Relaxer;
import edu.uci.ics.jung.graph.DirectedGraph;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.algorithms.layout.TreeLayout;
import edu.uci.ics.jung.graph.SparseMultigraph;
import edu.uci.ics.jung.graph.util.Pair;
import edu.uci.ics.jung.visualization.Layer;
//import edu.uci.ics.jung.visualization.decorators.
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.EditingModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.picking.PickedState;
import edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position;
import edu.uci.ics.jung.visualization.transform.MutableTransformer;



/**
 *
 * @author Dr. Greg M. Bernstein
 */
public class CopyOfSimpleGraphView2 {
	static Map<Integer,HashMap<Integer, Integer>> commsMap = new HashMap<Integer,HashMap<Integer, Integer>>();
	static int currentVertex=1;
	static JTextPane text = new JTextPane();
	static JButton deleteButton = new JButton();
	static JButton updateScores = new JButton();
	static Transformer<GraphElements.MyVertex, Double> distances;
	static HashMap<GraphElements.MyVertex, Double> cliqueScores;
	static HashMap<GraphElements.MyVertex, Double> cliqueScores2;
	static DijkstraDistance<GraphElements.MyVertex, Number> dikstra;
	static HashMap<Integer, Integer> to;
	static JPanel east = new JPanel();
	static JPanel grid2 = new JPanel(new GridLayout(2,1));
	static JTextPane vertexDetail = new JTextPane();
	static TitledBorder vertexBorder = BorderFactory.createTitledBorder("Vertex Details");
	static Map<GraphElements.MyVertex, Double> cl;
	static Map<GraphElements.MyVertex,Number> tmp;
	static Map<Integer, Integer> recievedlist = new HashMap<Integer,Integer>();
	static Map<Integer, Set<String>> sentlist = new HashMap<Integer,Set<String>>();
	static  Dimension preferredGraphSize=new Dimension(1000,700);
	static List<String> updatedNames = new ArrayList<String>();
	static  BetweennessCentrality<GraphElements.MyVertex,GraphElements.MyEdge> ranker;
	static  HITS<GraphElements.MyVertex,GraphElements.MyEdge> HitsRanker;
	static  MarkovCentrality<GraphElements.MyVertex,GraphElements.MyEdge> markovRanker;
	static  PageRank<GraphElements.MyVertex,GraphElements.MyEdge> pageRanker;
	static DistanceStatistics bob = new DistanceStatistics();


	static GraphElements.MyVertex[] vertexArray = new GraphElements.MyVertex[184];
	public final static Color[] similarColors =
		{
			new Color(210, 134, 134),
			new Color(135, 137, 211),
			new Color(134, 206, 189),
			new Color(206, 176, 134),
			new Color(194, 204, 134),
			new Color(145, 214, 134),
			new Color(133, 178, 209),
			new Color(103, 148, 255),
			new Color(60, 220, 220),
			new Color(30, 250, 100)
		};
	
	//vv = new VisualizationViewer<Number,Number>(layout);
	static Map<GraphElements.MyVertex,Paint> vertexPaints = 
			LazyMap.<GraphElements.MyVertex,Paint>decorate(new HashMap<GraphElements.MyVertex,Paint>(),
					new ConstantTransformer(Color.white));
		static Map<GraphElements.MyEdge,Paint> edgePaints =
		LazyMap.<GraphElements.MyEdge,Paint>decorate(new HashMap<GraphElements.MyEdge,Paint>(),
				new ConstantTransformer(Color.blue));
	static CopyOfSimpleGraphView2 sgv = new CopyOfSimpleGraphView2();   
	private static final Object DEMOKEY = "DEMOKEY";
//	GraphElements.MyVertex x = new GraphElements.MyVertex("BOO");
	static AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge> layout = new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new ISOMLayout<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g));
	static AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge> FRlayout =  new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new FRLayout<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g));
	static AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge> KKlayout = new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new KKLayout<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g));
static VisualizationViewer<GraphElements.MyVertex, GraphElements.MyEdge> vv = 
            new VisualizationViewer<GraphElements.MyVertex, GraphElements.MyEdge>(layout);
	static Transformer blankLabelTransformer = new ChainedTransformer<String,String>(new Transformer[]{
            new ToStringLabeller<String>(),
            new Transformer<String,String>() {
            public String transform(String input) {
                return "<html><font color=\"black\">";
            }}});
	static Transformer blankLabelTransformer2 = new ChainedTransformer<String,String>(new Transformer[]{
              new ToStringLabeller<String>(),
              new Transformer<String,String>() {
              public String transform(String input) {
                  return "<html><font size=\"1\" color=\"black\">"+input;
              }}});
//	private static boolean edgesVisible = false;
	//private static boolean verticesVisible = true;
    static Graph<GraphElements.MyVertex, GraphElements.MyEdge> g;
    static NumberFormat df = new DecimalFormat("####0.000000");
   // static BetweennessCentrality ranker = new BetweennessCentrality(g,true,false);

    /** Creates a new instance of SimpleGraphView */
    public CopyOfSimpleGraphView2() {
    	
    	// This builds the graph
        // Layout<V, E>, VisualizationComponent<V,E>
        
        // Graph<V, E> where V is the type of the vertices and E is the type of the edges
        // Note showing the use of a SparseGraph rather than a SparseMultigraph
        g = new DirectedSparseMultigraph<GraphElements.MyVertex, GraphElements.MyEdge>();
        // Add some vertices. From above we defined these to be type Number.   
        
        updateNames();
        for (int i = 0;i<updatedNames.size();i++){
        	GraphElements.MyVertex vertex1 = new GraphElements.MyVertex(updatedNames.get(i));
        	
        	if(!duplicate(i)){
        		System.out.println("Already seen "+vertex1.getName());
        		g.addVertex(vertex1);
        	}
        	vertexArray[i] = vertex1;
        	
            // if(i < 20){
           // 	 System.out.println(vertex1.getName());
           //  }
        };
        try {
			FileReader fr = new FileReader("/Users/DodionTwo/Documents/Enron data abbreviated/EnronTimeFromRecieverTag(ToCcBcc).txt");
			BufferedReader bf = new BufferedReader (fr);
			int lineNum = 0;
			
			String tmpline = "";
			while (lineNum<125409){
				tmpline = bf.readLine();
				//String tmpLine = bf.readLine();
				//System.out.println(tmpline);
				String[] tmpArray = tmpline.split(" ");
				String dt = tmpArray[0];
				String senderOld = tmpArray[1];
				String recipientOld = tmpArray[2];
				String tag = tmpArray[3]; //0 = to, 1 = cc, 2 = bcc
				Integer sender = updatedNames.indexOf(updatedNames.get(Integer.parseInt(senderOld)));
				Integer recipient = updatedNames.indexOf(updatedNames.get(Integer.parseInt(recipientOld)));
				
				if (!(tag == "2") &&commsMap.containsKey(sender)&&(updatedNames.indexOf(updatedNames.get(sender))!= updatedNames.indexOf(updatedNames.get(recipient)))){
					if (lineNum==47128){
						System.out.println("Within me "+dt +updatedNames.get(sender)+" to "+updatedNames.get(recipient)+ "("+senderOld+","+recipientOld+")");
					
					}
					
					
					HashMap<Integer,Integer>to = commsMap.get(sender);
					if (sender==150){System.out.println("Before = "+to);}
					if (to.containsKey(recipient)){

					/*	if (sender==150){
						System.out.println("Already sent from "+sender+" to "+recipient+" before "+to.get(recipient));
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						}*/
						int previousTotal = to.get(recipient);
						to.put(recipient, previousTotal+1);
					}
					else{
						/*if (sender==150){
						System.out.println("Not sent from "+sender+" to "+recipient+" before");
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						}*/
						to.put(recipient,1);
					}
					if (sender==150){System.out.println("After = "+to);System.out.println();}
				//	ArrayList<HashMap<String,Integer>> commsTriple = new ArrayList<HashMap<String,Integer>>();
					commsMap.put(sender,to);
					
				//	System.out.println("seen before new "+sender+" = "+commsTriple);
				//	Thread.sleep(1000);
				}
				else if (!(tag == "2")&&!commsMap.containsKey(sender)){
					//if(Integer.parseInt(tag)==1){
				//	HashMap<Integer,Integer> tempList = new HashMap<Integer,Integer>();
				//	HashMap<Integer,Integer> tempList2 = new HashMap<Integer,Integer>();
					HashMap<Integer,Integer> to = new HashMap<Integer,Integer>();
						to.put(recipient,1);
						
						commsMap.put(sender, to);
						System.out.println("not seen before new "+sender+" = ");
						
					//}
				}
				if (!(tag == "2")&&recievedlist.containsKey(recipient)&&updatedNames.indexOf(updatedNames.get(sender))!= updatedNames.indexOf(updatedNames.get(recipient))){
					int prev = recievedlist.get(recipient);
					prev++;
					recievedlist.put(recipient, prev);
				}
				else if (!(tag == "2")&&updatedNames.indexOf(updatedNames.get(sender))!= updatedNames.indexOf(updatedNames.get(recipient))){
					int prev = 1;
					recievedlist.put(recipient, prev);
				}
				
				if (!(tag == "2")&&sentlist.containsKey(sender)&&updatedNames.indexOf(updatedNames.get(sender))!= updatedNames.indexOf(updatedNames.get(recipient))){
					Set<String> prev = sentlist.get(sender);
					prev.add(dt);
					sentlist.put(sender, prev);
				}
				else if (!(tag == "2")&&updatedNames.indexOf(updatedNames.get(sender))!= updatedNames.indexOf(updatedNames.get(recipient))){
					Set<String> prev = new HashSet<String>();
					prev.add(dt);
					sentlist.put(sender, prev);
				}
				
				lineNum++;
			}
			fr.close();
		} 
        catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}// catch (InterruptedException e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
		//}
        
      //need to add list  
	//	g.addEdge(edge1, vertexArray[Integer.parseInt(tmpArray[2])], vertexArray[Integer.parseInt(tmpArray[3])]);
/*try {
	Thread.sleep(10000);
} catch (InterruptedException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}*/
     //   System.out.println(commsMap.get("0"));
        for (Integer sender : commsMap.keySet()){
			HashMap<Integer, Integer> testEdges = commsMap.get(sender);
			for (Integer recipient : testEdges.keySet()){
				GraphElements.MyEdge edge1 = new GraphElements.MyEdge("Edge "+sender+"-"+recipient);
				edge1.setWeight(testEdges.get(recipient));
				g.addEdge(edge1, vertexArray[sender], vertexArray[recipient]);
			}
			}
   
        FileWriter fr2;
        try {
        	fr2 = new FileWriter("/Users/DodionTwo/Documents/Enron data abbreviated/EnronLDCForHierarchy.txt", false);
    		fr2.flush();
    		System.out.println(commsMap);
    		Collection<GraphElements.MyVertex> vertexColl = g.getVertices();
    		ArrayList<GraphElements.MyVertex> vertexList = new ArrayList<GraphElements.MyVertex>(vertexColl);
    		for (int i = 0;i <vertexList.size() ; i++){
    			String sender = vertexList.get(i).getName();
    			//System.out.println()
    			if (commsMap.containsKey(updatedNames.indexOf(sender))){
    				HashMap<Integer, Integer> recipients = commsMap.get(updatedNames.indexOf(sender));
    				System.out.println("Recipients for "+sender+" "+ updatedNames.indexOf(sender)+" = "+recipients);
    				for (int j =0; j < vertexList.size();j++){
    					String recipient = vertexList.get(j).getName();
    					if (recipients.containsKey(updatedNames.indexOf(recipient))){
    					//	System.out.println("Link between "+sender+" and "+recipient+" of weight "+recipients.get(j));
    					fr2.write(sender.replace(" ", "_")+" "+recipient.replace(" ", "_")+" "+recipients.get(updatedNames.indexOf(recipient))+"\n");
    					}
    					else{
    					//	System.out.println("No Link between "+sender+" and "+recipient);
    						
        						fr2.write(sender.replace(" ", "_")+" "+recipient.replace(" ", "_")+" 0"+"\n");
    						
    					}
    				}
    			}
    			else {
    				
    					System.out.println("No messages sent from "+sender);
    					for (int  j = 0; j < vertexList.size();j++){
    						String recipient = vertexList.get(j).getName();
        						fr2.write(sender.replace(" ", "_")+" "+recipient.replace(" ", "_")+" 0"+"\n");
    						
    					}
    				
    				
    			}
    			}
    	fr2.close();	
        }
        catch (IOException e1) {
			e1.printStackTrace();
		}
        
		
        
       
    }
    
    private static boolean duplicate(int i) {
		if (i==26||	i==31||	i==54||	i==58||	i==66||	i==68||	i==72||	i==81||	i==85||	i==100||	i==111|i==112||i==114||i==121||i==124||i==133|| i==135|| i==136|| i==137|| i==138|| i==139|| i==140|| i==141|| i==143|| i==145|| i==154|| i==159|| i==163|| i==165|| i==167|| i==168|| i==172|| i==173|| i==178|| i==180|| i==181|| i==182){
		return true;}
		else return false;
	}

	private void updateNames() {
    	updatedNames.add("Albert Meyers");
		updatedNames.add("Thomas Martin");
		updatedNames.add("Andrea Ring");
		updatedNames.add("Andrew Lewis");
		updatedNames.add("Andy Zipper");
		updatedNames.add("Jeffrey Shankman");
		updatedNames.add("Barry Tycholiz");
		updatedNames.add("Benjamin Rogers");
		updatedNames.add("Bill Rapp");
		updatedNames.add("Bill Williams");
		updatedNames.add("Bradley Mckay");
		updatedNames.add("Brenda Whitehead");
		updatedNames.add("Richard Sanders");
		updatedNames.add("Cara Semperger");
		updatedNames.add("Daron Giron");
		updatedNames.add("Charles Weldon");
		updatedNames.add("Chris Dorland");
		updatedNames.add("Chris Germany");
		updatedNames.add("Clint Dean");
		updatedNames.add("Cooper Richey");
		updatedNames.add("Craig Dean");
		updatedNames.add("Dana Davis");
		updatedNames.add("Dan Hyvl");
		updatedNames.add("Danny McCarty");
		updatedNames.add("Daren Farmer");
		updatedNames.add("Darrell Schoolcraft");
		updatedNames.add("Daron Giron");
		updatedNames.add("David Delainey");
		updatedNames.add("Susan Bailey");
		updatedNames.add("Debra Perlingiere");
		updatedNames.add("Diana Scholtes");
		updatedNames.add("Thomas Martin");
		updatedNames.add("Don Baughman");
		updatedNames.add("Drew Fossum");
		updatedNames.add("James Steffes");
		updatedNames.add("Paul Thomas");
		updatedNames.add("Dutch Quigley");
		updatedNames.add("Mark Haedicke");
		updatedNames.add("Elizabeth Sager");
		updatedNames.add("Eric Bass");
		updatedNames.add("Eric Saibi");
		updatedNames.add("Errol McLaughlin");
		updatedNames.add("Mark Taylor");
		updatedNames.add("Sandra Brawner");
		updatedNames.add("Larry Campbell");
		updatedNames.add("Peter Keavey");
		updatedNames.add("Fletcher Sturm");
		updatedNames.add("Frank Ermis");
		updatedNames.add("Geir Solberg");
		updatedNames.add("Geoffery Storey");
		updatedNames.add("Gerald Nemec");
		updatedNames.add("Greg Whalley");
		updatedNames.add("Gretel Smith");
		updatedNames.add("Harpreet Arora");
		updatedNames.add("Andrew Lewis");
		updatedNames.add("Holden Salisbury");
		updatedNames.add("Hunter Shively");
		updatedNames.add("James Derrick");
		updatedNames.add("James Steffes");
		updatedNames.add("Jane Tholt");
		updatedNames.add("Jason Williams");
		updatedNames.add("Jason Wolfe");
		updatedNames.add("Jay Reitmeyer");
		updatedNames.add("Jeff Dasovich");
		updatedNames.add("Jeff King");
		updatedNames.add("John Hodge");
		updatedNames.add("Jeffrey Shankman");
		updatedNames.add("Jeffery Skilling");
		updatedNames.add("Daren Farmer");
		updatedNames.add("Stephen Harris");
		updatedNames.add("Jim Schwieger");
		updatedNames.add("Vince Kaminski");
		updatedNames.add("Vince Kaminski");
		updatedNames.add("Steven Kean");
		updatedNames.add("Joannie Williamson");
		updatedNames.add("Joe Parks");
		updatedNames.add("Joe Quenet");
		updatedNames.add("Joe Stepenovitch");
		updatedNames.add("John Arnold");
		updatedNames.add("John Forney");
		updatedNames.add("John Griffith");
		updatedNames.add("John Hodge");
		updatedNames.add("John Lavorato");
		updatedNames.add("John Zufferli");
		updatedNames.add("Jonathan Mckay");
		updatedNames.add("Fletcher Sturm");
		updatedNames.add("Juan Hernandez");
		updatedNames.add("Judy Hernandez");
		updatedNames.add("Judy Townsend");
		updatedNames.add("Philip Allen");
		updatedNames.add("Kam Keiser");
		updatedNames.add("Kate Symes");
		updatedNames.add("Kay Mann");
		updatedNames.add("Keith Holst");
		updatedNames.add("Kenneth Lay");
		updatedNames.add("Kevin Hyatt");
		updatedNames.add("Kevin Presto");
		updatedNames.add("Kevin Ruscitti");
		updatedNames.add("Kimberly Watson");
		updatedNames.add("Kim Ward");
		updatedNames.add("Larry Campbell");
		updatedNames.add("Lawrence May");
		updatedNames.add("Randall Gay");
		updatedNames.add("Lindy Donoho");
		updatedNames.add("Lisa Gang");
		updatedNames.add("Liz Taylor");
		updatedNames.add("Patrice Mims");
		updatedNames.add("Louise Kitchen");
		updatedNames.add("Lynn Blair");
		updatedNames.add("Margaret Carson");
		updatedNames.add("Marie Heard");
		updatedNames.add("Mark Haedicke");
		updatedNames.add("Mark Haedicke");
		updatedNames.add("Mark McConnell");
		updatedNames.add("Mark Taylor");
		updatedNames.add("Mark Whitt");
		updatedNames.add("Martin Cuilla");
		updatedNames.add("Mary Fischer");
		updatedNames.add("Matthew Lenhart");
		updatedNames.add("Matthew Motley");
		updatedNames.add("Matt Smith");
		updatedNames.add("John Forney");
		updatedNames.add("Michelle Lokay");
		updatedNames.add("Michelle Cash");
		updatedNames.add("Michelle Lokay");
		updatedNames.add("Mike Carson");
		updatedNames.add("Michael Grigsby");
		updatedNames.add("Michael Maggi");
		updatedNames.add("Mike McConnell");
		updatedNames.add("Mike Swerzbin");
		updatedNames.add("Phillip Love");
		updatedNames.add("Monika Causholli");
		updatedNames.add("Monique Sanchez");
		updatedNames.add("Kevin Presto");
		updatedNames.add("Susan Scott");
		updatedNames.add("Matt Smith");
		updatedNames.add("Jane Tholt");
		updatedNames.add("Patrice Mims");
		updatedNames.add("Paul Thomas");
		updatedNames.add("Peter Keavey");
		updatedNames.add("Philip Allen");
		updatedNames.add("Phillip Love");
		updatedNames.add("Phillip Platter");
		updatedNames.add("Randall Gay");
		updatedNames.add("Richard Ring");
		updatedNames.add("Richard Sanders");
		updatedNames.add("Richard Shapiro");
		updatedNames.add("Rick Buy");
		updatedNames.add("Robert Badeer");
		updatedNames.add("Robert Benson");
		updatedNames.add("Robert Gay");
		updatedNames.add("Rod Hayslett");
		updatedNames.add("Ryan Slinger");
		updatedNames.add("Sally Beck");
		updatedNames.add("Sandra Brawner");
		updatedNames.add("Sara Shackleton");
		updatedNames.add("Scott Hendrickson");
		updatedNames.add("Scott Neal");
		updatedNames.add("Shelley Corman");
		updatedNames.add("Hunter Shively");
		updatedNames.add("Stacy Dickson");
		updatedNames.add("Stanley Horton");
		updatedNames.add("Stephanie Panus");
		updatedNames.add("Steven Kean");
		updatedNames.add("Steven South");
		updatedNames.add("Susan Bailey");
		updatedNames.add("Susan Pereira");
		updatedNames.add("Susan Scott");
		updatedNames.add("Kim Ward");
		updatedNames.add("Tana Jones");
		updatedNames.add("Teb Lokey");
		updatedNames.add("Theresa Staab");
		updatedNames.add("John Hodge");
		updatedNames.add("Thomas Martin");
		updatedNames.add("Paul Lucci");
		updatedNames.add("Tom Donohoe");
		updatedNames.add("Tori Kuykendall");
		updatedNames.add("Tracy Geaccone");
		updatedNames.add("Vince Kaminski");
		updatedNames.add("Vladi Pimenov");
		updatedNames.add("Charles Weldon");
		updatedNames.add("David Delainey");
		updatedNames.add("Susan Pereira");
		updatedNames.add("Stacey White");		
	}

	private int[] rand(int indices) {
    	int min = 1;
    	Random random = new Random();
    	int value1 = random.nextInt(indices - min + 1) + min;
    	int value2 = random.nextInt(indices - min + 1) + min;
    	while(value2==value1){
    		value2 = random.nextInt(indices - min + 1) + min;
    	}
		// TODO Auto-generated method stub
		int[] values = new int[2];
		values[0] = value1;
		values[1] = value2;
		return values;
	}

	/**
     * @param args the command line arguments
	 * @throws InterruptedException 
     */
    public static void main(String[] args) throws InterruptedException {
      
        // Setup up a new vertex to paint transformer...
        Transformer<Number,Paint> vertexPaint = new Transformer<Number,Paint>() {
            public Paint transform(Number i) {
                return Color.GREEN;
            }
        }; 
        Transformer<GraphElements.MyVertex, Shape> vertexSize = new Transformer<GraphElements.MyVertex,Shape>(){
        //    private AbstractRanker ranker;

			public Shape transform(GraphElements.MyVertex i){
                Ellipse2D circle = new Ellipse2D.Double(-10, -10, 20, 20);
                Ellipse2D circlemini = new Ellipse2D.Double(-2,-2,3,3);
               // System.out.println("Rank score for "+vertexArray[1].getName()+" = "+ranker.getVertexRankScore(vertexArray[1]));
                // in this case, the vertex is twice as large
                if(g.degree(i)==0) return circlemini;
                
                else return AffineTransform.getScaleInstance(g.degree(i)/20, g.degree(i)/20).createTransformedShape(circle);
            }
        };
       // vv.getRenderContext().setVertexShapeTransformer(vertexSize);
        // Set up a new stroke Transformer for the edges
        float dash[] = {5.0f};
        final Stroke edgeStroke = new BasicStroke(0.5f, BasicStroke.CAP_BUTT,
             BasicStroke.JOIN_MITER, 5.0f, dash, 0.0f);
        // vv.getRenderContext().setVertexFillPaintTransformer(vertexPaint);
      //  vv.getRenderContext().setEdgeStrokeTransformer(edgeStrokeTransformer);
        vv.getRenderContext().setVertexLabelTransformer(blankLabelTransformer2);
        vv.getRenderer().getVertexLabelRenderer().setPosition(Position.CNTR);        
        
        final JFrame frame = new JFrame("Simple Graph View 2");
        frame.setSize(new Dimension(300,300));
       //frame.
        JFrame frameSide = new JFrame("Simple Graph Vi");
        JPanel pnlButton = new JPanel();
        BronKerboschCliqueFinder<GraphElements.MyVertex,GraphElements.MyEdge> clique = new BronKerboschCliqueFinder<GraphElements.MyVertex, GraphElements.MyEdge>(g);
        Collection<Set<GraphElements.MyVertex>> cliques = clique.getAllMaximalCliques();
        cl = Metrics.clusteringCoefficients(g);
        dikstra = new DijkstraDistance(g);
        distances = bob.averageDistances(g);
        ArrayList<Set<GraphElements.MyVertex>> cliqueList = new ArrayList<Set<GraphElements.MyVertex>>();
        cliqueList.addAll(cliques);
       // System.out.println(cliques);
        cliqueScores = new HashMap<GraphElements.MyVertex, Double>();
        Collection<GraphElements.MyVertex> g1 = g.getVertices();
        ArrayList<GraphElements.MyVertex> vertexList = new ArrayList<GraphElements.MyVertex>();
        vertexList.addAll(g1);
        for (int i = 0; i < vertexList.size();i++){
        	double count3= 0;
        //	System.out.print("row for "+vertexList.get(i).getName()+" = ");
        	for (int j = 0; j <cliques.size()-1;j++){
        		if (cliqueList.get(j).contains(vertexList.get(i))){
        			double temp = cliqueList.get(j).size();
            		double poww = Math.pow(2,(temp-1));
        			count3 = count3 + poww;
        		}
        		//System.out.print(cliqueList.get(j).contains(vertexList.get(i))+", ");
        	}
        	if (cliqueList.get(cliqueList.size()-1).contains(vertexList.get(i))){
        		double temp = cliqueList.get(cliqueList.size()-1).size();
        		double poww = Math.pow(2,(temp-1));
    			count3 = count3 + poww;
   		}
        //	System.out.println(cliqueList.get(cliqueList.size()-1).contains(vertexList.get(i)));
        //	System.out.println("Total for "+vertexList.get(i)+" = "+count);
        	cliqueScores.put(vertexList.get(i), count3);
        }
        
Collection<Set<GraphElements.MyVertex>> cliques2 = clique.getAllMaximalCliques();
        
        ArrayList<Set<GraphElements.MyVertex>> cliqueList2 = new ArrayList<Set<GraphElements.MyVertex>>();
        cliqueList2.addAll(cliques2);
       // System.out.println(cliques);
        cliqueScores2 = new HashMap<GraphElements.MyVertex, Double>();
        for (int i = 0; i < vertexList.size();i++){
        	double count= 0;
        //	System.out.print("row for "+vertexList.get(i).getName()+" = ");
        	for (int j = 0; j <cliques2.size()-1;j++){
        		if (cliqueList2.get(j).contains(vertexList.get(i))){
        			count++;
        		}
        		//System.out.print(cliqueList.get(j).contains(vertexList.get(i))+", ");
        	}
        	if (cliqueList2.get(cliqueList2.size()-1).contains(vertexList.get(i))){
        		count++;
   		}
        //	System.out.println(cliqueList.get(cliqueList.size()-1).contains(vertexList.get(i)));
        //	System.out.println("Total for "+vertexList.get(i)+" = "+count);
        	cliqueScores2.put(vertexList.get(i), count);
        }
       frame.setBackground(Color.WHITE);
       JMenuBar menuBar = new JMenuBar();
       JMenu mainMenu = new JMenu("File");
       JMenu viewMenu = new JMenu("Change view");
       JMenu display = new JMenu("Display Settings");
       JMenu toggle = new JMenu("Toggle Labels");
       JCheckBoxMenuItem toggleEdge = new JCheckBoxMenuItem("Edges");
       JCheckBoxMenuItem toggleVertices = new JCheckBoxMenuItem("Vertices", true);
       toggle.add(toggleEdge);
       toggleEdge.getState();
       
       ActionListener printCommandListener = new ActionListener() {
     	  
 	      public void actionPerformed(ActionEvent event) {
 	    	 
 	    	  Collection<GraphElements.MyVertex> allVertices = g.getVertices();
 	    	 Comparator<GraphElements.MyVertex> comparator = new Comparator<GraphElements.MyVertex>() {
 	    	    public int compare(GraphElements.MyVertex c1, GraphElements.MyVertex c2) {
 	    	        return c1.getName().compareTo(c2.getName()); // use your logic
 	    	    }
 	    	};
 	    	  ArrayList<GraphElements.MyVertex> listVertices = new ArrayList<GraphElements.MyVertex>();
 	    	  listVertices.addAll(allVertices);
 	    	  Collections.sort(listVertices, comparator);
 	    	  Map<GraphElements.MyVertex, Double> cl = Metrics.clusteringCoefficients(g);
 	    		DijkstraDistance<GraphElements.MyVertex, Number> dikstra = new DijkstraDistance(g);

 	         Transformer<GraphElements.MyVertex, Double> distances = bob.averageDistances(g);

 	    	  for (int p = 0; p < listVertices.size();p++){
 	    		  GraphElements.MyVertex v = listVertices.get(p);
 	    		  Map<GraphElements.MyVertex,Number> tmp = dikstra.getDistanceMap(v);
 	    		//  System.out.println(tmp);
 	    		  HashMap<Integer, Integer> to = commsMap.get(updatedNames.indexOf(v.getName()));
 	    		  int sum = 0;
 	    		  if  (to!=null){Collection<Integer> toRecipients = to.values();
 	    		  ArrayList<Integer> rec = new ArrayList<Integer>();
 	    		  rec.addAll(toRecipients);
 	    		  for (int l = 0; l < toRecipients.size();l++){
 	    			  sum = sum + rec.get(l);
 	    		  }
 	    		  }
 	    		  int sent = 0;
 	    		  if (!(sentlist.get(updatedNames.indexOf(v.getName()))==null)){
 	    			  sent = sentlist.get(updatedNames.indexOf(v.getName())).size();
 	    		  }
 	    		 Double dist = distances.transform(v);
 	    		 String diststr = "NaN";
					if (Double.isNaN(dist)){
						
					}
					else{
						dist = dist+1;
						diststr = df.format(dist);
					}
					Integer recTotal = recievedlist.get(updatedNames.indexOf(v.getName()));
					  if (recTotal == null){
						  recTotal = 0;
					  }
					  double deg = g.degree(v);
 	    		  System.out.println(v.getName().replace(" ","_")+", total_sentList= "+sent+", total_sent= "+sum+", total_recieved= "+recTotal+", degree(%)= "+(deg/147)+", betweenness= "+ranker.getVertexRankScore(v)+", PageRank= "+pageRanker.getVertexScore(v)+", Markov= "+markovRanker.getVertexRankScore(v)+", HITS_authority= "+HitsRanker.getVertexScore(v).authority+", HITS_hub= "+HitsRanker.getVertexScore(v).hub+", cliques_weighted= "+cliqueScores.get(v)+", cliques= "+cliqueScores2.get(v)+", avgDistance= "+(diststr)+", clusteringCoeff= "+cl.get(v));
 	    	  }
 	      }
 	    };
 	    
 	   ActionListener printFileListener = new ActionListener() {
      	  
  	      public void actionPerformed(ActionEvent event) {
  	    	  System.out.println("BOO PRINTED");
  	      }
  	    };
        
       
 	   JMenu a = new JMenu("Print values");
       JMenuItem b = new JMenuItem("Console");
       JMenuItem c = new JMenuItem("To File");
       b.addActionListener(printCommandListener);
       c.addActionListener(printFileListener);
       mainMenu.add(a);
       a.add(b);
       a.add(c);
       
       
       JRadioButtonMenuItem birdButton = new JRadioButtonMenuItem("birdString");
       birdButton.setMnemonic(KeyEvent.VK_B);
       birdButton.setActionCommand("birdString");
       birdButton.setSelected(true);

       JRadioButtonMenuItem catButton = new JRadioButtonMenuItem("catString");
       catButton.setMnemonic(KeyEvent.VK_C);
       catButton.setActionCommand("catString");

       JRadioButtonMenuItem dogButton = new JRadioButtonMenuItem("dogString");
       dogButton.setMnemonic(KeyEvent.VK_D);
       dogButton.setActionCommand("dogString");
       

       JRadioButtonMenuItem rabbitButton = new JRadioButtonMenuItem("rabbitString");
       rabbitButton.setMnemonic(KeyEvent.VK_R);
       rabbitButton.setActionCommand("rabbitString");

       JRadioButtonMenuItem pigButton = new JRadioButtonMenuItem("pigString");
       pigButton.setMnemonic(KeyEvent.VK_P);
       pigButton.setActionCommand("pigString");

       //Group the radio buttons.
       ButtonGroup group = new ButtonGroup();
       group.add(birdButton);
       group.add(catButton);
       group.add(dogButton);
       group.add(rabbitButton);
       group.add(pigButton);
		final JToggleButton groupVertices = new JToggleButton("Group Clusters");

       final JSlider edgeBetweennessSlider = new JSlider(JSlider.HORIZONTAL);
       edgeBetweennessSlider.setBackground(Color.WHITE);
		edgeBetweennessSlider.setPreferredSize(new Dimension(280, 50));
		edgeBetweennessSlider.setPaintTicks(true);
		edgeBetweennessSlider.setMaximum(g.getEdgeCount());
		edgeBetweennessSlider.setMinimum(0);
		edgeBetweennessSlider.setValue(0);
		System.out.println("Tick - "+g.getEdgeCount()/10);
		edgeBetweennessSlider.setMajorTickSpacing(g.getEdgeCount()/10 - g.getEdgeCount()/10%10);
		edgeBetweennessSlider.setPaintLabels(true);
		edgeBetweennessSlider.setPaintTicks(true);
		final JPanel eastControls = new JPanel();
		eastControls.setOpaque(true);
		eastControls.setLayout(new BoxLayout(eastControls, BoxLayout.Y_AXIS));
		eastControls.add(Box.createVerticalGlue());
		eastControls.add(edgeBetweennessSlider);
		final String COMMANDSTRING = "Edges removed for clusters: ";

		final String eastSize = COMMANDSTRING + edgeBetweennessSlider.getValue();

		final TitledBorder sliderBorder = BorderFactory.createTitledBorder(eastSize);
		eastControls.setBorder(sliderBorder);
		//eastControls.add(eastSize);
		eastControls.add(Box.createVerticalGlue());
		JButton scramble = new JButton("Restart");
		scramble.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Layout layout = vv.getGraphLayout();
				layout.setSize(preferredGraphSize);
				layout.initialize();
				Relaxer relaxer = vv.getModel().getRelaxer();
				if(relaxer != null) {
					relaxer.stop();
					relaxer.prerelax();
					relaxer.relax();
				}
			}

		});
		JPanel south = new JPanel();
		JPanel grid = new JPanel(new GridLayout(3,1));
		grid.add(scramble);
		grid.add(groupVertices);
		updateScores.setText("Update Scores");
		updateScores.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				System.out.println("Scores updated");
				Set<GraphElements.MyVertex> setV = new HashSet<GraphElements.MyVertex>();
			       setV.addAll(g.getVertices());
				 ranker = new BetweennessCentrality<GraphElements.MyVertex,GraphElements.MyEdge>(g,true,false);
			       markovRanker = new MarkovCentrality<GraphElements.MyVertex,GraphElements.MyEdge>((DirectedGraph<GraphElements.MyVertex, GraphElements.MyEdge>) g,setV);
			       markovRanker.setRemoveRankScoresOnFinalize(false);
			       ranker.setRemoveRankScoresOnFinalize(false);
			       ranker.evaluate();
			       pageRanker = new PageRank<GraphElements.MyVertex, GraphElements.MyEdge>(g,0.2);
			       pageRanker.initialize();
			       pageRanker.evaluate();
			       HitsRanker = new HITS<GraphElements.MyVertex,GraphElements.MyEdge>(g);
			       HitsRanker.initialize();
			       HitsRanker.evaluate();
			       markovRanker.evaluate();
			       BronKerboschCliqueFinder<GraphElements.MyVertex,GraphElements.MyEdge> clique = new BronKerboschCliqueFinder<GraphElements.MyVertex, GraphElements.MyEdge>(g);
			        Collection<Set<GraphElements.MyVertex>> cliques = clique.getAllMaximalCliques();
			        
			        ArrayList<Set<GraphElements.MyVertex>> cliqueList = new ArrayList<Set<GraphElements.MyVertex>>();
			        cliqueList.addAll(cliques);
			       // System.out.println(cliques);
			        cliqueScores = new HashMap<GraphElements.MyVertex, Double>();
			        Collection<GraphElements.MyVertex> g1 = g.getVertices();
			        ArrayList<GraphElements.MyVertex> vertexList = new ArrayList<GraphElements.MyVertex>();
			        vertexList.addAll(g1);
			        for (int i = 0; i < vertexList.size();i++){
			        	double countp= 0;
			        //	System.out.print("row for "+vertexList.get(i).getName()+" = ");
			        	for (int j = 0; j <cliques.size()-1;j++){
			        		if (cliqueList.get(j).contains(vertexList.get(i))){
			        			double temp = cliqueList.get(j).size();
			            		double poww = Math.pow(2,(temp-1));
			        			System.out.print("size = "+temp+"Previous p = "+countp);
			        			if (temp == 1){
			        				System.out.println(cliqueList);
			        				try {
										Thread.sleep(1000);
									} catch (InterruptedException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
			        			}
			        			countp = countp + poww;
			        			System.out.println(", new p = "+countp);
			        		}
			        		//System.out.print(cliqueList.get(j).contains(vertexList.get(i))+", ");
			        	}
			        	if (cliqueList.get(cliqueList.size()-1).contains(vertexList.get(i))){
			        		double temp = cliqueList.get(cliqueList.size()-1).size();
			        		double poww = Math.pow(2,(temp-1));
			        		System.out.print("size = "+temp+", Previous p = "+countp);
		        			countp = countp + poww;
		        			System.out.println(", new p = "+countp);
			   		}
			        //	System.out.println(cliqueList.get(cliqueList.size()-1).contains(vertexList.get(i)));
			        //	System.out.println("Total for "+vertexList.get(i)+" = "+count);
			        	cliqueScores.put(vertexList.get(i), countp);
			        }
			        
			Collection<Set<GraphElements.MyVertex>> cliques2 = clique.getAllMaximalCliques();
			        
			        ArrayList<Set<GraphElements.MyVertex>> cliqueList2 = new ArrayList<Set<GraphElements.MyVertex>>();
			        cliqueList2.addAll(cliques2);
			       // System.out.println(cliques);
			        cliqueScores2 = new HashMap<GraphElements.MyVertex, Double>();
			        for (int i = 0; i < vertexList.size();i++){
			        	double count= 0;
			        //	System.out.print("row for "+vertexList.get(i).getName()+" = ");
			        	for (int j = 0; j <cliques2.size()-1;j++){
			        		if (cliqueList2.get(j).contains(vertexList.get(i))){
			        			count++;
			        		}
			        		//System.out.print(cliqueList.get(j).contains(vertexList.get(i))+", ");
			        	}
			        	if (cliqueList2.get(cliqueList2.size()-1).contains(vertexList.get(i))){
			        		count++;
			   		}
			        //	System.out.println(cliqueList.get(cliqueList.size()-1).contains(vertexList.get(i)));
			        //	System.out.println("Total for "+vertexList.get(i)+" = "+count);
			        	cliqueScores2.put(vertexList.get(i), count);
			        }
		        cl = Metrics.clusteringCoefficients(g);
		        	dikstra = new DijkstraDistance(g);
		        	distances = bob.averageDistances(g);
			}
		});
		grid.add(updateScores);
		south.add(grid);
		south.add(eastControls);
		text.setText("Initialising");
		text.setMaximumSize(new Dimension(200,220));
		
		vertexDetail.setVisible(false);
		east.add(text);
		east.add(vertexDetail);
		frame.add(east,BorderLayout.EAST);
		east.setBackground(Color.white);
		east.setBorder(vertexBorder);
		JButton nextSlide = new JButton();
		JButton previousSlide = new JButton();
		nextSlide.setText("Next Vertex");
		nextSlide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Set<GraphElements.MyVertex> selected = vv.getPickedVertexState().getPicked();
				int total = selected.size();
				currentVertex = (currentVertex+1)%total;
				if (currentVertex ==0){
					currentVertex = total;
				}
				//System.out.println(currentVertex);
				ArrayList<GraphElements.MyVertex> vertices = new ArrayList<GraphElements.MyVertex>(selected);
				
				updatePanel(vertices, currentVertex);
				}

		});
		previousSlide.setText("Previous Vertex");
		previousSlide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Set<GraphElements.MyVertex> selected = vv.getPickedVertexState().getPicked();
				int total = selected.size();
				currentVertex = (currentVertex + total -1)%total;
				if (currentVertex ==0){
					currentVertex = total;
				}
			//	System.out.println(currentVertex);
				ArrayList<GraphElements.MyVertex> vertices = new ArrayList<GraphElements.MyVertex>(selected);

				updatePanel(vertices,currentVertex);
			}

		});
		
		grid2.add(nextSlide);
		grid2.add(previousSlide);
		grid2.setVisible(false);
		
		east.add(grid2,BorderLayout.SOUTH);
		
		frame.add(south, BorderLayout.SOUTH);
		groupVertices.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
					try {
						clusterAndRecolor(layout, edgeBetweennessSlider.getValue(), 
								similarColors, e.getStateChange() == ItemEvent.SELECTED);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
					vv.repaint();
			}});


		clusterAndRecolor(layout, 0, similarColors, groupVertices.isSelected());

		edgeBetweennessSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				if (!source.getValueIsAdjusting()) {
					int numEdgesToRemove = source.getValue();
					try {
						clusterAndRecolor(layout, numEdgesToRemove, similarColors,
								groupVertices.isSelected());
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
					sliderBorder.setTitle(
						COMMANDSTRING + edgeBetweennessSlider.getValue());
					eastControls.repaint();
					vv.validate();
					vv.repaint();
				}
			}
		});
       
       ActionListener viewMenuListener = new ActionListener() {
     	  
 	      public void actionPerformed(ActionEvent event) {
 	    	//  System.out.println(event.getActionCommand());
 	    	  if (event.getActionCommand()=="catString"){
					 //layout = new FRLayout(sgv.g);	
					// vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT);
					 layout = new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new FRLayout<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g));
					 layout.setSize(preferredGraphSize);
					 vv.setGraphLayout(layout);
					
					 try {
							clusterAndRecolor(layout, edgeBetweennessSlider.getValue(), 
										similarColors, groupVertices.isSelected());
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							

							e.printStackTrace();
						}
					
					vv.updateUI();
 	    	  }
 	    	  if (event.getActionCommand() == "rabbitString"){
					// layout = new ISOMLayout(sgv.g);	
					// vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT);
					layout = new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new ISOMLayout<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g)); 
 	    		  layout.setSize(preferredGraphSize);
					vv.setGraphLayout(layout);
 	    		 try {
						clusterAndRecolor(layout, edgeBetweennessSlider.getValue(), 
									similarColors, groupVertices.isSelected());
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block

						e.printStackTrace();
					}
				 
				vv.updateUI();
 	    	  }
 	    	 if (event.getActionCommand() == "dogString"){
					// layout = new ISOMLayout(sgv.g);	
					// vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT);
 	    		layout = new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new KKLayout<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g)); 
				layout.setSize(preferredGraphSize);	
 	    		vv.setGraphLayout(layout);
					 try {
						clusterAndRecolor(layout, edgeBetweennessSlider.getValue(), 
									similarColors, groupVertices.isSelected());
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block

						e.printStackTrace();
					}
					vv.updateUI();
	    	  }
 	    	if (event.getActionCommand() == "pigString"){
				// layout = new ISOMLayout(sgv.g);	
				// vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT);
	    		layout = new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new FRLayout2<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g)); 
				layout.setSize(preferredGraphSize);
	    		vv.setGraphLayout(layout);
				 try {
					clusterAndRecolor(layout, edgeBetweennessSlider.getValue(), 
								similarColors, groupVertices.isSelected());
				} catch (InterruptedException e) {
				//	vv.CENTER_ALIGNMENT;
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				
				vv.updateUI();
    	  }
 	      }
 	    };

       //Register a listener for the radio buttons.
       birdButton.addActionListener(viewMenuListener);
       catButton.addActionListener(viewMenuListener);
       dogButton.addActionListener(viewMenuListener);
       rabbitButton.addActionListener(viewMenuListener);
       pigButton.addActionListener(viewMenuListener);
       
      // viewMenu.add(group);
       viewMenu.add(birdButton);
       viewMenu.add(catButton);
       viewMenu.add(dogButton);
       viewMenu.add(rabbitButton);
       viewMenu.add(pigButton);
       
       ActionListener edgeMenuListener = new ActionListener() {
    	  
    	      public void actionPerformed(ActionEvent event) {
    	    	  
    	    //	  toggleEdges();
    	    	//  vv.updateUI();
    	        AbstractButton aButton = (AbstractButton) event.getSource();
    	        boolean selected = aButton.getModel().isSelected();
    	        String newLabel;
    	        Icon newIcon;
    	        if (selected) {
					 vv.getRenderContext().setEdgeLabelTransformer(new ToStringLabeller());
				//	 FRlayout = new FRLayout(sgv.g);	
					 
				//	 vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.FRLAYOUT);
					 vv.updateUI();

    	          newLabel = "Edges";
    	        } else {
    	          newLabel = "Edges";
					 vv.getRenderContext().setEdgeLabelTransformer(blankLabelTransformer);
					
					
					//layout2.setTranslate(g.getVertices()., );
					// layout2.translate(tmp.getX(), tmp.getY());
					 vv.updateUI();
    	        }
    	        aButton.setText(newLabel);
    	      }
    	    };
    	    ActionListener VertexMenuListener = new ActionListener() {
    	    	  
      	      public void actionPerformed(ActionEvent event) {
      	    	  
      	    //	  toggleEdges();
      	    	  vv.updateUI();
      	        AbstractButton aButton = (AbstractButton) event.getSource();
      	        boolean selected = aButton.getModel().isSelected();
      	        String newLabel;
      	        Icon newIcon;
      	        if (selected) {
      	        	
  					 vv.getRenderContext().setVertexLabelTransformer(blankLabelTransformer2);
  					 
  					 vv.updateUI();
      	          newLabel = "Vertices";
      	        } else {
      	          newLabel = "Vertices";
  					 vv.getRenderContext().setVertexLabelTransformer(blankLabelTransformer);

      	          vv.updateUI();
      	        }
      	        aButton.setText(newLabel);
      	      }
      	    };
       
       toggleEdge.addActionListener(edgeMenuListener);
       toggleVertices.addActionListener(VertexMenuListener);
       toggle.add(toggleVertices);
      // menu.setMnemonic(KeyEvent.VK_A);
       display.add(toggle);
   //    menu.getAccessibleContext().setAccessibleDescription(
            //   "The only menu in this program that has menu items");
       menuBar.add(mainMenu);
       menuBar.add(viewMenu);
       menuBar.add(display);
       //frame.add(pnlButton, BorderLayout.SOUTH);
       EditingModalGraphMouse gm = new EditingModalGraphMouse(vv.getRenderContext(), 
               GraphElements.MyVertexFactory.getInstance(),
              GraphElements.MyEdgeFactory.getInstance()); 
       gm.setMode(ModalGraphMouse.Mode.TRANSFORMING);
       PopupVertexEdgeMenuMousePlugin myPlugin = new PopupVertexEdgeMenuMousePlugin();
       gm.remove(gm.getPopupEditingPlugin());
       gm.add(myPlugin);
       vv.setGraphMouse(gm);
       JPopupMenu edgeMenu = new MyMouseMenus.EdgeMenu(frame);
       JPopupMenu vertexMenu = new MyMouseMenus.VertexMenu();
       myPlugin.setEdgePopup(edgeMenu);
       myPlugin.setVertexPopup(vertexMenu);
       vv.getRenderContext().setVertexFillPaintTransformer(MapTransformer.<GraphElements.MyVertex,Paint>getInstance(vertexPaints));
		vv.getRenderContext().setVertexDrawPaintTransformer(new Transformer<GraphElements.MyVertex,Paint>() {
			public Paint transform(GraphElements.MyVertex v) {
				Set<GraphElements.MyVertex> pickedVertices = vv.getPickedVertexState().getPicked();
				ArrayList<GraphElements.MyVertex> selected = new ArrayList<GraphElements.MyVertex>(pickedVertices);
				if(pickedVertices.size()==0) {
					text.setText("Select Vertices ...");						
					grid2.setVisible(false);
					vertexDetail.setVisible(false);
					currentVertex=1;
					vertexBorder.setTitle("Vertex Details");
					east.updateUI();
					return Color.BLACK;
					} 
				else if(vv.getPickedVertexState().isPicked(v)) {
					
					if (pickedVertices.size()>1){
						grid2.setVisible(true);
					}
					vertexDetail.setVisible(true);
					text.setText(v.getName());
					Set<GraphElements.MyVertex> setV = new HashSet<GraphElements.MyVertex>();
				       setV.addAll(g.getVertices());
				      
					updatePanel(selected,1);
					
					return Color.cyan;
					
				} else {
					return Color.BLACK;
				}
			}
		});
		
		

		vv.getRenderContext().setEdgeDrawPaintTransformer(MapTransformer.<GraphElements.MyEdge,Paint>getInstance(edgePaints));
		vv.getRenderContext().setArrowFillPaintTransformer(MapTransformer.<GraphElements.MyEdge,Paint>getInstance(edgePaints));
		vv.getRenderContext().setArrowDrawPaintTransformer(MapTransformer.<GraphElements.MyEdge,Paint>getInstance(edgePaints));
		vv.getRenderContext().setEdgeStrokeTransformer(new Transformer<GraphElements.MyEdge,Stroke>() {
               protected final Stroke THIN = new BasicStroke(1);
               protected final Stroke THICK= new BasicStroke(1);
               
               public Stroke transform(GraphElements.MyEdge e)
               {
                   Paint c = edgePaints.get(e);
                   if (c == Color.LIGHT_GRAY)
                       return THIN;
                   else 
                       return THIN;
               }
           });
       JMenu modeMenu = gm.getModeMenu();
       modeMenu.setText("Mouse Mode");
       menuBar.add(modeMenu);
       frame.setJMenuBar(menuBar);
       modeMenu.setPreferredSize(new Dimension(120,20));
       gm.setMode(ModalGraphMouse.Mode.TRANSFORMING);
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       vv.setSize(preferredGraphSize);
     //  vv.getGraphLayout().getSize();
       ScrollPane scrollPane=new ScrollPane();
       scrollPane.setSize(preferredGraphSize);
       scrollPane.add(vv);
       
       deleteButton.setVisible(false);
       ActionListener deleteAction = new ActionListener() {
       	  
   	      public void actionPerformed(ActionEvent event) {
   	    	  System.out.println("BOO PRINTED");
   	      }
   	    };
       deleteButton.addActionListener(deleteAction);
       InputMap im = vv.getInputMap();
       KeyListener keyl = new KeyListener() {
           public void keyPressed(KeyEvent e) {
        	   if (e.getKeyCode() == 127){
        		   System.out.println("DELETED ::::");
        		   Object[] options = {"Yes, please",
                           "No, thanks"};
        		   Set<GraphElements.MyVertex> vert = vv.getPickedVertexState().getPicked();
        		   int n = JOptionPane.showOptionDialog(frame,
        				    "Are you sure that you want to delete "+vert.size()
        				    + " vertices?",
        				    "Confirm the deletion",
        				    JOptionPane.YES_NO_OPTION,
        				    JOptionPane.QUESTION_MESSAGE,
        				    null,
        				    options,
        				    options[1]);
        		  System.out.println("value = "+n);
        		  if (n==0){
        		   for (GraphElements.MyVertex v : vert){
        			   g.removeVertex(v);
        			   vv.updateUI();
        			   east.updateUI();
        			   updatePanel(new ArrayList<GraphElements.MyVertex>(vert),0);
        		   }
        		  }
        		  
        		   
        		   }
           }

           public void keyReleased(KeyEvent e) {
           }

           public void keyTyped(KeyEvent e) {
           }

       };
       vv.addKeyListener(keyl);
       frame.getContentPane().add(scrollPane);
       Set<GraphElements.MyVertex> setV = new HashSet<GraphElements.MyVertex>();
       setV.addAll(g.getVertices());
       ranker = new BetweennessCentrality<GraphElements.MyVertex,GraphElements.MyEdge>(g,true,false);
       markovRanker = new MarkovCentrality<GraphElements.MyVertex,GraphElements.MyEdge>((DirectedGraph<GraphElements.MyVertex, GraphElements.MyEdge>) g,setV);
       pageRanker = new PageRank<GraphElements.MyVertex, GraphElements.MyEdge>(g,0.2);
       pageRanker.initialize();
       pageRanker.evaluate();
       HitsRanker = new HITS<GraphElements.MyVertex,GraphElements.MyEdge>(g);
       HitsRanker.initialize();
       HitsRanker.evaluate();
       markovRanker.evaluate();
       
       
             
       ranker.setRemoveRankScoresOnFinalize(false);
   	ranker.evaluate();
       resizeNodes(ranker);
       ranker.getVertexRankScore(vertexArray[1]);
  /*     for (int i = 0; i < vertexArray.length; i++){ 
    	   if(!duplicate(i)){
       Collection<GraphElements.MyEdge> neighbourEdge = g.getIncidentEdges(vertexArray[i]);
       System.out.println();
       System.out.println(vertexArray[i].getName());
        System.out.println("neighbours = "+neighbourEdge.size()+ "("+i+") for "+vertexArray[i].getName()+", degree = "+g.degree(vertexArray[i]));
        
      for (GraphElements.MyEdge e : neighbourEdge){
    	   Pair<GraphElements.MyVertex> end = g.getEndpoints(e);
    	   System.out.println("Edges from "+vertexArray[i].getName()+"("+e.getName()+") = "+"<"+end.getFirst().getName()+","+end.getSecond().getName()+"> of weight "+e.getWeight());
       }
       }
       }*/
   //    EdgeBetweennessClusterer<>
       int numberOfEdgesToRemove = 0;
      // EdgeBetweennessClusterer bc = new EdgeBetweennessClusterer(numberOfEdgesToRemove);
     //  Set clusterSet = bc.transform(g);
    //   List edges = bc.getEdgesRemoved();
       
   //   EdgeBetweennessClusterer<Collection<MyEdge>,g>  bc = new EdgeBetweennessClusterer<g.getEdges(),g.getVertices()>(1);
     //  Set<Set<MyNode>> sets = EBC1.transform(g);
       
     //  System.out.println(vv.size());
       //frame.add(menuBar, BorderLayout.NORTH);
       frame.pack();
       frame.setVisible(true);     
      // for (int i = 0;i<vertexArray.length;i++){
    //	   System.out.println("degree for "+vertexArray[i].getName()+" = "+g.degree(vertexArray[i]));
    	   //g.
     //  }
      
       
       
    }
    
    private static void resizeNodes(final BetweennessCentrality ranker) {
    	
    	 Transformer<GraphElements.MyVertex, Shape> vertexSize2 = new Transformer<GraphElements.MyVertex,Shape>(){
    	        //    private AbstractRanker ranker;
    		 public Shape transform(GraphElements.MyVertex i){
					
	                Ellipse2D circle = new Ellipse2D.Double(-10, -10, 20, 20);
	                Ellipse2D circlemini = new Ellipse2D.Double(-2,-2,3,3);
	              //  System.out.println("Rank score for "+i.getName()+" = "+ranker.getVertexRankScore(i));
	                // in this case, the vertex is twice as large
	                if(g.degree(i)==0) return circlemini;
	                
	                else return AffineTransform.getScaleInstance(HitsRanker.getVertexScore(i).authority*10, HitsRanker.getVertexScore(i).authority*10).createTransformedShape(circle);
	            }
	        };
	        vv.getRenderContext().setVertexShapeTransformer(vertexSize2);	
	}

	public static void clusterAndRecolor(AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge> layout,
    		int numEdgesToRemove,
    		Color[] colors, boolean groupClusters) throws InterruptedException {

    		
    		Graph<GraphElements.MyVertex, GraphElements.MyEdge> g = layout.getGraph();
            layout.removeAll();
            vv.repaint();
            vv.updateUI();
     //       Thread.sleep(1910);
           // jjj
    		EdgeBetweennessClusterer<GraphElements.MyVertex, GraphElements.MyEdge> clusterer =
    			new EdgeBetweennessClusterer<GraphElements.MyVertex, GraphElements.MyEdge>(numEdgesToRemove);
    		Set<Set<GraphElements.MyVertex>> clusterSet = clusterer.transform(g);
    		List<GraphElements.MyEdge> edges = clusterer.getEdgesRemoved();
    		

    		int i = 0;
    		//Set the colors of each node so that each cluster's vertices have the same color
    		for (Iterator<Set<GraphElements.MyVertex>> cIt = clusterSet.iterator(); cIt.hasNext();) {

    			Set<GraphElements.MyVertex> vertices = cIt.next();
    			Color c = colors[i % colors.length];

    			colorCluster(vertices, c);
    			if(groupClusters == true) {
    				groupCluster(layout, vertices);
    			}
    			i++;
    		}
    		for (GraphElements.MyEdge e : g.getEdges()) {

    			if (edges.contains(e)) {
    				edgePaints.put(e, Color.lightGray);
    			} else {
    				edgePaints.put(e, Color.pink);
    			}
    		}

    	}

    
    
    	private static void colorCluster(Set<GraphElements.MyVertex> vertices, Color c) {
    		for (GraphElements.MyVertex v : vertices) {
    			vertexPaints.put(v, c);
    		}
    		vv.repaint();
    	}
    	
    	public Integer sum(List<Integer> list) {
    	     Integer sum= 0; 
    	     for (Integer i:list)
    	         sum = sum + i;
    	     return sum;
    	}

    	
    	private static void groupCluster(AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge> layout, Set<GraphElements.MyVertex> vertices) {
    	//	System.out.println("GROUPING");
    		if(vertices.size() < layout.getGraph().getVertexCount()) {
    			Point2D center = layout.transform(vertices.iterator().next());
    			Graph<GraphElements.MyVertex, GraphElements.MyEdge> subGraph = SparseMultigraph.<GraphElements.MyVertex, GraphElements.MyEdge>getFactory().create();
    			for(GraphElements.MyVertex v : vertices) {
    				subGraph.addVertex(v);
    			}
    			Layout<GraphElements.MyVertex, GraphElements.MyEdge> subLayout = 
    				new CircleLayout<GraphElements.MyVertex, GraphElements.MyEdge>(subGraph);
    			subLayout.setInitializer(vv.getGraphLayout());
    			subLayout.setSize(preferredGraphSize);

    			layout.put(subLayout,center);
    			vv.repaint();
    		}
    	}

    	 protected static void updatePanel(ArrayList<GraphElements.MyVertex> vertices,
    				int currentVertex2) {
    		 System.out.println("Updating ...");
    	    	if (currentVertex2 ==0){
    	    		text.setText("Select Vertices ...");						
    				grid2.setVisible(false);
    				vertexDetail.setVisible(false);
    				currentVertex=1;
    				vertexBorder.setTitle("Vertex Details");
    				east.updateUI();
    	    	}
    	    	
    	    	else{
    	    	//System.out.println(layout.getSize());
    	    	GraphElements.MyVertex v=vertices.get(currentVertex - 1);
    	    	 
    			  tmp = dikstra.getDistanceMap(v);
    			//  System.out.println(tmp);
    			  to = commsMap.get(updatedNames.indexOf(v.getName()));
    			  int sum = 0;
    			  if  (to!=null){Collection<Integer> toRecipients = to.values();
    			  ArrayList<Integer> rec = new ArrayList<Integer>();
    			  rec.addAll(toRecipients);
    			  for (int l = 0; l < toRecipients.size();l++){
    				  sum = sum + rec.get(l);
    			  }
    			  }
    			  int sent = 0;
    			  if (!(sentlist.get(updatedNames.indexOf(v.getName()))==null)){
    				  sent = sentlist.get(updatedNames.indexOf(v.getName())).size();
    			  }
    			 Double dist = distances.transform(v);
    			 String diststr = "NaN";
    				if (Double.isNaN(dist)){
    				//	dist = (double)0;
    				}
    				else{
    					dist = dist+1;
    					diststr = df.format(dist);
    				}
    			 // System.out.println(v.getName().replace(" ","_")+", total_sentList= "+sent+", total_sent= "+sum+", total_recieved= "+recievedlist.get(updatedNames.indexOf(v.getName()))+", degree= "+g.degree(v)+", betweenness= "+ranker.getVertexRankScore(v)+", PageRank= "+pageRanker.getVertexScore(v)+", Markov= "+markovRanker.getVertexRankScore(v)+", HITS_authority= "+HitsRanker.getVertexScore(v).authority+", HITS_hub= "+HitsRanker.getVertexScore(v).hub+", cliques= "+cliqueScores.get(v)+", avgDistance= "+(diststr)+", clusteringCoeff= "+cl.get(v));
    			  Integer recTotal = recievedlist.get(updatedNames.indexOf(v.getName()));
    			  if (recTotal == null){
    				  recTotal = 0;
    			  }
    			 vertexBorder.setTitle(Integer.toString(currentVertex)+" of "+vertices.size());
    			vertexDetail.setText("total sent = "+sent+"\n"+"total recieved = "+recTotal+
    					"\n"+"degree = "+g.degree(v)+"\n"+"betweenness= "+
    					df.format(ranker.getVertexRankScore(v))+"\n"+"PageRank= "+
    			 df.format(pageRanker.getVertexScore(v))+"\n"+"Markov= "+
    					df.format(markovRanker.getVertexRankScore(v))+"\n"+"HITS_authority= "+
    			 df.format(HitsRanker.getVertexScore(v).authority)+"\n"+"HITS_hub= "+
    					df.format(HitsRanker.getVertexScore(v).hub)+
    					"\n"+"weighted clique score= "+cliqueScores.get(v)+
    					"\n"+"number of cliques= "+cliqueScores2.get(v)+
    					"\n"+"avgDistance= "+(diststr)+
    					"\n"+"clusteringCoeff= "+df.format(cl.get(v)));
    			text.setText(v.getName());
    			east.updateUI();	
    	    	}
    		}
    
}