
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Paint;
import java.awt.ScrollPane;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

import edu.uci.ics.jung.algorithms.shortestpath.DistanceStatistics;
import edu.uci.ics.jung.algorithms.metrics.Metrics;
import edu.uci.ics.jung.algorithms.shortestpath.DijkstraDistance;

import java.io.BufferedReader;
import java.io.FileNotFoundException;

import edu.uci.ics.jung.algorithms.importance.BetweennessCentrality;
import edu.uci.ics.jung.algorithms.importance.MarkovCentrality;
import edu.uci.ics.jung.algorithms.importance.KStepMarkov;//needtoadd
import edu.uci.ics.jung.algorithms.importance.Ranking;//needtoadd
import edu.uci.ics.jung.algorithms.importance.RelativeAuthorityRanker;//needtoadd
import edu.uci.ics.jung.algorithms.importance.WeightedNIPaths;//needtoadd
import edu.uci.ics.jung.algorithms.scoring.DistanceCentralityScorer;//needtoadd
import edu.uci.ics.jung.algorithms.scoring.EigenvectorCentrality;//needtoadd
import edu.uci.ics.jung.algorithms.scoring.HITS;//needtoadd
import edu.uci.ics.jung.algorithms.scoring.VoltageScorer;//needtoadd
import edu.uci.ics.jung.algorithms.scoring.PageRank;//needtoadd

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.functors.ChainedTransformer;
import org.apache.commons.collections15.functors.ConstantTransformer;
import org.apache.commons.collections15.functors.MapTransformer;
import org.apache.commons.collections15.map.LazyMap;

import edu.uci.ics.jung.algorithms.cluster.EdgeBetweennessClusterer;
import edu.uci.ics.jung.algorithms.cluster.VoltageClusterer;
import edu.uci.ics.jung.algorithms.layout.AggregateLayout;
import edu.uci.ics.jung.algorithms.layout.CircleLayout;
//import edu.uci.ics.jung.algorithms.cluster.ClusterSet;
import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.FRLayout2;
import edu.uci.ics.jung.algorithms.layout.ISOMLayout;
import edu.uci.ics.jung.algorithms.layout.KKLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.layout.util.Relaxer;
import edu.uci.ics.jung.graph.DirectedGraph;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.algorithms.layout.TreeLayout;
import edu.uci.ics.jung.graph.SparseMultigraph;
import edu.uci.ics.jung.graph.util.Pair;
import edu.uci.ics.jung.visualization.Layer;
//import edu.uci.ics.jung.visualization.decorators.
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.EditingModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.picking.PickedState;
import edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position;
import edu.uci.ics.jung.visualization.transform.MutableTransformer;



/**
 *
 * @author Dr. Greg M. Bernstein
 */
public class EmailsGroupSize {
	static Map<Integer,HashMap<Integer, Integer>> commsMap = new HashMap<Integer,HashMap<Integer, Integer>>();
	static int currentVertex=1;
	static JTextPane text = new JTextPane();
	static JButton deleteButton = new JButton();
	static Transformer<GraphElements.MyVertex, Double> distances;
	static HashMap<GraphElements.MyVertex, Double> cliqueScores;
	static HashMap<GraphElements.MyVertex, Double> cliqueScores2;
	static DijkstraDistance<GraphElements.MyVertex, Number> dikstra;
	static HashMap<Integer, Integer> to;
	static JPanel east = new JPanel();
	static JPanel grid2 = new JPanel(new GridLayout(2,1));
	static JTextPane vertexDetail = new JTextPane();
	static TitledBorder vertexBorder = BorderFactory.createTitledBorder("Vertex Details");
	static Map<GraphElements.MyVertex, Double> cl;
	static Map<GraphElements.MyVertex,Number> tmp;
	static Map<Integer, Integer> recievedlist = new HashMap<Integer,Integer>();
	static Map<Integer, Set<String>> sentlist = new HashMap<Integer,Set<String>>();
	static List<String> updatedNames = new ArrayList<String>();
	static  BetweennessCentrality<GraphElements.MyVertex,GraphElements.MyEdge> ranker;
	static  HITS<GraphElements.MyVertex,GraphElements.MyEdge> HitsRanker;
	static  MarkovCentrality<GraphElements.MyVertex,GraphElements.MyEdge> markovRanker;
	static  PageRank<GraphElements.MyVertex,GraphElements.MyEdge> pageRanker;
	static DistanceStatistics bob = new DistanceStatistics();
	static  Dimension preferredGraphSize=new Dimension(1000,700);

	static GraphElements.MyVertex[] vertexArray = new GraphElements.MyVertex[184];
	public final static Color[] similarColors =
		{
			new Color(210, 134, 134),
			new Color(135, 137, 211),
			new Color(134, 206, 189),
			new Color(206, 176, 134),
			new Color(194, 204, 134),
			new Color(145, 214, 134),
			new Color(133, 178, 209),
			new Color(103, 148, 255),
			new Color(60, 220, 220),
			new Color(30, 250, 100)
		};
	
	//vv = new VisualizationViewer<Number,Number>(layout);
	static Map<GraphElements.MyVertex,Paint> vertexPaints = 
			LazyMap.<GraphElements.MyVertex,Paint>decorate(new HashMap<GraphElements.MyVertex,Paint>(),
					new ConstantTransformer(Color.white));
		static Map<GraphElements.MyEdge,Paint> edgePaints =
		LazyMap.<GraphElements.MyEdge,Paint>decorate(new HashMap<GraphElements.MyEdge,Paint>(),
				new ConstantTransformer(Color.blue));
	static EmailsGroupSize sgv = new EmailsGroupSize();   
	private static final Object DEMOKEY = "DEMOKEY";
//	GraphElements.MyVertex x = new GraphElements.MyVertex("BOO");
	static AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge> layout = new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new ISOMLayout<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g));
	static AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge> FRlayout =  new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new FRLayout<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g));
	static AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge> KKlayout = new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new KKLayout<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g));
static VisualizationViewer<GraphElements.MyVertex, GraphElements.MyEdge> vv = 
            new VisualizationViewer<GraphElements.MyVertex, GraphElements.MyEdge>(layout);
	static Transformer blankLabelTransformer = new ChainedTransformer<String,String>(new Transformer[]{
            new ToStringLabeller<String>(),
            new Transformer<String,String>() {
            public String transform(String input) {
                return "<html><font color=\"black\">";
            }}});
//	private static boolean edgesVisible = false;
	//private static boolean verticesVisible = true;
    static Graph<GraphElements.MyVertex, GraphElements.MyEdge> g;
    static NumberFormat df = new DecimalFormat("####0.000000");
   // static BetweennessCentrality ranker = new BetweennessCentrality(g,true,false);

    /** Creates a new instance of SimpleGraphView */
    public EmailsGroupSize() {
    	
    	// This builds the graph
        // Layout<V, E>, VisualizationComponent<V,E>
        
        // Graph<V, E> where V is the type of the vertices and E is the type of the edges
        // Note showing the use of a SparseGraph rather than a SparseMultigraph
        g = new DirectedSparseMultigraph<GraphElements.MyVertex, GraphElements.MyEdge>();
        // Add some vertices. From above we defined these to be type Number.   
        
        updateNames();
        for (int i = 0;i<updatedNames.size();i++){
        	GraphElements.MyVertex vertex1 = new GraphElements.MyVertex(Integer.toString(i));
        	
        	if(!duplicate(i)){
        	//	System.out.println("Already seen "+vertex1.getName());
        		g.addVertex(vertex1);
        	}
        	vertexArray[i] = vertex1;
        	
            // if(i < 20){
           // 	 System.out.println(vertex1.getName());
           //  }
        };
        try {
			FileReader fr = new FileReader("/Users/DodionTwo/Documents/AllTogether2.csv");
			BufferedReader bf = new BufferedReader (fr);
			int lineNum = 0;
			//If headers is true, we want to avoid inserting the first line
			bf.readLine();
			String tmpline = "";
			while ((tmpline = bf.readLine())!=null){
			//	System.out.println("Line = '"+tmpline+"'");
				//String tmpLine = bf.readLine();
				//System.out.println(tmpline);
				String[] tmpArray = tmpline.split(",");
				System.out.println(Arrays.deepToString(tmpArray));
				String dt = tmpArray[4];
				String senderEmail = tmpArray[0];
				String senderTmp = senderEmail.split("@")[0].replace(" ","").replace(".", " ");				
				String[] Torecipients = tmpArray[1].split("; ");
			//	System.out.println(senderTmp+" to "+Arrays.deepToString(recipients));
				String senderName = emailHeaderToName(senderTmp);
				//String CcRecipients = tmpArray[2];
				String[] CcRecipientList = tmpArray[2].split("; ");
				String[] recipients = new String[Torecipients.length
						+ CcRecipientList.length];
				System.arraycopy(Torecipients, 0, recipients, 0,
						Torecipients.length);
				System.arraycopy(CcRecipientList, 0, recipients,
						Torecipients.length, CcRecipientList.length);
				String daysOld = tmpArray[tmpArray.length-2];
				int size = Integer.parseInt(tmpArray[tmpArray.length-1]);
				if (size > (1024*1024*1)){
			//	String tag = tmpArray[3]; //0 = to, 1 = cc, 2 = bcc
			//	System.out.println(senderName+" to "+Arrays.deepToString(recipients)+"date = "+dt);
			/*	try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
				for (int i = 0; i < recipients.length;i++){
					String recipientName = emailHeaderToName(recipients[i].split("@")[0].replace("\""," ").replace(" ","").replace(".", " "));
					System.out.println("Recipient Name"+recipientName);
					if (recipientName!= ""){
				Integer sender = updatedNames.indexOf(senderName);
				Integer recipient = updatedNames.indexOf(recipientName);
			//	System.out.println(senderName+" to "+Arrays.deepToString(recipients));
				if (commsMap.containsKey(sender)&&(updatedNames.indexOf(updatedNames.get(sender))!= updatedNames.indexOf(updatedNames.get(recipient)))){
					if (lineNum==47128){
						//System.out.println("Within me "+dt +updatedNames.get(sender)+" to "+updatedNames.get(recipient)+ "("+senderName+","+recipientName+")");
				
					}
					
					
					HashMap<Integer,Integer>to = commsMap.get(sender);
				//	if (sender==150){System.out.println("Before = "+to);}
					if (to.containsKey(recipient)){

					/*	if (sender==150){
						System.out.println("Already sent from "+sender+" to "+recipient+" before "+to.get(recipient));
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						}*/
						int previousTotal = to.get(recipient);
						to.put(recipient, previousTotal+1);
					}
					else{
						/*if (sender==150){
						System.out.println("Not sent from "+sender+" to "+recipient+" before");
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						}*/
						to.put(recipient,1);
					}
				//	if (sender==150){System.out.println("After = "+to);System.out.println();}
				//	ArrayList<HashMap<String,Integer>> commsTriple = new ArrayList<HashMap<String,Integer>>();
					commsMap.put(sender,to);
					
				//	System.out.println("seen before new "+sender+" = "+commsTriple);
				//	Thread.sleep(1000);
				}
				else if (!commsMap.containsKey(sender)){
					//if(Integer.parseInt(tag)==1){
				//	HashMap<Integer,Integer> tempList = new HashMap<Integer,Integer>();
				//	HashMap<Integer,Integer> tempList2 = new HashMap<Integer,Integer>();
					HashMap<Integer,Integer> to = new HashMap<Integer,Integer>();
						to.put(recipient,1);
						
						commsMap.put(sender, to);
					//	System.out.println("not seen before new "+sender+" = ");
						
					//}
				}
				if (recievedlist.containsKey(recipient)&&updatedNames.indexOf(updatedNames.get(sender))!= updatedNames.indexOf(updatedNames.get(recipient))){
					int prev = recievedlist.get(recipient);
					prev++;
					
					//System.out.println("PUTTING "+recipient+" FROM "+sender);
					recievedlist.put(recipient, prev);
				}
				else if (sender!= recipient){
					int prev = 1;
					recievedlist.put(recipient, prev);
				}
				
				if (sentlist.containsKey(sender)&&(sender!= recipient)){
					Set<String> prev = sentlist.get(sender);
					prev.add(dt);
					sentlist.put(sender, prev);
				}
				else if (sender!= recipient){
					Set<String> prev = new HashSet<String>(); 
					prev.add(dt);
					sentlist.put(sender, prev);
				}
				}
				}
				lineNum++;
				}
			}
			fr.close();
		} 
        catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}// catch (InterruptedException e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
		//}
        
      //need to add list  
	//	g.addEdge(edge1, vertexArray[Integer.parseInt(tmpArray[2])], vertexArray[Integer.parseInt(tmpArray[3])]);
/*try {
	Thread.sleep(10000);
} catch (InterruptedException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}*/
        
     //   System.out.println(commsMap.get("0"));
        for (Integer sender : commsMap.keySet()){
			HashMap<Integer, Integer> testEdges = commsMap.get(sender);
			for (Integer recipient : testEdges.keySet()){
				if (testEdges.get(recipient)>1){
				GraphElements.MyEdge edge1 = new GraphElements.MyEdge("Edge "+sender+"-"+recipient);
				edge1.setWeight(testEdges.get(recipient));
				g.addEdge(edge1, vertexArray[sender], vertexArray[recipient]);}
			}
			}
   
        FileWriter fr2;
        try {
        	fr2 = new FileWriter("/Users/DodionTwo/Desktop/MyLDCForHierarchy.txt", false);
    		fr2.flush();
    		System.out.println(commsMap);
    		Collection<GraphElements.MyVertex> vertexColl = g.getVertices();
    		ArrayList<GraphElements.MyVertex> vertexList = new ArrayList<GraphElements.MyVertex>(vertexColl);
    		for (int i = 0;i <vertexList.size() ; i++){
    			String sender = vertexList.get(i).getName();
    			//System.out.println()
    			if (commsMap.containsKey(Integer.parseInt(sender))){
    				HashMap<Integer, Integer> recipients = commsMap.get(Integer.parseInt(sender));
    		//		System.out.println("Recipients for "+sender+" "+ updatedNames.indexOf(sender)+" = "+recipients);
    				for (int j =0; j < vertexList.size();j++){
    					String recipient = vertexList.get(j).getName();
    					if (recipients.containsKey(Integer.parseInt(recipient))){
    					//	System.out.println("Link between "+sender+" and "+recipient+" of weight "+recipients.get(j));
    					fr2.write(sender.replace(" ", "_")+" "+recipient.replace(" ", "_")+" "+recipients.get(Integer.parseInt(recipient))+"\n");
    					}
    					else{
    					//	System.out.println("No Link between "+sender+" and "+recipient);
    						
        						fr2.write(sender.replace(" ", "_")+" "+recipient.replace(" ", "_")+" 0"+"\n");
    						
    					}
    				}
    			}
    			else {
    				
    				//	System.out.println("No messages sent from "+sender);
    					for (int  j = 0; j < vertexList.size();j++){
    						String recipient = vertexList.get(j).getName();
        						fr2.write(sender.replace(" ", "_")+" "+recipient.replace(" ", "_")+" 0"+"\n");
    						
    					}
    				
    				
    			}
    			}
    	fr2.close();	
        }
        catch (IOException e1) {
			e1.printStackTrace();
		}
        
		
        
       
    }
    
    private String emailHeaderToName(String senderOld) {
    	String senderName = "";
    	String[] senderNames = senderOld.split(" ");
    	//System.out.println("Processing name = "+senderOld+" of name length "+senderNames.length);
		if (senderNames.length>=2){
		for (int j = 0; j < senderNames.length-1;j++){
			senderName = senderName + properCase(senderNames[j])+" ";
		}
		senderName = senderName + properCase(senderNames[senderNames.length-1]);
		}
		if (senderNames.length==1){
			senderName = properCase(senderNames[0]);
		}
		return senderName;
	}

	private static boolean duplicate(int i) {
return false;
	}

	private void updateNames() {
    	updatedNames.add("Michael Goldsmith");
    	updatedNames.add("Elizabeth Phillips");
    	updatedNames.add("Phil Legg");
    	updatedNames.add("Oliver Buckley");
    	updatedNames.add("Jason Nurse");
    	updatedNames.add("Ioannis Agrafiotis");
    	updatedNames.add("Arnau Erola");
    	updatedNames.add("Duncan Hodges");
    	updatedNames.add("Jassim Happa");
    	updatedNames.add("Thomas Gibson-robinson");
	}

	private int[] rand(int indices) {
    	int min = 1;
    	Random random = new Random();
    	int value1 = random.nextInt(indices - min + 1) + min;
    	int value2 = random.nextInt(indices - min + 1) + min;
    	while(value2==value1){
    		value2 = random.nextInt(indices - min + 1) + min;
    	}
		// TODO Auto-generated method stub
		int[] values = new int[2];
		values[0] = value1;
		values[1] = value2;
		return values;
	}

	/**
     * @param args the command line arguments
	 * @throws InterruptedException 
     */
    public static void main(String[] args) throws InterruptedException {
       
        // Setup up a new vertex to paint transformer...
        Transformer<Number,Paint> vertexPaint = new Transformer<Number,Paint>() {
            public Paint transform(Number i) {
                return Color.GREEN;
            }
        }; 
        // vv.getRenderContext().setVertexShapeTransformer(vertexSize);
        // Set up a new stroke Transformer for the edges
        float dash[] = {5.0f};
        final Stroke edgeStroke = new BasicStroke(0.5f, BasicStroke.CAP_BUTT,
             BasicStroke.JOIN_MITER, 5.0f, dash, 0.0f);
        // vv.getRenderContext().setVertexFillPaintTransformer(vertexPaint);
      //  vv.getRenderContext().setEdgeStrokeTransformer(edgeStrokeTransformer);
        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        vv.getRenderer().getVertexLabelRenderer().setPosition(Position.CNTR);        
        
        final JFrame frame = new JFrame("Simple Graph View 2");
        frame.setSize(new Dimension(300,300));
       //frame.
        JFrame frameSide = new JFrame("Simple Graph Vi");
        JPanel pnlButton = new JPanel();
        BronKerboschCliqueFinder<GraphElements.MyVertex,GraphElements.MyEdge> clique = new BronKerboschCliqueFinder<GraphElements.MyVertex, GraphElements.MyEdge>(g);
        Collection<Set<GraphElements.MyVertex>> cliques = clique.getAllMaximalCliques();
        
        ArrayList<Set<GraphElements.MyVertex>> cliqueList = new ArrayList<Set<GraphElements.MyVertex>>();
        cliqueList.addAll(cliques);
       // System.out.println(cliques);
        cliqueScores = new HashMap<GraphElements.MyVertex, Double>();
        Collection<GraphElements.MyVertex> g1 = g.getVertices();
        ArrayList<GraphElements.MyVertex> vertexList = new ArrayList<GraphElements.MyVertex>();
        vertexList.addAll(g1);
        for (int i = 0; i < vertexList.size();i++){
        	double count= 0;
        //	System.out.print("row for "+vertexList.get(i).getName()+" = ");
        	for (int j = 0; j <cliques.size()-1;j++){
        		if (cliqueList.get(j).contains(vertexList.get(i))){
        			double temp = cliqueList.get(j).size();
            		double poww = Math.pow(2,(temp-1));
        			count = count + poww;
        		}
        		//System.out.print(cliqueList.get(j).contains(vertexList.get(i))+", ");
        	}
        	if (cliqueList.get(cliqueList.size()-1).contains(vertexList.get(i))){
        		double temp = cliqueList.get(cliqueList.size()-1).size();
        		double poww = Math.pow(2,(temp-1));
    			count = count + poww;
   		}
        //	System.out.println(cliqueList.get(cliqueList.size()-1).contains(vertexList.get(i)));
        //	System.out.println("Total for "+vertexList.get(i)+" = "+count);
        	cliqueScores.put(vertexList.get(i), count);
        }
        
Collection<Set<GraphElements.MyVertex>> cliques2 = clique.getAllMaximalCliques();
        
        ArrayList<Set<GraphElements.MyVertex>> cliqueList2 = new ArrayList<Set<GraphElements.MyVertex>>();
        cliqueList2.addAll(cliques2);
       // System.out.println(cliques);
        cliqueScores2 = new HashMap<GraphElements.MyVertex, Double>();
        for (int i = 0; i < vertexList.size();i++){
        	double count= 0;
        //	System.out.print("row for "+vertexList.get(i).getName()+" = ");
        	for (int j = 0; j <cliques2.size()-1;j++){
        		if (cliqueList2.get(j).contains(vertexList.get(i))){
        			count++;
        		}
        		//System.out.print(cliqueList.get(j).contains(vertexList.get(i))+", ");
        	}
        	if (cliqueList2.get(cliqueList2.size()-1).contains(vertexList.get(i))){
        		count++;
   		}
        //	System.out.println(cliqueList.get(cliqueList.size()-1).contains(vertexList.get(i)));
        //	System.out.println("Total for "+vertexList.get(i)+" = "+count);
        	cliqueScores2.put(vertexList.get(i), count);
        }
       frame.setBackground(Color.WHITE);
       JMenuBar menuBar = new JMenuBar();
       JMenu mainMenu = new JMenu("File");
       JMenu viewMenu = new JMenu("Change view");
       JMenu display = new JMenu("Display Settings");
       JMenu toggle = new JMenu("Toggle Labels");
       JCheckBoxMenuItem toggleEdge = new JCheckBoxMenuItem("Edges");
       JCheckBoxMenuItem toggleVertices = new JCheckBoxMenuItem("Vertices", true);
       toggle.add(toggleEdge);
       toggleEdge.getState();
       
       ActionListener printCommandListener = new ActionListener() {
     	  
 	      public void actionPerformed(ActionEvent event) {
 	    	 
 	    	  Collection<GraphElements.MyVertex> allVertices = g.getVertices();
 	    	 Comparator<GraphElements.MyVertex> comparator = new Comparator<GraphElements.MyVertex>() {
 	    	    public int compare(GraphElements.MyVertex c1, GraphElements.MyVertex c2) {
 	    	        return c1.getName().compareTo(c2.getName()); // use your logic
 	    	    }
 	    	};
 	    	  ArrayList<GraphElements.MyVertex> listVertices = new ArrayList<GraphElements.MyVertex>();
 	    	  listVertices.addAll(allVertices);
 	    	  Collections.sort(listVertices, comparator);
 	    	  cl = Metrics.clusteringCoefficients(g);
 	    		dikstra = new DijkstraDistance(g);

 	         distances = bob.averageDistances(g);

 	    	  for (int p = 0; p < listVertices.size();p++){
 	    		  GraphElements.MyVertex v = listVertices.get(p);
 	    		  tmp = dikstra.getDistanceMap(v);
 	    		//  System.out.println(tmp);
 	    		  to = commsMap.get(Integer.parseInt(v.getName()));
 	    		 // System.out.println(to);
 	    		  int sum = 0;
 	    		  if  (to!=null){Collection<Integer> toRecipients = to.values();
 	    		//  System.out.println(toRecipients);
 	    		  ArrayList<Integer> rec = new ArrayList<Integer>();
 	    		  rec.addAll(toRecipients);
 	    		  for (int l = 0; l < toRecipients.size();l++){
 	    			  sum = sum + rec.get(l);
 	    		  }
 	    		  }
 	    		 // System.out.println(sum);
 	    		  int sent = 0;
 	    		  if (!(sentlist.get(Integer.parseInt(v.getName()))==null)){
 	    			  sent = sentlist.get(Integer.parseInt(v.getName())).size();
 	    		  }
 	    		 Double dist = distances.transform(v);
 	    		 String diststr = "NaN";
					if (Double.isNaN(dist)){
						
					}
					else{
						dist = dist+1;
						diststr = df.format(dist);
					}
					//System.out.println(v.getName());
					//System.out.println(Integer.parseInt(v.getName()));
					int recievednum = recievedlist.get(Integer.parseInt(v.getName()));
					if (recievedlist.get(Integer.parseInt(v.getName()))==null){
						recievednum=0;
					}
					 double deg = g.degree(v);
 	    		//  System.out.println(v.getName().replace(" ","_")+", total_sentList= "+sent+", total_sent= "+sum+", total_recieved= "+recievednum+", degree= "+deg/20+", betweenness= "+ranker.getVertexRankScore(v)+", PageRank= "+pageRanker.getVertexScore(v)+", Markov= "+markovRanker.getVertexRankScore(v)+", HITS_authority= "+HitsRanker.getVertexScore(v).authority+", HITS_hub= "+HitsRanker.getVertexScore(v).hub+", weighted cliques= "+cliqueScores.get(v)+", cliques= "+cliqueScores2.get(v)+", avgDistance= "+(diststr)+", clusteringCoeff= "+cl.get(v));
 	    		  System.out.println(v.getName().replace(" ","_")+" "+sent+" "+recievednum+" "+deg/20+" "+ranker.getVertexRankScore(v)+" "+pageRanker.getVertexScore(v)+" "+markovRanker.getVertexRankScore(v)+" "+HitsRanker.getVertexScore(v).authority+" "+HitsRanker.getVertexScore(v).hub+" "+cliqueScores.get(v)+" "+cliqueScores2.get(v)+" "+(diststr)+" "+cl.get(v));

 	    	  }
 	      }
 	    };
 	    
 	   ActionListener printFileListener = new ActionListener() {
      	  
  	      public void actionPerformed(ActionEvent event) {
  	    	  System.out.println("BOO PRINTED");
  	      }
  	    };
        
  	    
       
 	   JMenu a = new JMenu("Print values");
       JMenuItem b = new JMenuItem("Console");
       JMenuItem c = new JMenuItem("To File");
       b.addActionListener(printCommandListener);
       c.addActionListener(printFileListener);
       mainMenu.add(a);
       a.add(b);
       a.add(c);
       
       
       JRadioButtonMenuItem birdButton = new JRadioButtonMenuItem("birdString");
       birdButton.setMnemonic(KeyEvent.VK_B);
       birdButton.setActionCommand("birdString");
       birdButton.setSelected(true);

       JRadioButtonMenuItem catButton = new JRadioButtonMenuItem("catString");
       catButton.setMnemonic(KeyEvent.VK_C);
       catButton.setActionCommand("catString");

       JRadioButtonMenuItem dogButton = new JRadioButtonMenuItem("dogString");
       dogButton.setMnemonic(KeyEvent.VK_D);
       dogButton.setActionCommand("dogString");
       

       JRadioButtonMenuItem rabbitButton = new JRadioButtonMenuItem("rabbitString");
       rabbitButton.setMnemonic(KeyEvent.VK_R);
       rabbitButton.setActionCommand("rabbitString");

       JRadioButtonMenuItem pigButton = new JRadioButtonMenuItem("pigString");
       pigButton.setMnemonic(KeyEvent.VK_P);
       pigButton.setActionCommand("pigString");

       //Group the radio buttons.
       ButtonGroup group = new ButtonGroup();
       group.add(birdButton);
       group.add(catButton);
       group.add(dogButton);
       group.add(rabbitButton);
       group.add(pigButton);
		final JToggleButton groupVertices = new JToggleButton("Group Clusters");

       final JSlider edgeBetweennessSlider = new JSlider(JSlider.HORIZONTAL);
       edgeBetweennessSlider.setBackground(Color.WHITE);
		edgeBetweennessSlider.setPreferredSize(new Dimension(280, 50));
		edgeBetweennessSlider.setPaintTicks(true);
		edgeBetweennessSlider.setMaximum(g.getEdgeCount());
		edgeBetweennessSlider.setMinimum(0);
		edgeBetweennessSlider.setValue(0);
		//System.out.println("Tick - "+g.getEdgeCount()/10);
		edgeBetweennessSlider.setMajorTickSpacing(g.getEdgeCount()/10 - g.getEdgeCount()/10%10);
		edgeBetweennessSlider.setPaintLabels(true);
		edgeBetweennessSlider.setPaintTicks(true);
		final JPanel eastControls = new JPanel();
		eastControls.setOpaque(true);
		eastControls.setLayout(new BoxLayout(eastControls, BoxLayout.Y_AXIS));
		eastControls.add(Box.createVerticalGlue());
		eastControls.add(edgeBetweennessSlider);
		final String COMMANDSTRING = "Edges removed for clusters: ";

		final String eastSize = COMMANDSTRING + edgeBetweennessSlider.getValue();

		final TitledBorder sliderBorder = BorderFactory.createTitledBorder(eastSize);
		
		vertexBorder.setTitleJustification(2);
		vertexBorder.setTitle("Vertex Details");
		eastControls.setBorder(sliderBorder);
		//eastControls.add(eastSize);
		eastControls.add(Box.createVerticalGlue());
		JButton scramble = new JButton("Restart");
		scramble.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Layout layout = vv.getGraphLayout();
				layout.setSize(preferredGraphSize);
				layout.initialize();
				Relaxer relaxer = vv.getModel().getRelaxer();
				if(relaxer != null) {
					relaxer.stop();
					relaxer.prerelax();
					relaxer.relax();
				}
			}

		});
		JPanel south = new JPanel();
		
		JPanel grid = new JPanel(new GridLayout(2,1));
		grid.add(scramble);
		grid.add(groupVertices);
		south.add(grid);
		east.setPreferredSize(new Dimension(200,220));
		south.add(eastControls);
		
		text.setText("Initialising");
		text.setMaximumSize(new Dimension(200,220));
		
		vertexDetail.setVisible(false);
		east.add(text);
		east.add(vertexDetail);
		frame.add(east,BorderLayout.EAST);
		east.setBackground(Color.white);
		east.setBorder(vertexBorder);
		
		JButton nextSlide = new JButton();
		JButton previousSlide = new JButton();
		nextSlide.setText("Next Vertex");
		nextSlide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Set<GraphElements.MyVertex> selected = vv.getPickedVertexState().getPicked();
				int total = selected.size();
				currentVertex = (currentVertex+1)%total;
				if (currentVertex ==0){
					currentVertex = total;
				}
				//System.out.println(currentVertex);
				ArrayList<GraphElements.MyVertex> vertices = new ArrayList<GraphElements.MyVertex>(selected);
				
				updatePanel(vertices, currentVertex);
				}

		});
		previousSlide.setText("Previous Vertex");
		previousSlide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Set<GraphElements.MyVertex> selected = vv.getPickedVertexState().getPicked();
				int total = selected.size();
				currentVertex = (currentVertex + total -1)%total;
				if (currentVertex ==0){
					currentVertex = total;
				}
			//	System.out.println(currentVertex);
				ArrayList<GraphElements.MyVertex> vertices = new ArrayList<GraphElements.MyVertex>(selected);

				updatePanel(vertices,currentVertex);
			}

		});
		
		grid2.add(nextSlide);
		grid2.add(previousSlide);
		grid2.setVisible(false);
		
		east.add(grid2,BorderLayout.SOUTH);
		
		frame.add(south, BorderLayout.SOUTH);
		//frame.add(frameSide,BorderLayout.EAST);
		groupVertices.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
					try {
						clusterAndRecolor(layout, edgeBetweennessSlider.getValue(), 
								similarColors, e.getStateChange() == ItemEvent.SELECTED);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
					vv.repaint();
			}});


		clusterAndRecolor(layout, 0, similarColors, groupVertices.isSelected());

		edgeBetweennessSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				if (!source.getValueIsAdjusting()) {
					int numEdgesToRemove = source.getValue();
					try {
						clusterAndRecolor(layout, numEdgesToRemove, similarColors,
								groupVertices.isSelected());
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
					sliderBorder.setTitle(
						COMMANDSTRING + edgeBetweennessSlider.getValue());
					eastControls.repaint();
					vv.validate();
					vv.repaint();
				}
			}
		});
       
       ActionListener viewMenuListener = new ActionListener() {
     	  
 	      public void actionPerformed(ActionEvent event) {
 	    	//  System.out.println(event.getActionCommand());
 	    	  if (event.getActionCommand()=="catString"){
					 //layout = new FRLayout(sgv.g);	
					// vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT);
					 layout = new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new FRLayout<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g));
					 layout.setSize(preferredGraphSize);
					 vv.setGraphLayout(layout);
					
					 try {
							clusterAndRecolor(layout, edgeBetweennessSlider.getValue(), 
										similarColors, groupVertices.isSelected());
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							

							e.printStackTrace();
						}
					
					vv.updateUI();
 	    	  }
 	    	  if (event.getActionCommand() == "rabbitString"){
					// layout = new ISOMLayout(sgv.g);	
					// vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT);
					layout = new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new ISOMLayout<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g)); 
 	    		  layout.setSize(preferredGraphSize);
					vv.setGraphLayout(layout);
 	    		 try {
						clusterAndRecolor(layout, edgeBetweennessSlider.getValue(), 
									similarColors, groupVertices.isSelected());
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block

						e.printStackTrace();
					}
				 
				vv.updateUI();
 	    	  }
 	    	 if (event.getActionCommand() == "dogString"){
					// layout = new ISOMLayout(sgv.g);	
					// vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT);
 	    		layout = new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new KKLayout<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g)); 
				layout.setSize(preferredGraphSize);	
 	    		vv.setGraphLayout(layout);
					 try {
						clusterAndRecolor(layout, edgeBetweennessSlider.getValue(), 
									similarColors, groupVertices.isSelected());
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block

						e.printStackTrace();
					}
					vv.updateUI();
	    	  }
 	    	if (event.getActionCommand() == "pigString"){
				// layout = new ISOMLayout(sgv.g);	
				// vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT);
	    		layout = new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new FRLayout2<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g)); 
				layout.setSize(preferredGraphSize);
	    		vv.setGraphLayout(layout);
				 try {
					clusterAndRecolor(layout, edgeBetweennessSlider.getValue(), 
								similarColors, groupVertices.isSelected());
				} catch (InterruptedException e) {
				//	vv.CENTER_ALIGNMENT;
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				
				vv.updateUI();
    	  }
 	      }
 	    };

       //Register a listener for the radio buttons.
       birdButton.addActionListener(viewMenuListener);
       catButton.addActionListener(viewMenuListener);
       dogButton.addActionListener(viewMenuListener);
       rabbitButton.addActionListener(viewMenuListener);
       pigButton.addActionListener(viewMenuListener);
       
      // viewMenu.add(group);
       viewMenu.add(birdButton);
       viewMenu.add(catButton);
       viewMenu.add(dogButton);
       viewMenu.add(rabbitButton);
       viewMenu.add(pigButton);
       
       ActionListener edgeMenuListener = new ActionListener() {
    	  
    	      public void actionPerformed(ActionEvent event) {
    	    	  
    	    //	  toggleEdges();
    	    	//  vv.updateUI();
    	        AbstractButton aButton = (AbstractButton) event.getSource();
    	        boolean selected = aButton.getModel().isSelected();
    	        String newLabel;
    	        Icon newIcon;
    	        if (selected) {
					 vv.getRenderContext().setEdgeLabelTransformer(new ToStringLabeller());
				//	 FRlayout = new FRLayout(sgv.g);	
					 
				//	 vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.FRLAYOUT);
					 vv.updateUI();

    	          newLabel = "Edges";
    	        } else {
    	          newLabel = "Edges";
					 vv.getRenderContext().setEdgeLabelTransformer(blankLabelTransformer);
					
					
					//layout2.setTranslate(g.getVertices()., );
					// layout2.translate(tmp.getX(), tmp.getY());
					 vv.updateUI();
    	        }
    	        aButton.setText(newLabel);
    	      }
    	    };
    	    ActionListener VertexMenuListener = new ActionListener() {
    	    	  
      	      public void actionPerformed(ActionEvent event) {
      	    	  
      	    //	  toggleEdges();
      	    	  vv.updateUI();
      	        AbstractButton aButton = (AbstractButton) event.getSource();
      	        boolean selected = aButton.getModel().isSelected();
      	        String newLabel;
      	        Icon newIcon;
      	        if (selected) {
  					 vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
  					 vv.updateUI();
      	          newLabel = "Vertices";
      	        } else {
      	          newLabel = "Vertices";
  					 vv.getRenderContext().setVertexLabelTransformer(blankLabelTransformer);

      	          vv.updateUI();
      	        }
      	        aButton.setText(newLabel);
      	      }
      	    };
       
       toggleEdge.addActionListener(edgeMenuListener);
       toggleVertices.addActionListener(VertexMenuListener);
       toggle.add(toggleVertices);
      // menu.setMnemonic(KeyEvent.VK_A);
       display.add(toggle);
   //    menu.getAccessibleContext().setAccessibleDescription(
            //   "The only menu in this program that has menu items");
       menuBar.add(mainMenu);
       menuBar.add(viewMenu);
       menuBar.add(display);
       //frame.add(pnlButton, BorderLayout.SOUTH);
       EditingModalGraphMouse gm = new EditingModalGraphMouse(vv.getRenderContext(), 
               GraphElements.MyVertexFactory.getInstance(),
              GraphElements.MyEdgeFactory.getInstance()); 
       gm.setMode(ModalGraphMouse.Mode.TRANSFORMING);
       PopupVertexEdgeMenuMousePlugin myPlugin = new PopupVertexEdgeMenuMousePlugin();
       gm.remove(gm.getPopupEditingPlugin());
       gm.add(myPlugin);
       vv.setGraphMouse(gm);
       JPopupMenu edgeMenu = new MyMouseMenus.EdgeMenu(frame);
       JPopupMenu vertexMenu = new MyMouseMenus.VertexMenu();
       myPlugin.setEdgePopup(edgeMenu);
       myPlugin.setVertexPopup(vertexMenu);
       vv.getRenderContext().setVertexFillPaintTransformer(MapTransformer.<GraphElements.MyVertex,Paint>getInstance(vertexPaints));
		vv.getRenderContext().setVertexDrawPaintTransformer(new Transformer<GraphElements.MyVertex,Paint>() {
			public Paint transform(GraphElements.MyVertex v) {
				Set<GraphElements.MyVertex> pickedVertices = vv.getPickedVertexState().getPicked();
				ArrayList<GraphElements.MyVertex> selected = new ArrayList<GraphElements.MyVertex>(pickedVertices);
				if(pickedVertices.size()==0) {
					text.setText("Select Vertices ...");						
					grid2.setVisible(false);
					vertexDetail.setVisible(false);
					currentVertex=1;
					vertexBorder.setTitle("Vertex Details");
					east.updateUI();
					return Color.BLACK;
					} 
				else if (vv.getPickedVertexState().isPicked(v)){
					
					if (pickedVertices.size()>29){
						grid2.setVisible(true);
					}
					vertexDetail.setVisible(true);
					text.setText(v.getName());
					Set<GraphElements.MyVertex> setV = new HashSet<GraphElements.MyVertex>();
				       setV.addAll(g.getVertices());
				       ranker = new BetweennessCentrality<GraphElements.MyVertex,GraphElements.MyEdge>(g,true,false);
				       markovRanker = new MarkovCentrality<GraphElements.MyVertex,GraphElements.MyEdge>((DirectedGraph<GraphElements.MyVertex, GraphElements.MyEdge>) g,setV);
				       markovRanker.setRemoveRankScoresOnFinalize(false);
				       ranker.setRemoveRankScoresOnFinalize(false);
				       ranker.evaluate();
				       pageRanker = new PageRank<GraphElements.MyVertex, GraphElements.MyEdge>(g,0.15);
				       System.out.println("TOLERANCE = "+pageRanker.getTolerance());				      
				       pageRanker.initialize();
				       pageRanker.evaluate();
				       HitsRanker = new HITS<GraphElements.MyVertex,GraphElements.MyEdge>(g);
				       HitsRanker.initialize();
				       HitsRanker.evaluate();
				       markovRanker.evaluate();
					updatePanel(selected,1);
					
					return Color.cyan;
				}

					else{
						//other unselected vertices
						return Color.BLACK;
					}
				
			}
		});
		
		
		vv.getRenderContext().setArrowDrawPaintTransformer(MapTransformer.<GraphElements.MyEdge,Paint>getInstance(edgePaints));
		vv.getRenderContext().setArrowFillPaintTransformer(MapTransformer.<GraphElements.MyEdge,Paint>getInstance(edgePaints));
		vv.getRenderContext().setEdgeDrawPaintTransformer(MapTransformer.<GraphElements.MyEdge,Paint>getInstance(edgePaints));

		vv.getRenderContext().setEdgeStrokeTransformer(new Transformer<GraphElements.MyEdge,Stroke>() {
               protected final Stroke THIN = new BasicStroke(1);
               protected final Stroke THICK= new BasicStroke(1);
               public Stroke transform(GraphElements.MyEdge e)
               {
                   Paint c = edgePaints.get(e);
                   if (c == Color.LIGHT_GRAY)
                       return THIN;
                   else 
                       return THICK;
               }
           });
       JMenu modeMenu = gm.getModeMenu();
       modeMenu.setText("Mouse Mode");
       menuBar.add(modeMenu);
       frame.setJMenuBar(menuBar);
       modeMenu.setPreferredSize(new Dimension(120,20));
       gm.setMode(ModalGraphMouse.Mode.TRANSFORMING);
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       vv.setSize(preferredGraphSize);
     //  vv.getGraphLayout().getSize();
       ScrollPane scrollPane=new ScrollPane();
       scrollPane.setSize(preferredGraphSize);
       scrollPane.add(vv);
       frame.getContentPane().add(scrollPane);
       Set<GraphElements.MyVertex> setV = new HashSet<GraphElements.MyVertex>();
       setV.addAll(g.getVertices());
       ranker = new BetweennessCentrality<GraphElements.MyVertex,GraphElements.MyEdge>(g,true,false);
       markovRanker = new MarkovCentrality<GraphElements.MyVertex,GraphElements.MyEdge>((DirectedGraph<GraphElements.MyVertex, GraphElements.MyEdge>) g,setV);
       markovRanker.setRemoveRankScoresOnFinalize(false);
       ranker.setRemoveRankScoresOnFinalize(false);
       pageRanker = new PageRank<GraphElements.MyVertex, GraphElements.MyEdge>(g,0.15);
       pageRanker.initialize();
       pageRanker.evaluate();
       deleteButton.setVisible(false);
       ActionListener deleteAction = new ActionListener() {
       	  
   	      public void actionPerformed(ActionEvent event) {
   	    	  System.out.println("BOO PRINTED");
   	      }
   	    };
       deleteButton.addActionListener(deleteAction);
       HitsRanker = new HITS<GraphElements.MyVertex,GraphElements.MyEdge>(g);
       HitsRanker.initialize();
       HitsRanker.evaluate();
       markovRanker.evaluate();
       InputMap im = vv.getInputMap();
       KeyListener keyl = new KeyListener() {
           public void keyPressed(KeyEvent e) {
        	   if (e.getKeyCode() == 127){
        		   System.out.println("DELETED ::::");
        		   Object[] options = {"Yes, please",
                           "No, thanks"};
        		   Set<GraphElements.MyVertex> vert = vv.getPickedVertexState().getPicked();
        		   int n = JOptionPane.showOptionDialog(frame,
        				    "Are you sure that you want to delete "+vert.size()
        				    + " vertices?",
        				    "Confirm the deletion",
        				    JOptionPane.YES_NO_OPTION,
        				    JOptionPane.QUESTION_MESSAGE,
        				    null,
        				    options,
        				    options[1]);
        		  System.out.println("value = "+n);
        		  if (n==0){
        		   for (GraphElements.MyVertex v : vert){
        			   g.removeVertex(v);
        			   vv.updateUI();
        			   east.updateUI();
        			   updatePanel(new ArrayList<GraphElements.MyVertex>(vert),0);
        		   }
        		  }
        		  
        		   
        		   }
           }

           public void keyReleased(KeyEvent e) {
           }

           public void keyTyped(KeyEvent e) {
           }

       };
       vv.addKeyListener(keyl);
       
       
             
       ranker.setRemoveRankScoresOnFinalize(false);
   	ranker.evaluate();
   	ranker.printRankings(true, true);
   	ranker.getEdgeRankScores();
    for (GraphElements.MyEdge e : g.getEdges()){
 	   Pair<GraphElements.MyVertex> end = g.getEndpoints(e);
 	   
 	   System.out.println("Edges from "+"("+e.getName()+") = "+"<"+end.getFirst().getName()+","+end.getSecond().getName()+"> of weight "+e.getWeight());
    }
       resizeNodes(ranker);
       ranker.getVertexRankScore(vertexArray[1]);
  /*     for (int i = 0; i < vertexArray.length; i++){ 
    	   if(!duplicate(i)){
       Collection<GraphElements.MyEdge> neighbourEdge = g.getIncidentEdges(vertexArray[i]);
       System.out.println();
       System.out.println(vertexArray[i].getName());
        System.out.println("neighbours = "+neighbourEdge.size()+ "("+i+") for "+vertexArray[i].getName()+", degree = "+g.degree(vertexArray[i]));
        
      for (GraphElements.MyEdge e : neighbourEdge){
    	   Pair<GraphElements.MyVertex> end = g.getEndpoints(e);
    	   System.out.println("Edges from "+vertexArray[i].getName()+"("+e.getName()+") = "+"<"+end.getFirst().getName()+","+end.getSecond().getName()+"> of weight "+e.getWeight());
       }
       }
       }*/
   //    EdgeBetweennessClusterer<>
       int numberOfEdgesToRemove = 0;
      // EdgeBetweennessClusterer bc = new EdgeBetweennessClusterer(numberOfEdgesToRemove);
     //  Set clusterSet = bc.transform(g);
    //   List edges = bc.getEdgesRemoved();
       
   //   EdgeBetweennessClusterer<Collection<MyEdge>,g>  bc = new EdgeBetweennessClusterer<g.getEdges(),g.getVertices()>(1);
     //  Set<Set<MyNode>> sets = EBC1.transform(g);
       
     //  System.out.println(vv.size());
       //frame.add(menuBar, BorderLayout.NORTH);
       frame.pack();
       frame.setVisible(true);     
      // for (int i = 0;i<vertexArray.length;i++){
    //	   System.out.println("degree for "+vertexArray[i].getName()+" = "+g.degree(vertexArray[i]));
    	   //g.
     //  }
      
       
       
    }
    
    protected static void updatePanel(ArrayList<GraphElements.MyVertex> vertices,
			int currentVertex2) {
    	if (currentVertex2 ==0){
    		text.setText("Select Vertices ...");						
			grid2.setVisible(false);
			vertexDetail.setVisible(false);
			currentVertex=1;
			vertexBorder.setTitle("Vertex Details");
			east.updateUI();
    	}
    	else{
    	//System.out.println(layout.getSize());
    	GraphElements.MyVertex v=vertices.get(currentVertex - 1);
    	 cl = Metrics.clusteringCoefficients(g);
  		dikstra = new DijkstraDistance(g);

       distances = bob.averageDistances(g);
		  tmp = dikstra.getDistanceMap(v);
		//  System.out.println(tmp);
		  to = commsMap.get(Integer.parseInt(v.getName()));
		  int sum = 0;
		  if  (to!=null){Collection<Integer> toRecipients = to.values();
		  ArrayList<Integer> rec = new ArrayList<Integer>();
		  rec.addAll(toRecipients);
		  for (int l = 0; l < toRecipients.size();l++){
			  sum = sum + rec.get(l);
		  }
		  }
		  int sent = 0;
		  if (!(sentlist.get(Integer.parseInt(v.getName()))==null)){
			  sent = sentlist.get(Integer.parseInt(v.getName())).size();
		  }
		 Double dist = distances.transform(v);
		 String diststr = "NaN";
			if (Double.isNaN(dist)){
			//	dist = (double)0;
			}
			else{
				dist = dist+1;
				diststr = df.format(dist);
			}
		 // System.out.println(v.getName().replace(" ","_")+", total_sentList= "+sent+", total_sent= "+sum+", total_recieved= "+recievedlist.get(updatedNames.indexOf(v.getName()))+", degree= "+g.degree(v)+", betweenness= "+ranker.getVertexRankScore(v)+", PageRank= "+pageRanker.getVertexScore(v)+", Markov= "+markovRanker.getVertexRankScore(v)+", HITS_authority= "+HitsRanker.getVertexScore(v).authority+", HITS_hub= "+HitsRanker.getVertexScore(v).hub+", cliques= "+cliqueScores.get(v)+", avgDistance= "+(diststr)+", clusteringCoeff= "+cl.get(v));
		  Integer recTotal = recievedlist.get(Integer.parseInt(v.getName()));
		  if (recTotal == null){
			  recTotal = 0;
		  }
		 vertexBorder.setTitle(Integer.toString(currentVertex)+" of "+vertices.size());
		vertexDetail.setText("total sent = "+sent+"\n"+"total recieved = "+recTotal+
				"\n"+"degree = "+g.degree(v)+"\n"+"betweenness= "+
				df.format(ranker.getVertexRankScore(v))+"\n"+"PageRank= "+
		 df.format(pageRanker.getVertexScore(v))+"\n"+"Markov= "+
				df.format(markovRanker.getVertexRankScore(v))+"\n"+"HITS_authority= "+
		 df.format(HitsRanker.getVertexScore(v).authority)+"\n"+"HITS_hub= "+
				df.format(HitsRanker.getVertexScore(v).hub)+"\n"+"cliques= "+cliqueScores.get(v)+"\n"+"("+cliqueScores2.get(v)+")"+"\n"+"avgDistance= "+(diststr)+"\n"+"clusteringCoeff= "+df.format(cl.get(v)));
		text.setText(v.getName());
		east.updateUI();	
    	}
	}

	private static void resizeNodes(final BetweennessCentrality ranker) {
    	
    	 Transformer<GraphElements.MyVertex, Shape> vertexSize2 = new Transformer<GraphElements.MyVertex,Shape>(){
    	        //    private AbstractRanker ranker;

    				public Shape transform(GraphElements.MyVertex i){
    					HitsRanker = new HITS<GraphElements.MyVertex,GraphElements.MyEdge>(g);
 				       HitsRanker.initialize();
 				       HitsRanker.evaluate();
    	                Ellipse2D circle = new Ellipse2D.Double(-10, -10, 20, 20);
    	                Ellipse2D circlemini = new Ellipse2D.Double(-2,-2,3,3);
    	              //  System.out.println("Rank score for "+i.getName()+" = "+ranker.getVertexRankScore(i));
    	                // in this case, the vertex is twice as large
    	                if(g.degree(i)==0) return circlemini;
    	                
    	                else return AffineTransform.getScaleInstance(Math.pow(2,HitsRanker.getVertexScore(i).authority*10)/5, Math.pow(2,HitsRanker.getVertexScore(i).authority*10)/5).createTransformedShape(circle);
    	            }
    	        };
    	        vv.getRenderContext().setVertexShapeTransformer(vertexSize2);		
	}

	public static void clusterAndRecolor(AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge> layout,
    		int numEdgesToRemove,
    		Color[] colors, boolean groupClusters) throws InterruptedException {

    		
    		Graph<GraphElements.MyVertex, GraphElements.MyEdge> g = layout.getGraph();
            layout.removeAll();
            vv.repaint();
            vv.updateUI();
     //       Thread.sleep(1910);
           // jjj
    		EdgeBetweennessClusterer<GraphElements.MyVertex, GraphElements.MyEdge> clusterer =
    			new EdgeBetweennessClusterer<GraphElements.MyVertex, GraphElements.MyEdge>(numEdgesToRemove);
    		Set<Set<GraphElements.MyVertex>> clusterSet = clusterer.transform(g);
    		List<GraphElements.MyEdge> edges = clusterer.getEdgesRemoved();
    		

    		int i = 0;
    		//Set the colors of each node so that each cluster's vertices have the same color
    		for (Iterator<Set<GraphElements.MyVertex>> cIt = clusterSet.iterator(); cIt.hasNext();) {

    			Set<GraphElements.MyVertex> vertices = cIt.next();
    			Color c = colors[i % colors.length];

    			colorCluster(vertices, c);
    			if(groupClusters == true) {
    				groupCluster(layout, vertices);
    			}
    			i++;
    		}
    		for (GraphElements.MyEdge e : g.getEdges()) {

    			if (edges.contains(e)) {
    				edgePaints.put(e, Color.lightGray);
    			} else {
    				edgePaints.put(e, Color.black);
    			}
    		}

    	}

    
    
    	private static void colorCluster(Set<GraphElements.MyVertex> vertices, Color c) {
    		for (GraphElements.MyVertex v : vertices) {
    			vertexPaints.put(v, c);
    		}
    		vv.repaint();
    	}
    	
    	public Integer sum(List<Integer> list) {
    	     Integer sum= 0; 
    	     for (Integer i:list)
    	         sum = sum + i;
    	     return sum;
    	}

    	public String properCase (String inputVal) {
    	//	System.out.println("Processing word "+inputVal +" of length "+inputVal.length());
    	    // Empty strings should be returned as-is.

    	    if (inputVal.length() == 0) return "";

    	    // Strings with only one character uppercased.

    	    if (inputVal.length() == 1) return inputVal.toUpperCase();

    	    // Otherwise uppercase first letter, lowercase the rest.

    	    return inputVal.substring(0,1).toUpperCase()
    	        + inputVal.substring(1).toLowerCase();
    	}
    	
    	private static void groupCluster(AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge> layout, Set<GraphElements.MyVertex> vertices) {
    	//	System.out.println("GROUPING");
    		if(vertices.size() < layout.getGraph().getVertexCount()) {
    			Point2D center = layout.transform(vertices.iterator().next());
    			Graph<GraphElements.MyVertex, GraphElements.MyEdge> subGraph = SparseMultigraph.<GraphElements.MyVertex, GraphElements.MyEdge>getFactory().create();
    			for(GraphElements.MyVertex v : vertices) {
    				subGraph.addVertex(v);
    			}
    			
    			Layout<GraphElements.MyVertex, GraphElements.MyEdge> subLayout = 
    				new CircleLayout<GraphElements.MyVertex, GraphElements.MyEdge>(subGraph);
    			subLayout.setInitializer(vv.getGraphLayout());
    			subLayout.setSize(preferredGraphSize);

    			layout.put(subLayout,center);
    			
    			vv.repaint();
    		}
    	}

    	
    
}