import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class ImportMain {
	//private static int totalMessages = 517423;
//	private static String[] msgIds = new String[totalMessages];
//	private static String[] topics = new String[totalMessages];
//	private static String[] dates = new String[totalMessages];
//	private static String[] contents = new String[totalMessages];
	static Map<String, ArrayList<ArrayList<String>>> commsMap = new HashMap<String,ArrayList<ArrayList<String>>>();
	static SortedSet<String> AllRecipients =  new TreeSet<String>();
	static List<String> completeList = new ArrayList<String>();
	static Set<String> senders = commsMap.keySet();
	//static Map<String,ArrayList<String>> 
	private static Map<String,List<String>> msgLink = new HashMap<String, List<String>>();
	private static int fileNum = 0;
	private static Map<String,String> emailAliasLink = new HashMap<String,String>(); 
	private static final String[] validEmails = {"phillip.allen@enron.com", "e..taylor@enron.com", "l..taylor@enron.com", "e.taylor@enron.com", "liz.taylor@enron.com", "joannie.williamson@enron.com", "williamson@enron.com", "darrell.schoolcraft@enron.com", "brenda.whitehead@enron.com", "clint.dean@enron.com","john.zufferli@enron.com", "andy.zipper@enron.com", "bill.williams@enron.com", "susan.pereira@enron.com", "barry.tycholiz@enron.com", "jason.williams@enron.com", "paul.lucci@enron.com", "john.hodge@enron.com", "susan.scott@enron.com", "steven.merriss@enron.com", "stanley.horton@enron.com", "jeff.skilling@enron.com", "richard.shapiro@enron.com", "cara.semperger@enron.com", "jim.schwieger@enron.com", "stacey.white@enron.com", "hunter.shively@enron.com", "kevin.presto@enron.com", "steven.south@enron.com", "greg.whalley@enron.com", "scott.neal@enron.com", "michelle.lokay@enron.com", "martin.cuilla@enron.com", "brad.mckay@enron.com", "jonathan.mckay@enron.com", "danny.mccarty@enron.com", "mark.haedicke@enron.com", "marie.heard@enron.com", "jane.tholt@enron.com", "phillip.love@enron.com", "john.forney@enron.com", "rob.gay@enron.com", "patrice.mims@enron.com", "randall.gay@enron.com", "margaret.carson@enron.com","mike.carson@enron.com", "kate.symes@enron.com", "louise.kitchen@enron.com", "kenneth.lay@enron.com", "vince.kaminski@enron.com", "john.lavorato@enron.com", "jeff.dasovich@enron.com", "fletcher.sturm@enron.com", "steven.kean@enron.com", "daren.farmer@enron.com", "kim.ward@enron.com", "judy.hernandez@enron.com", "rod.hayslett@enron.com", "drew.fossum@enron.com", "peter.keavey@enron.com", "larry.campbell@enron.com", "sandra.brawner@enron.com", "eric.bass@enron.com", "stacy.dickson@enron.com", "chris.dorland@enron.com", "david.delainey@enron.com", "darron.c.giron@enron.com", "james.steffes@enron.com", "dana.davis@enron.com", "don.baughman@enron.com", "shelley.corman@enron.com", "andrew.lewis@enron.com", "rick.buy@enron.com", "lynn.blair@enron.com", "albert.meyers@enron.com", "sally.beck@enron.com", "susan.bailey@enron.com", "robert.badeer@enron.com", "richard.sanders@enron.com", "bani.arora@enron.com", "harry.arora@enron.com", "john.arnold@enron.com", "mark.taylor@enron.com", "matt.smith@enron.com", "jeffrey.shankman@enron.com", "thomas.martin@enron.com", "carol.clair@enron.com", "robert.benson@enron.com", "michelle.cash@enron.com", "monika.causholli@enron.com", "sean.crandall@enron.com", "pete.davis@enron.com", "james.derrick@enron.com", "tom.donohoe@enron.com", "lindy.donoho@enron.com", "frank.ermis@enron.com", "mary.fischer@enron.com", "lisa.gang@enron.com", "tracy.geaccone@enron.com", "chris.germany@enron.com", "doug.gilbert_smith@enron.com", "john.griffith@enron.com", "mike.grigsby@enron.com", "mark.guzman@enron.com", "mary.hain@enron.com","j.harris@enron.com", "steven.harris@enron.com", "scott.hendrickson@enron.com", "keith.holst@enron.com", "kevin.hyatt@enron.com", "dan.hyvl@enron.com", "tana.jones@enron.com", "kam.keiser@enron.com", "jeff.king@enron.com", "tori.kuykendall@enron.com", "matthew.lenhart@enron.com", "eric.linder@enron.com", "teb.lokey@enron.com", "mike.maggi@enron.com", "kay.mann@enron.com", "larry.may@enron.com", "mark.mcconnell@enron.com", "mike.mcconnell@enron.com", "errol.mclaughlin@enron.com", "matt.motley@enron.com", "gerald.nemec@enron.com", "stephanie.panus@enron.com", "joe.parks@enron.com", "debra.perlingiere@enron.com", "vladi.pimenov@enron.com", "phillip.platter@enron.com", "joe.quenet@enron.com", "dutch.quigley@enron.com", "bill.rapp@enron.com", "jay.reitmeyer@enron.com", "cooper.richey@enron.com", "andrea.ring@enron.com", "richard.ring@enron.com", "robin.rodrigue@enron.com", "benjamin.rogers@enron.com", "kevin.ruscitti@enron.com", "elizabeth.sager@enron.com", "eric.saibi@enron.com", "holden.salisbury@enron.com", "monique.sanchez@enron.com", "sara.shackleton@enron.com", "diana.scholtes@enron.com", "ryan.slinger@enron.com", "geir.solberg@enron.com", "theresa.staab@enron.com", "joe.stepenovitch@enron.com", "chris.stokley@enron.com", "geoff.storey@enron.com", "mike.swerzbin@enron.com", "paul.thomas@enron.com", "judy.townsend@enron.com", "kimberly.watson@enron.com", "charles.weldon@enron.com", "grep.whalley@enron.com", "mark.whitt@enron.com", "jason.wolfe@enron.com", "paul.y'barbo@enron.com", "a..allen@enron.com@enron.com", "zufferli@enron.com", "zipper@enron.com", "williams@enron.com", "w..pereira@enron.com", "tycholiz@enron.com", "trading <.williams@enron.com>@enron.com", "t..lucci@enron.com", "t..hodge@enron.com", "susan.m.scott@enron.com", "steven.merris@enron.com", "stan.horton@enron.com", "skilling@enron.com", "shapiro@enron.com", "semperger@enron.com", "schwieger@enron.com", "s..white@enron.com", "s..shively@enron.com", "presto@enron.com", "p..south@enron.com", "office.whalley@enron.com", "neal@enron.com", "michele.lokay@enron.com", "mcuilla@enron.com", "mckay@enron.com", "mckay@enron.com", "mccarty@enron.com", "mark.e.haedicke@enron.com", "marie_heard@enron.com", "m..tholt@enron.com", "m..love@enron.com", "m..forney@enron.com", "l..mims@enron.com", "l..gay@enron.com", "l..carson@enron.com", "ksymes@enron.com", "kitchen@enron.com", "ken.lay_.chairman.of.the.board@enron.com", "kaminski@enron.com", "john.j.lavorato@enron.com", "jeff_dasovich@enron.com", "j..sturm@enron.com", "j..kean@enron.com", "j..farmer@enron.com", "houston <.ward@enron.com>@enron.com", "hernandez@enron.com", "hayslett@enron.com", "fossum@enron.com", "f..keavey@enron.com", "f..campbell@enron.com", "f..brawner@enron.com", "ebass@enron.com", "e..dickson@enron.com", "dorland@enron.com", "dave.delainey@enron.com", "darron.giron@enron.com", "d..steffes@enron.com", "d..davis@enron.com", "d..baughman@enron.com", "corman@enron.com", "c..lewis@enron.com", "buy@enron.com", "blair.lichtenwalter@enron.com", "bert.meyers@enron.com", "beck@enron.com", "bailey@enron.com", "badeer@enron.com", "b..sanders@enron.com", "arora@enron.com", "arnold@enron.com", "a.taylor@enron.com", "a..smith@enron.com", "a..shankman@enron.com", "e..haedicke@enron.com", "c..giron@enron.com", "allen@enron.com", "a..martin@enron.com", "williams@enron.com", "whalley@enron.com", "w..delainey@enron.com", "vince.j.kaminski@enron.com", "steffes@enron.com", "stan.horton.@enron.com", "shively@enron.com", "phillip.m.love@enron.com", "peter.f.keavey@enron.com", "m..presto@enron.com", "lavorato@enron.com", "ken.lay_@enron.com", "forney@enron.com", "delainey@enron.com", "j.kaminski@enron.com", "horton@enron.com", "ken.lay@enron.com", "j..kaminski@enron.com"};
	private static Set<String> validEmailList = new HashSet<String>(Arrays.asList(validEmails));
	private static String[] stopwords = {"enron", "a", "as", "able", "about", "above", "according", "accordingly", "across", "actually", "after", "afterwards", "again", "against", "aint", "all", "allow", "allows", "almost", "alone", "along", "already", "also", "although", "always", "am", "among", "amongst", "an", "and", "another", "any", "anybody", "anyhow", "anyone", "anything", "anyway", "anyways", "anywhere", "apart", "appear", "appreciate", "appropriate", "are", "arent", "around", "as", "aside", "ask", "asking", "associated", "at", "available", "away", "awfully", "be", "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "behind", "being", "believe", "below", "beside", "besides", "best", "better", "between", "beyond", "both", "brief", "but", "by", "cmon", "cs", "came", "can", "cant", "cannot", "cant", "cause", "causes", "certain", "certainly", "changes", "clearly", "co", "com", "come", "comes", "concerning", "consequently", "consider", "considering", "contain", "containing", "contains", "corresponding", "could", "couldnt", "course", "currently", "definitely", "described", "despite", "did", "didnt", "different", "do", "does", "doesnt", "doing", "dont", "done", "down", "downwards", "during", "each", "edu", "eg", "eight", "either", "else", "elsewhere", "enough", "entirely", "especially", "et", "etc", "even", "ever", "every", "everybody", "everyone", "everything", "everywhere", "ex", "exactly", "example", "except", "far", "few", "ff", "fifth", "first", "five", "followed", "following", "follows", "for", "former", "formerly", "forth", "four", "from", "further", "furthermore", "get", "gets", "getting", "given", "gives", "go", "goes", "going", "gone", "got", "gotten", "greetings", "had", "hadnt", "happens", "hardly", "has", "hasnt", "have", "havent", "having", "he", "hes", "hello", "help", "hence", "her", "here", "heres", "hereafter", "hereby", "herein", "hereupon", "hers", "herself", "hi", "him", "himself", "his", "hither", "hopefully", "how", "howbeit", "however", "i", "id", "ill", "im", "ive", "ie", "if", "ignored", "immediate", "in", "inasmuch", "inc", "indeed", "indicate", "indicated", "indicates", "inner", "insofar", "instead", "into", "inward", "is", "isnt", "it", "itd", "itll", "its", "its", "itself", "just", "keep", "keeps", "kept", "know", "knows", "known", "last", "lately", "later", "latter", "latterly", "least", "less", "lest", "let", "lets", "like", "liked", "likely", "little", "look", "looking", "looks", "ltd", "mainly", "many", "may", "maybe", "me", "mean", "meanwhile", "merely", "might", "more", "moreover", "most", "mostly", "much", "must", "my", "myself", "name", "namely", "nd", "near", "nearly", "necessary", "need", "needs", "neither", "never", "nevertheless", "new", "next", "nine", "no", "nobody", "non", "none", "noone", "nor", "normally", "not", "nothing", "novel", "now", "nowhere", "obviously", "of", "off", "often", "oh", "ok", "okay", "old", "on", "once", "one", "ones", "only", "onto", "or", "other", "others", "otherwise", "ought", "our", "ours", "ourselves", "out", "outside", "over", "overall", "own", "particular", "particularly", "per", "perhaps", "placed", "please", "plus", "possible", "presumably", "probably", "provides", "que", "quite", "qv", "rather", "rd", "re", "really", "reasonably", "regarding", "regardless", "regards", "relatively", "respectively", "right", "said", "same", "saw", "say", "saying", "says", "second", "secondly", "see", "seeing", "seem", "seemed", "seeming", "seems", "seen", "self", "selves", "sensible", "sent", "serious", "seriously", "seven", "several", "shall", "she", "should", "shouldnt", "since", "six", "so", "some", "somebody", "somehow", "someone", "something", "sometime", "sometimes", "somewhat", "somewhere", "soon", "sorry", "specified", "specify", "specifying", "still", "sub", "such", "sup", "sure", "ts", "take", "taken", "tell", "tends", "th", "than", "thank", "thanks", "thanx", "that", "thats", "thats", "the", "their", "theirs", "them", "themselves", "then", "thence", "there", "theres", "thereafter", "thereby", "therefore", "therein", "theres", "thereupon", "these", "they", "theyd", "theyll", "theyre", "theyve", "think", "third", "this", "thorough", "thoroughly", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "took", "toward", "towards", "tried", "tries", "truly", "try", "trying", "twice", "two", "un", "under", "unfortunately", "unless", "unlikely", "until", "unto", "up", "upon", "us", "use", "used", "useful", "uses", "using", "usually", "value", "various", "very", "via", "viz", "vs", "want", "wants", "was", "wasnt", "way", "we", "wed", "well", "were", "weve", "welcome", "well", "went", "were", "werent", "what", "whats", "whatever", "when", "whence", "whenever", "where", "wheres", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whos", "whoever", "whole", "whom", "whose", "why", "will", "willing", "wish", "with", "within", "without", "wont", "wonder", "would", "would", "wouldnt", "yes", "yet", "you", "youd", "youll", "youre", "youve", "your", "yours", "yourself", "yourselves", "zero"};
	private static Set<String> stopWordSet = new HashSet<String>(Arrays.asList(stopwords));
	private static boolean filesProcessed;
	private static File curDirectory;
	static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	private static boolean isStopword(String word) {
		if(word.length() < 2) return true;
		if(word.charAt(0) >= '0' && word.charAt(0) <= '9') return true; //remove numbers, "25th", etc
		if(stopWordSet.contains(word)) {
			return true;
		}		
		else return false;
	}

	private static String removeStopWords(String string) {
		String result = "";
		string = string.replaceAll("[^a-zA-Z0-9 ]", " ").toLowerCase();
		String[] words = string.split("\\s+");
		for(String word : words) {
			if(word.isEmpty()) {continue;}
			if(isStopword(word)) {
				continue;
				} 
			result += (word+" ");
		}
		return result;
	}
	
	private static void processFiles(File[] files) {
	    for (File file : files) {

	        if (file.isDirectory()) {
	            System.out.println("Directory: " + file.getName());
	            processFiles(file.listFiles()); // Calls same method again.
	        } else {
	        	if(!file.toString().contains("DS_Store")){
			    	try {
			    	//	System.out.println(commsMap);
			    	String	file_name = file.getAbsolutePath();
						ReadFile rf = new ReadFile(file_name);
					//	System.out.println(file_name);
						String[] aryLines = rf.OpenFile();
						String[] tmpStrings = rf.recipientsList();
						String sender1 = aryLines[2].substring(6).replace('-', '_');
						String sender = emailAliasLink.get(sender1);
				if (sender == null&&validEmailList.contains(sender1)){
					System.out.println("UNABLE TO ADD "+sender1+" who's alias is "+sender);
				}
						if (emailAliasLink.containsKey(sender1)){
						String[] str = new String[tmpStrings.length];
						for (int k = 0; k < tmpStrings.length;k++){
							str[k] = tmpStrings[k].substring(1);
							}
						
						//Second attempt for to
						if(commsMap.containsKey(sender)){
						

							ArrayList<String> to = commsMap.get(sender).get(0);
							ArrayList<String> cc = commsMap.get(sender).get(1);
							ArrayList<String> bcc = commsMap.get(sender).get(2);
							//commsTriple = commsMap.get(sender);						//	commsMap.remove(sender);							//to = commsTriple.get(0);							//cc = commsTriple.get(1);							//bcc = commsTriple.get(2);							//commsTriple.clear();
							for (int l = 0; l < str.length;l++){
								if(str[l].length()!=0&&validEmailList.contains(str[l])){
									
									//valid to recipient
									
									List<String> msgIdsList = new ArrayList<String>();
									String recipient = emailAliasLink.get(str[l].replace('-', '_'));
									if(msgLink.containsKey(recipient)){
										msgIdsList = msgLink.get(recipient);
										msgIdsList.add(aryLines[0].substring(13, aryLines[0].length() -1));
										msgLink.remove(recipient);
										msgLink.put(recipient, msgIdsList);
									}
									else{
										msgIdsList.add(aryLines[0].substring(13, aryLines[0].length() -1));
										msgLink.put(recipient, msgIdsList);
									}
									if(emailAliasLink.get(str[l].replace('-', '_'))==null){
										System.out.println("ERROR!! "+emailAliasLink.get(str[l].replace('-', '_')));
									}
									AllRecipients.add(emailAliasLink.get(str[l].replace('-', '_')));
								//AllRecipients.add(str[l].replace('-', '_'));
								to.add(emailAliasLink.get(str[l].replace('-', '_')));
								}
							}
							ArrayList<ArrayList<String>> commsTriple = new ArrayList<ArrayList<String>>();
							commsTriple.add(to);
							commsTriple.add(cc);
							commsTriple.add(bcc);
							commsMap.put(sender, commsTriple);
						
						}
						else{
							ArrayList<String> tempList = new ArrayList<String>();
						ArrayList<String> tempList2 = new ArrayList<String>();
						ArrayList<String> to = new ArrayList<String>();
							for (int l = 0; l < str.length;l++){
								if(str[l].length()!=0&&validEmailList.contains(str[l])){
									
									//valid to recipient
									
									List<String> msgIdsList = new ArrayList<String>();
									String recipient = emailAliasLink.get(str[l].replace('-', '_'));
									if(msgLink.containsKey(recipient)){
										msgIdsList = msgLink.get(recipient);
										msgIdsList.add(aryLines[0].substring(13, aryLines[0].length() -1));
										msgLink.remove(recipient);
										msgLink.put(recipient, msgIdsList);
									}
									else{
										msgIdsList.add(aryLines[0].substring(13, aryLines[0].length() -1));
										msgLink.put(recipient, msgIdsList);
									}
									
									if(emailAliasLink.get(str[l].replace('-', '_'))==null){
										System.out.println("ERROR!! "+emailAliasLink.get(str[l].replace('-', '_')));
									}
								AllRecipients.add(emailAliasLink.get(str[l].replace('-', '_')));
								to.add(emailAliasLink.get(str[l].replace('-', '_')));
								}
							}
							ArrayList<ArrayList<String>> commsTriple = new ArrayList<ArrayList<String>>();
							commsTriple.add(to);
							commsTriple.add(tempList);
							commsTriple.add(tempList2);
							commsMap.put(sender, commsTriple);
							
						}
						
						int ccSize = 0;
						String[] tmpcCStrings = rf.cCrecipients();
						if (tmpcCStrings!=null){
							ccSize = tmpcCStrings.length;
						}
						//Second attempt for cC
						if (tmpcCStrings!=null){
							
							String[] cCstr = new String[tmpcCStrings.length];
							for (int k = 0; k < tmpcCStrings.length;k++){
								cCstr[k] = tmpcCStrings[k].substring(1);
							}
							
							if(commsMap.containsKey(sender)){
								//commsTriple = commsMap.get(sender);						//	commsMap.remove(sender);							//to = commsTriple.get(0);							//cc = commsTriple.get(1);							//bcc = commsTriple.get(2);							//commsTriple.clear();
								ArrayList<String> to = commsMap.get(sender).get(0);
								ArrayList<String> cc = commsMap.get(sender).get(1);
								ArrayList<String> bcc = commsMap.get(sender).get(2);
								//commsTriple = commsMap.get(sender);						//	commsMap.remove(sender);							//to = commsTriple.get(0);							//cc = commsTriple.get(1);							//bcc = commsTriple.get(2);							//commsTriple.clear();
								for (int l = 0; l < cCstr.length;l++){
									if(cCstr[l].length()!=0&&validEmailList.contains(cCstr[l])){
										
										//valid cc recipient
										
										List<String> msgIdsList = new ArrayList<String>();
										String recipient = emailAliasLink.get(cCstr[l].replace('-', '_'));
										if(msgLink.containsKey(recipient)){
											msgIdsList = msgLink.get(recipient);
											msgIdsList.add(aryLines[0].substring(13, aryLines[0].length() -1));
											msgLink.remove(recipient);
											msgLink.put(recipient, msgIdsList);
										}
										else{
											msgIdsList.add(aryLines[0].substring(13, aryLines[0].length() -1));
											msgLink.put(recipient, msgIdsList);
										}
										if(emailAliasLink.get(cCstr[l].replace('-', '_'))==null){
											System.out.println("ERROR!! "+emailAliasLink.get(cCstr[l].replace('-', '_')));
										}
									AllRecipients.add(emailAliasLink.get(cCstr[l].replace('-', '_')));
									cc.add(emailAliasLink.get(cCstr[l].replace('-', '_')));
									}
								}
								ArrayList<ArrayList<String>> commsTriple = new ArrayList<ArrayList<String>>();
								commsTriple.add(to);
								commsTriple.add(cc);
								commsTriple.add(bcc);
								commsMap.put(sender, commsTriple);

							}
							else{
								ArrayList<String> tempList = new ArrayList<String>();
								ArrayList<String> tempList2 = new ArrayList<String>();
								ArrayList<String> cc = new ArrayList<String>();
								for (int l = 0; l < str.length;l++){
									if(cCstr[l].length()!=0&&validEmailList.contains(cCstr[l])){
										
										//valid cc recipient
										List<String> msgIdsList = new ArrayList<String>();
										String recipient = emailAliasLink.get(cCstr[l].replace('-', '_'));
										if(msgLink.containsKey(recipient)){
											msgIdsList = msgLink.get(recipient);
											msgIdsList.add(aryLines[0].substring(13, aryLines[0].length() -1));
											msgLink.remove(recipient);
											msgLink.put(recipient, msgIdsList);
										}
										else{
											msgIdsList.add(aryLines[0].substring(13, aryLines[0].length() -1));
											msgLink.put(recipient, msgIdsList);
										}
										if(emailAliasLink.get(cCstr[l].replace('-', '_'))==null){
											System.out.println("ERROR!! "+emailAliasLink.get(cCstr[l].replace('-', '_')));
										}
									AllRecipients.add(emailAliasLink.get(cCstr[l].replace('-', '_')));
									cc.add(emailAliasLink.get(cCstr[l].replace('-', '_')));
									//commsTriple.get(0).add(str[l].replace('-', '_'));
									}
								}
								ArrayList<ArrayList<String>> commsTriple = new ArrayList<ArrayList<String>>();
								commsTriple.add(tempList);
								commsTriple.add(cc);
								commsTriple.add(tempList2);
								commsMap.put(sender, commsTriple);
							}
							
							
						}
						String[] tmpbcCStrings = rf.bcCrecipients();
						int bccSize = 0;
						
						if (tmpbcCStrings!=null){
							bccSize = tmpbcCStrings.length;
						}
						
						if (bccSize != ccSize&& tmpcCStrings!=null&& tmpbcCStrings!=null){
							System.out.println("UNEQUAL CC AND BCC!!!!!!!!!!");
							System.out.println(Arrays.deepToString(tmpbcCStrings));
							System.out.println(Arrays.deepToString(tmpcCStrings));
						}
						if (tmpbcCStrings!=null){
							
							String[] bcCstr = new String[tmpbcCStrings.length];
							for (int k = 0; k < tmpbcCStrings.length;k++){
								bcCstr[k] = tmpbcCStrings[k].substring(1);
							}
							
							//Second attempt for bcc
							if(commsMap.containsKey(sender)){
								//commsTriple = commsMap.get(sender);						//	commsMap.remove(sender);							//to = commsTriple.get(0);							//cc = commsTriple.get(1);							//bcc = commsTriple.get(2);							//commsTriple.clear();
								ArrayList<String> to = commsMap.get(sender).get(0);
								ArrayList<String> cc = commsMap.get(sender).get(1);
								ArrayList<String> bcc = commsMap.get(sender).get(2);
								//commsTriple = commsMap.get(sender);						//	commsMap.remove(sender);							//to = commsTriple.get(0);							//cc = commsTriple.get(1);							//bcc = commsTriple.get(2);							//commsTriple.clear();
								for (int l = 0; l < bcCstr.length;l++){
									if(bcCstr[l].length()!=0&&validEmailList.contains(bcCstr[l])){
										
										//valid bcc recipient
										
										List<String> msgIdsList = new ArrayList<String>();
										String recipient = emailAliasLink.get(bcCstr[l].replace('-', '_'));
										if(msgLink.containsKey(recipient)){
											msgIdsList = msgLink.get(recipient);
											msgIdsList.add(aryLines[0].substring(13, aryLines[0].length() -1));
											msgLink.remove(recipient);
											msgLink.put(recipient, msgIdsList);
										}
										else{
											msgIdsList.add(aryLines[0].substring(13, aryLines[0].length() -1));
											msgLink.put(recipient, msgIdsList);
										}
										if(emailAliasLink.get(bcCstr[l].replace('-', '_'))==null){
											System.out.println("ERROR!! "+emailAliasLink.get(bcCstr[l].replace('-', '_')));
										}
									AllRecipients.add(emailAliasLink.get(bcCstr[l].replace('-', '_')));
									bcc.add(emailAliasLink.get(bcCstr[l].replace('-', '_')));
									}
								}
								ArrayList<ArrayList<String>> commsTriple = new ArrayList<ArrayList<String>>();
								commsTriple.add(to);
								commsTriple.add(cc);
								commsTriple.add(bcc);
								commsMap.put(sender, commsTriple);
							//	commsMap.put(sender, commsTriple);

							}
							else{
								ArrayList<String> tempList = new ArrayList<String>();
								ArrayList<String> tempList2 = new ArrayList<String>();

								ArrayList<String> bcc = new ArrayList<String>();
								for (int l = 0; l < str.length;l++){
									if(bcCstr[l].length()!=0&&validEmailList.contains(bcCstr[l])){
										if(emailAliasLink.get(bcCstr[l].replace('-', '_'))==null){
											
											//valid bcc recipient
											
											List<String> msgIdsList = new ArrayList<String>();
											String recipient = emailAliasLink.get(bcCstr[l].replace('-', '_'));
											if(msgLink.containsKey(recipient)){
												msgIdsList = msgLink.get(recipient);
												msgIdsList.add(aryLines[0].substring(13, aryLines[0].length() -1));
												msgLink.remove(recipient);
												msgLink.put(recipient, msgIdsList);
											}
											else{
												msgIdsList.add(aryLines[0].substring(13, aryLines[0].length() -1));
												msgLink.put(recipient, msgIdsList);
											}
											System.out.println("ERROR!! "+emailAliasLink.get(bcCstr[l].replace('-', '_')));
										}
									AllRecipients.add(emailAliasLink.get(bcCstr[l].replace('-', '_')));
									bcc.add(emailAliasLink.get(bcCstr[l].replace('-', '_')));
									//commsTriple.get(0).add(str[l].replace('-', '_'));
									}
								}
								ArrayList<ArrayList<String>> commsTriple = new ArrayList<ArrayList<String>>();
								commsTriple.add(tempList);
								commsTriple.add(tempList2);
								commsTriple.add(bcc);
								commsMap.put(sender, commsTriple);
							}
							
							
						}
						
						
					//	msgIds[fileNum] = aryLines[0].substring(13, aryLines[0].length() -1);
					//	dates[fileNum] = aryLines[1].substring(6);
					//	topics[fileNum] = aryLines[rf.subjectLine()].substring(9);
						List<String> msgIdsList = new ArrayList<String>();
						
						if(msgLink.containsKey(sender)){
							msgIdsList = msgLink.get(sender);
							msgIdsList.add(aryLines[0].substring(13, aryLines[0].length() -1));
							msgLink.remove(sender);
							msgLink.put(sender, msgIdsList);
						}
						else{
							msgIdsList.add(aryLines[0].substring(13, aryLines[0].length() -1));
							msgLink.put(sender, msgIdsList);
						}
						
						
						int i;
					/*	contents[fileNum]="\n";
						for (i=contentValue+1; i <aryLines.length; i++){
							String tmp = "\n";
							if(aryLines[i].length()==0){
								tmp = contents[fileNum]+ "\n";
							}
							else{
								tmp = contents[fileNum]+aryLines[i]+"\n";
							}
								contents[fileNum] = tmp;
							
						}*/
			    	}
						//else{
						//	System.out.println("Didn't have "+sender1+" or "+sender);
					//	}
					}
					catch (IOException e){
						System.out.println(e.getMessage());
					}
			    	fileNum++;
	        	}
	        }
	    }
	}
	public static void main(String[] args) throws IOException, InterruptedException{
		linkEmails();
		curDirectory = new File("/Users/DodionTwo/Downloads/enron_mail_20110402/maildir2");
		System.out.println("Enter query: ");
		while (true) {
			

			
				System.out.print("> "); System.out.flush();
			String input = in.readLine(); // waits for input
			
			
			List<String> argsInput = Arrays.asList(input.split("\\s+"));
			if (argsInput.contains("processFiles")){
				processFiles();
				
			}
			
			else if (argsInput.contains("help")){
				help();
				
			}
			
			else if (argsInput.contains("adjacencyMatrix")){
				adjacencyMatrix();
				
			}
			
			else if (argsInput.contains("contacts")){
				contacts();
				
			}
			else if (argsInput.contains("getAllComms")){
				getAllCommsMIDs();
				}

			else if (argsInput.contains("getAllDates")){
				getAllDates();
				}
			
			else if (argsInput.contains("getDate")){
				getDate();
			}
			
			else if (argsInput.contains("getMID")){
				getMID();
				}
			
			
			else if (argsInput.contains("getContent")){
				getContent();
			}
			
			else if (argsInput.contains("contents")){
				contents();
				}
			
			else if (argsInput.contains("getAllMIDs")){
				getAllMIDs();
				
			}
			
			else if (argsInput.contains("currentDirectory")){
				currentDirectory();
				}
			else if (argsInput.contains("setDirectory")){
				setDirectory();
				}
			
			
			else if (argsInput.contains("splitSentence")){
				topWordsByEmail();
				
			}
			
				else {
					System.out.println("For help, type help");
					}
					
			}
			
		}
	private static void linkEmails() {
		emailAliasLink.put("kaminski@enron.com","vince kaminski");
		emailAliasLink.put("joannie.williamson@enron.com", "joannie williamson");
		emailAliasLink.put("williamson@enron.com", "joannie williamson");
		emailAliasLink.put("clint.dean@enron.com", "clint dean");
		emailAliasLink.put("darrell.schoolcraft@enron.com", "darrell schoolcraft");
		emailAliasLink.put("trading <.williams@enron.com>@enron.com","jason williams");
		emailAliasLink.put("office.whalley@enron.com","greg whalley");
		emailAliasLink.put("dave.delainey@enron.com","david delainey");
		emailAliasLink.put("d..steffes@enron.com","james steffes");
		emailAliasLink.put("stan.horton@enron.com","stanley horton");
		emailAliasLink.put("s..shively@enron.com","hunter shively");
		emailAliasLink.put("m..love@enron.com","phillip love");
		emailAliasLink.put("f..keavey@enron.com","peter keavey");
		emailAliasLink.put("presto@enron.com","kevin presto");
		emailAliasLink.put("john.j.lavorato@enron.com","john lavorato");
		emailAliasLink.put("ken.lay_.chairman.of.the.board@enron.com","kenneth lay");
		emailAliasLink.put("m..forney@enron.com","john forney");
		emailAliasLink.put("mark.e.haedicke@enron.com","mark haedicke");
		emailAliasLink.put("darron.giron@enron.com","darron c.giron");
		emailAliasLink.put("a..allen@enron.com@enron.com","phillip allen");
		emailAliasLink.put("zufferli@enron.com","john zufferli");
		emailAliasLink.put("zipper@enron.com","andy zipper");
		emailAliasLink.put("williams@enron.com","bill williams");
		emailAliasLink.put("w..pereira@enron.com","susan pereira");
		emailAliasLink.put("tycholiz@enron.com","barry tycholiz");
		emailAliasLink.put("t..lucci@enron.com","paul lucci");
		emailAliasLink.put("t..hodge@enron.com","john hodge");
		emailAliasLink.put("susan.m.scott@enron.com","susan scott");
		emailAliasLink.put("steven.merris@enron.com","steven merriss");
		emailAliasLink.put("skilling@enron.com","jeff skilling");
		emailAliasLink.put("shapiro@enron.com","richard shapiro");
		emailAliasLink.put("semperger@enron.com","cara semperger");
		emailAliasLink.put("schwieger@enron.com","jim schwieger");
		emailAliasLink.put("s..white@enron.com","stacey white");
		emailAliasLink.put("p..south@enron.com","steven south");
		emailAliasLink.put("neal@enron.com","scott neal");
		emailAliasLink.put("michele.lokay@enron.com","michelle lokay");
		emailAliasLink.put("mcuilla@enron.com","martin cuilla");
		emailAliasLink.put("mckay@enron.com","brad mckay");
		emailAliasLink.put("mckay@enron.com","jonathan mckay");
		emailAliasLink.put("mccarty@enron.com","danny mccarty");
		emailAliasLink.put("marie_heard@enron.com","marie heard");
		emailAliasLink.put("m..tholt@enron.com","jane tholt");
		emailAliasLink.put("l..mims@enron.com","patrice mims");
		emailAliasLink.put("l..gay@enron.com","randall gay");
		emailAliasLink.put("rob.gay@enron.com", "robert gay");
		emailAliasLink.put("l..carson@enron.com","mike carson");
		emailAliasLink.put("ksymes@enron.com","kate symes");
		emailAliasLink.put("kitchen@enron.com","louise kitchen");
		emailAliasLink.put("jeff_dasovich@enron.com","jeff dasovich");
		emailAliasLink.put("j..sturm@enron.com","fletcher sturm");
		emailAliasLink.put("j..kean@enron.com","steven kean");
		emailAliasLink.put("j..farmer@enron.com","daren farmer");
		emailAliasLink.put("houston <.ward@enron.com>@enron.com","kim ward");
		emailAliasLink.put("hernandez@enron.com","judy hernandez");
		emailAliasLink.put("hayslett@enron.com","rod hayslett");
		emailAliasLink.put("fossum@enron.com","drew fossum");
		emailAliasLink.put("f..campbell@enron.com","larry campbell");
		emailAliasLink.put("f..brawner@enron.com","sandra brawner");
		emailAliasLink.put("ebass@enron.com","eric bass");
		emailAliasLink.put("e..dickson@enron.com","stacy dickson");
		emailAliasLink.put("dorland@enron.com","chris dorland");
		emailAliasLink.put("d..davis@enron.com","dana davis");
		emailAliasLink.put("d..baughman@enron.com","don baughman");
		emailAliasLink.put("corman@enron.com","shelley corman");
		emailAliasLink.put("c..lewis@enron.com","andrew lewis");
		emailAliasLink.put("buy@enron.com","rick buy");
		emailAliasLink.put("brenda.whitehead@enron.com", "brenda whitehead");
		emailAliasLink.put("blair.lichtenwalter@enron.com","lynn blair");
		emailAliasLink.put("bert.meyers@enron.com","albert meyers");
		emailAliasLink.put("beck@enron.com","sally beck");
		emailAliasLink.put("bailey@enron.com","susan bailey");
		emailAliasLink.put("badeer@enron.com","robert badeer");
		emailAliasLink.put("b..sanders@enron.com","richard sanders");
		emailAliasLink.put("arora@enron.com","harry arora");
		emailAliasLink.put("arnold@enron.com","john arnold");
		emailAliasLink.put("a.taylor@enron.com","mark taylor");
		emailAliasLink.put("a..smith@enron.com","matt smith");
		emailAliasLink.put("a..shankman@enron.com","jeffrey shankman");
		emailAliasLink.put("a..martin@enron.com","thomas martin");
		emailAliasLink.put("vince.j.kaminski@enron.com","vince kaminski");
		emailAliasLink.put("williams@enron.com","jason williams");
		emailAliasLink.put("whalley@enron.com","greg whalley");
		emailAliasLink.put("w..delainey@enron.com","david delainey");
		emailAliasLink.put("steffes@enron.com","james steffes");
		emailAliasLink.put("stan.horton.@enron.com","stanley horton");
		emailAliasLink.put("shively@enron.com","hunter shively");
		emailAliasLink.put("phillip.m.love@enron.com","phillip love");
		emailAliasLink.put("peter.f.keavey@enron.com","peter keavey");
		emailAliasLink.put("m..presto@enron.com","kevin presto");
		emailAliasLink.put("lavorato@enron.com","john lavorato");
		emailAliasLink.put("ken.lay_@enron.com","kenneth lay");
		emailAliasLink.put("forney@enron.com","john forney");
		emailAliasLink.put("e..haedicke@enron.com","mark haedicke");
		emailAliasLink.put("c..giron@enron.com","darron c.giron");
		emailAliasLink.put("allen@enron.com","phillip allen");
		emailAliasLink.put("j.kaminski@enron.com","vince kaminski");
		emailAliasLink.put("delainey@enron.com","david delainey");
		emailAliasLink.put("horton@enron.com","stanley horton");
		emailAliasLink.put("ken.lay@enron.com","kenneth lay");
		emailAliasLink.put("j..kaminski@enron.com","vince kaminski");
		emailAliasLink.put("vince.kaminski@enron.com","vince kaminski");
		emailAliasLink.put("jason.williams@enron.com","jason williams");
		emailAliasLink.put("greg.whalley@enron.com","greg whalley");
		emailAliasLink.put("david.delainey@enron.com","david delainey");
		emailAliasLink.put("james.steffes@enron.com","james steffes");
		emailAliasLink.put("stanley.horton@enron.com","stanley horton");
		emailAliasLink.put("hunter.shively@enron.com","hunter shively");
		emailAliasLink.put("phillip.love@enron.com","phillip love");
		emailAliasLink.put("peter.keavey@enron.com","peter keavey");
		emailAliasLink.put("kevin.presto@enron.com","kevin presto");
		emailAliasLink.put("john.lavorato@enron.com","john lavorato");
		emailAliasLink.put("kenneth.lay@enron.com","kenneth lay");
		emailAliasLink.put("john.forney@enron.com","john forney");
		emailAliasLink.put("mark.haedicke@enron.com","mark haedicke");
		emailAliasLink.put("darron.c.giron@enron.com","darron c.giron");
		emailAliasLink.put("phillip.allen@enron.com","phillip allen");
		emailAliasLink.put("john.zufferli@enron.com","john zufferli");
		emailAliasLink.put("andy.zipper@enron.com","andy zipper");
		emailAliasLink.put("bill.williams@enron.com","bill williams");
		emailAliasLink.put("susan.pereira@enron.com","susan pereira");
		emailAliasLink.put("barry.tycholiz@enron.com","barry tycholiz");
		emailAliasLink.put("paul.lucci@enron.com","paul lucci");
		emailAliasLink.put("john.hodge@enron.com","john hodge");
		emailAliasLink.put("susan.scott@enron.com","susan scott");
		emailAliasLink.put("steven.merriss@enron.com","steven merriss");
		emailAliasLink.put("jeff.skilling@enron.com","jeff skilling");
		emailAliasLink.put("richard.shapiro@enron.com","richard shapiro");
		emailAliasLink.put("cara.semperger@enron.com","cara semperger");
		emailAliasLink.put("jim.schwieger@enron.com","jim schwieger");
		emailAliasLink.put("stacey.white@enron.com","stacey white");
		emailAliasLink.put("steven.south@enron.com","steven south");
		emailAliasLink.put("scott.neal@enron.com","scott neal");
		emailAliasLink.put("michelle.lokay@enron.com","michelle lokay");
		emailAliasLink.put("martin.cuilla@enron.com","martin cuilla");
		emailAliasLink.put("brad.mckay@enron.com","brad mckay");
		emailAliasLink.put("jonathan.mckay@enron.com","jonathan mckay");
		emailAliasLink.put("danny.mccarty@enron.com","danny mccarty");
		emailAliasLink.put("marie.heard@enron.com","marie heard");
		emailAliasLink.put("jane.tholt@enron.com","jane tholt");
		emailAliasLink.put("patrice.mims@enron.com","patrice mims");
		emailAliasLink.put("randall.gay@enron.com","randall gay");
		emailAliasLink.put("mike.carson@enron.com","mike carson");
		emailAliasLink.put("margaret.carson@enron.com", "margaret carson");
		emailAliasLink.put("kate.symes@enron.com","kate symes");
		emailAliasLink.put("louise.kitchen@enron.com","louise kitchen");
		emailAliasLink.put("jeff.dasovich@enron.com","jeff dasovich");
		emailAliasLink.put("fletcher.sturm@enron.com","fletcher sturm");
		emailAliasLink.put("steven.kean@enron.com","steven kean");
		emailAliasLink.put("daren.farmer@enron.com","daren farmer");
		emailAliasLink.put("kim.ward@enron.com","kim ward");
		emailAliasLink.put("judy.hernandez@enron.com","judy hernandez");
		emailAliasLink.put("rod.hayslett@enron.com","rod hayslett");
		emailAliasLink.put("drew.fossum@enron.com","drew fossum");
		emailAliasLink.put("larry.campbell@enron.com","larry campbell");
		emailAliasLink.put("sandra.brawner@enron.com","sandra brawner");
		emailAliasLink.put("eric.bass@enron.com","eric bass");
		emailAliasLink.put("stacy.dickson@enron.com","stacy dickson");
		emailAliasLink.put("chris.dorland@enron.com","chris dorland");
		emailAliasLink.put("dana.davis@enron.com","dana davis");
		emailAliasLink.put("don.baughman@enron.com","don baughman");
		emailAliasLink.put("shelley.corman@enron.com","shelley corman");
		emailAliasLink.put("andrew.lewis@enron.com","andrew lewis");
		emailAliasLink.put("rick.buy@enron.com","rick buy");
		emailAliasLink.put("lynn.blair@enron.com","lynn blair");
		emailAliasLink.put("albert.meyers@enron.com","albert meyers");
		emailAliasLink.put("sally.beck@enron.com","sally beck");
		emailAliasLink.put("susan.bailey@enron.com","susan bailey");
		emailAliasLink.put("robert.badeer@enron.com","robert badeer");
		emailAliasLink.put("richard.sanders@enron.com","richard sanders");
		emailAliasLink.put("harry.arora@enron.com","harry arora");
		emailAliasLink.put("bani.arora@enron.com","harry arora");
		emailAliasLink.put("john.arnold@enron.com","john arnold");
		emailAliasLink.put("mark.taylor@enron.com","mark taylor");
		emailAliasLink.put("e..taylor@enron.com", "liz taylor");
		emailAliasLink.put("l..taylor@enron.com", "liz taylor");
		emailAliasLink.put("e.taylor@enron.com", "mark taylor");
		emailAliasLink.put("liz.taylor@enron.com", "liz taylor");
		emailAliasLink.put("matt.smith@enron.com","matt smith");
		emailAliasLink.put("jeffrey.shankman@enron.com","jeffrey shankman");
		emailAliasLink.put("thomas.martin@enron.com","thomas martin");
		emailAliasLink.put("carol.clair@enron.com","carol st clair");
		emailAliasLink.put("robert.benson@enron.com","robert benson");
		emailAliasLink.put("michelle.cash@enron.com","michelle cash");
		emailAliasLink.put("monika.causholli@enron.com","monika causholli");
		emailAliasLink.put("sean.crandall@enron.com","sean crandall");
		emailAliasLink.put("pete.davis@enron.com","pete davis");
		emailAliasLink.put("james.derrick@enron.com","james derrick");
		emailAliasLink.put("tom.donohoe@enron.com","tom donohoe");
		emailAliasLink.put("lindy.donoho@enron.com","lindy donoho");
		emailAliasLink.put("frank.ermis@enron.com","frank ermis");
		emailAliasLink.put("mary.fischer@enron.com","mary fischer");
		emailAliasLink.put("mfischer@enron.com","mary fischer");
		emailAliasLink.put("lisa.gang@enron.com","lisa gang");
		emailAliasLink.put("tracy.geaccone@enron.com","tracy geaccone");
		emailAliasLink.put("chris.germany@enron.com","chris germany");
		emailAliasLink.put("doug.gilbert_smith@enron.com","doug gilbert_smith");
		emailAliasLink.put("john.griffith@enron.com","john griffith");
		emailAliasLink.put("mike.grigsby@enron.com","mike grigsby");
		emailAliasLink.put("mark.guzman@enron.com","mark guzman");
		emailAliasLink.put("mary.hain@enron.com","mary hain");
		emailAliasLink.put("steven.harris@enron.com","steven harris");
		emailAliasLink.put("j.harris@enron.com", "steven harris");
		emailAliasLink.put("scott.hendrickson@enron.com","scott hendrickson");
		emailAliasLink.put("keith.holst@enron.com","keith holst");
		emailAliasLink.put("kevin.hyatt@enron.com","kevin hyatt");
		emailAliasLink.put("dan.hyvl@enron.com","dan hyvl");
		emailAliasLink.put("tana.jones@enron.com","tana jones");
		emailAliasLink.put("kam.keiser@enron.com","kam keiser");
		emailAliasLink.put("jeff.king@enron.com","jeff king");
		emailAliasLink.put("tori.kuykendall@enron.com","tori kuykendall");
		emailAliasLink.put("matthew.lenhart@enron.com","matthew lenhart");
		emailAliasLink.put("eric.linder@enron.com","eric linder");
		emailAliasLink.put("teb.lokey@enron.com","teb lokey");
		emailAliasLink.put("mike.maggi@enron.com","mike maggi");
		emailAliasLink.put("kay.mann@enron.com","kay mann");
		emailAliasLink.put("larry.may@enron.com","larry may");
		emailAliasLink.put("mark.mcconnell@enron.com","mark mcconnell");
		emailAliasLink.put("mike.mcconnell@enron.com","mike mcconnell");
		emailAliasLink.put("errol.mclaughlin@enron.com","errol mclaughlin");
		emailAliasLink.put("matt.motley@enron.com","matt motley");
		emailAliasLink.put("gerald.nemec@enron.com","gerald nemec");
		emailAliasLink.put("stephanie.panus@enron.com","stephanie panus");
		emailAliasLink.put("joe.parks@enron.com","joe parks");
		emailAliasLink.put("debra.perlingiere@enron.com","debra perlingiere");
		emailAliasLink.put("vladi.pimenov@enron.com","vladi pimenov");
		emailAliasLink.put("phillip.platter@enron.com","phillip platter");
		emailAliasLink.put("joe.quenet@enron.com","joe quenet");
		emailAliasLink.put("dutch.quigley@enron.com","dutch quigley");
		emailAliasLink.put("bill.rapp@enron.com","bill rapp");
		emailAliasLink.put("jay.reitmeyer@enron.com","jay reitmeyer");
		emailAliasLink.put("cooper.richey@enron.com","cooper richey");
		emailAliasLink.put("andrea.ring@enron.com","andrea ring");
		emailAliasLink.put("richard.ring@enron.com","richard ring");
		emailAliasLink.put("robin.rodrigue@enron.com","robin rodrigue");
		emailAliasLink.put("benjamin.rogers@enron.com","benjamin rogers");
		emailAliasLink.put("kevin.ruscitti@enron.com","kevin ruscitti");
		emailAliasLink.put("elizabeth.sager@enron.com","elizabeth sager");
		emailAliasLink.put("eric.saibi@enron.com","eric saibi");
		emailAliasLink.put("holden.salisbury@enron.com","holden salisbury");
		emailAliasLink.put("monique.sanchez@enron.com","monique sanchez");
		emailAliasLink.put("sara.shackleton@enron.com","sara shackleton");
		emailAliasLink.put("diana.scholtes@enron.com","diana scholtes");
		emailAliasLink.put("ryan.slinger@enron.com","ryan slinger");
		emailAliasLink.put("geir.solberg@enron.com","geir solberg");
		emailAliasLink.put("theresa.staab@enron.com","theresa staab");
		emailAliasLink.put("joe.stepenovitch@enron.com","joe stepenovitch");
		emailAliasLink.put("chris.stokley@enron.com","chris stokley");
		emailAliasLink.put("geoff.storey@enron.com","geoff storey");
		emailAliasLink.put("mike.swerzbin@enron.com","mike swerzbin");
		emailAliasLink.put("paul.thomas@enron.com","paul thomas");
		emailAliasLink.put("judy.townsend@enron.com","judy townsend");
		emailAliasLink.put("kimberly.watson@enron.com","kimberly watson");
		emailAliasLink.put("charles.weldon@enron.com","charles weldon");
		emailAliasLink.put("grep.whalley@enron.com","greg whalley");
		emailAliasLink.put("mark.whitt@enron.com","mark whitt");
		emailAliasLink.put("jason.wolfe@enron.com","jason wolfe");
		emailAliasLink.put("paul.y'barbo@enron.com","paul y'barbo");	}

	private static void help() throws IOException {
		System.out.println("press the number corresponding to the corresponding function or return and type the corresponding function"
				+ "\n 0 = return"
				+ "\n 1 = contacts "
				+ "\n 2 = getAllCommsMIDs "
				+ "\n 3 = getAllDates "
				+ "\n 4 = getDate"
				+ "\n 5 = getMID"
				+ "\n 6 = getContent"
				+ "\n 7 = contents"
				+ "\n 8 = getAllMIDs"
				+ "\n 9 = currentDirectory"
				+ "\n 10 = setDirectory"
				+ "\n 11 = topWordsByEmail"
				+ "\n 12 = processFiles"
				+ "\n 13 = adjacencyMatrixComplete"
				+ "\n 14 = adjacencyMatrixMini"
				+ "\n 15 = emailIDMatrix");
		String input2 = in.readLine();
		List<String> argsInput2 = Arrays.asList(input2.split("\\s+"));
		
		if (argsInput2.contains("15")||argsInput2.contains("emailIDMatrix")){
			System.out.println("emailIDMatrix");
			emailIDMatrix();
		}
		
		else if (argsInput2.contains("14")||argsInput2.contains("adjacencyMatrixMini")){
			System.out.println("adjacencyMatrixMini");
			adjacencyMatrixMini();
		}
		
		else if (argsInput2.contains("13")||argsInput2.contains("adjacencyMatrixComplete")){
			System.out.println("adjacencyMatrixComplete");
			adjacencyMatrix();
		}
		
		else if (argsInput2.contains("12")||argsInput2.contains("processFiles")){
			System.out.println("processFiles ...");
			processFiles();
		}
		
		else if (argsInput2.contains("11")||argsInput2.contains("topWordsByEmail")){
			System.out.println("topWordsByEmail ...");
			topWordsByEmail();
		}			
		
		else if (argsInput2.contains("10")||argsInput2.contains("setDirectory")){
			System.out.println("setDirectory ...");
			setDirectory();
		}
		else if (argsInput2.contains("9")||argsInput2.contains("currentDirectory")){
			System.out.println("currentDirectory ...");
			currentDirectory();
		}
		else if (argsInput2.contains("8")||argsInput2.contains("getAllMIDs")){
			System.out.println("getAllMIDs ...");
			getAllMIDs();
		}
		else if (argsInput2.contains("7")||argsInput2.contains("contents")){
			System.out.println("contents ...");
			contents();
		}
		else if (argsInput2.contains("6")||argsInput2.contains("getContent")){
			System.out.println("getContent ...");
			getContent();
		}
		else if (argsInput2.contains("5")||argsInput2.contains("getMID")){
			System.out.println("getMID ...");
			getMID();
		}
		else if (argsInput2.contains("4")||argsInput2.contains("getDate")){
			System.out.println("getDate ...");
			getDate();
		}
		else if (argsInput2.contains("3")||argsInput2.contains("getAllDates")){
			System.out.println("getAllDates ...");
			getAllDates();
		}
		else if (argsInput2.contains("2")|| argsInput2.contains("getAllCommsMIDs")){
			System.out.println("getAllCommsMIDs ...");
			getAllCommsMIDs();
		}
		else if (argsInput2.contains("1")||argsInput2.contains("contacts")){
			System.out.println("contacts ...");
			contacts();
		}

		else{System.out.println("home ...");
		
		}
	
	}

	private static void emailIDMatrix() throws IOException {
		// TODO Auto-generated method stub
		System.out.println("Please enter a directory and file name to save the Matrix to (with a .csv extension), or leave blank for the default file");
		String input3 = in.readLine();
		List<String> argsInput3 = Arrays.asList(input3.split("\\s+"));
		String tempDir = "/Users/DodionTwo/Documents/EnronEmails/EmailCollaborationMatrix.csv";
		if (!(argsInput3.get(0).length()==0)){
			tempDir = argsInput3.get(0);
			}
		FileWriter fw = new FileWriter(tempDir,false);
		
		Collection<List<String>> listOfMsgIDs = msgLink.values();
		SortedSet<String> msgIDs = new TreeSet<String>();
		//List<String>
		for (List<String> listTemp : listOfMsgIDs){
			msgIDs.addAll(listTemp);
		}
		List<String> msgIDsList = new ArrayList<String>();
		msgIDsList.addAll(msgIDs);
		List<String> completeList = new ArrayList<String>();
		SortedSet<String> completeSet = new TreeSet<String>();
		completeSet.addAll(emailAliasLink.values());
		//completeSet.addAll(AllRecipients);
		completeList.addAll(completeSet);
		int msgIDsTotal = msgIDsList.size();
		int[][] idMatrix = new int[completeList.size()][msgIDsTotal];
		for (String s : msgLink.keySet()){
			List<String> msgsIn = msgLink.get(s);
			System.out.println("Adding entry for "+s);
			for (String msgId : msgsIn){
				int msgIndex = msgIDsList.indexOf(msgId);
				int senderIndex = completeList.indexOf(s);
				if (idMatrix[senderIndex][msgIndex]==0){
					idMatrix[senderIndex][msgIndex] =1;
				}
			}
		}
		
		// Empty cell for the top left entry.
		fw.write(",");
		
		
		//top row not including the last entry
			for (int i = 0; i < msgIDsTotal-1; i++){
				fw.write(msgIDsList.get(i).replace(".JavaMail.evans@thyme","")+",");
				}
		
		//last entry of the top row
			fw.write(msgIDsList.get(msgIDsTotal-1).replace(".JavaMail.evans@thyme","")+"\n");

			//main body of the matrix
		for (int j = 0; j < completeList.size()-1;j++){
			System.out.println("Calculating Row "+j);
		//first entry of collumn (not the last row)
			fw.write(completeList.get(j)+",");
			
		//rest of column excluding the last entry (not the last row)
			for (int i = 0; i < msgIDsTotal-1; i++){
				fw.write(idMatrix[j][i]+",");
				}
		//last entry of column (not the last row)
			fw.write(idMatrix[j][msgIDsTotal-1]+"\n");

		}
		//first entry of collumn (last row)
		fw.write(completeList.get(completeList.size()-1)+",");
		System.out.println("Calculating Final Row");
		//rest of column excluding the last entry (last row)
		for (int i = 0; i < msgIDsTotal-1; i++){
			fw.write(idMatrix[completeList.size()-1][i]+",");
			}
		
		//last entry of last row and last column		
		int tmp = idMatrix[completeList.size()-1][msgIDsTotal-1];

	//	System.out.println("LASt entry = "+Integer.toString(tmp));
		fw.write(Integer.toString(tmp));
		
		
		fw.close();
		
	}

	private static void adjacencyMatrixMini() throws IOException {
			
		
		System.out.println("Please Indicate whether you would like to use the To Cc or Bcc field\n 0 = home \n 1 = To \n 2 = Cc \n 3 = Bcc");
		String input2 = in.readLine();
		List<String> argsInput2 = Arrays.asList(input2.split("\\s+"));
		
		if(argsInput2.contains("1")){
			System.out.println("Please enter a directory and file name to save the Matrix to (with a .csv extension), or leave blank for the default directory");
			String input3 = in.readLine();
			List<String> argsInput3 = Arrays.asList(input3.split("\\s+"));
			String tempDir = "/Users/DodionTwo/Documents/EnronEmails/AdjacencyMatrixTo.csv";
			if (!(argsInput3.get(0).length()==0)){
				tempDir = argsInput3.get(0);
				}
			FileWriter fw = new FileWriter(tempDir,false);
			FileWriter fw2 = new FileWriter("/Users/DodionTwo/Documents/EnronEmails/EnronTimeFromRecieverTag(To)ForHierarchy.txt",false);
			fw.flush();
			fw2.flush();
			List<String> completeList = new ArrayList<String>();
			SortedSet<String> completeSet = new TreeSet<String>();
			completeSet.addAll(emailAliasLink.values());
			//completeSet.addAll(AllRecipients);
			completeList.addAll(completeSet);
			
			Collections.sort(completeList);
		//	String[] completeArray = completeList.toArray(new String[0]);
			
			int completeSize = completeList.size();
			String[][] matrix = new String[completeSize+1][completeSize+1];
			matrix[0][0] = "";
		
			for (int i = 0; i< completeSize;i++){
				matrix[i+1][0] = completeList.get(i).replace(" ","_");
				matrix[0][i+1] = completeList.get(i).replace(" ","_");
			}
			
			System.out.println("Processing To Adjacency Matrix");
			
			//second attempt for Calculation
			for (String stri: commsMap.keySet()){
				ArrayList<ArrayList<String>> st = commsMap.get(stri);
				
				int index =0;
					

					ArrayList<String> toList = st.get(index);
					int len = toList.size();
					for (int l = 0; l < len; l++){
						String rec = toList.get(l);
						if (completeList.indexOf(rec)!=completeList.indexOf(stri)){
						if(rec.length()!=0){
							int yIndex = completeList.indexOf(rec);
							int xIndex = completeList.indexOf(stri);
							String tmp =  matrix[xIndex+1][yIndex+1];
							
							if (tmp == null){
								tmp = Integer.toString(1);
								//tmp = Integer.toString(1);
								}
							else {				 
								//System.out.println("tmp = '"+tmp+"'");
								int tmpInt = Integer.parseInt(tmp);
								tmp = Integer.toString(tmpInt +1);
								}
							matrix[xIndex+1][yIndex+1] = tmp;
							}
						}
					}
					
				}
			
			
			for (int i = 0; i <completeSize; i++){
				
				for (int j = 0; j < completeSize;j++){
					if (matrix[i][j]!=null){
						fw.write(matrix[i][j]);
						if((i>0)&&(j>0)&&(i!=j)){
							fw2.write(matrix[i][0].replace(" ", "_")+" "+matrix[0][j].replace(" ", "_")+" "+matrix[i][j]+"\n");

						}
					}
					else{
						fw.write(Integer.toString(0));
						if((i>0)&&(j>0)&&(i!=j)){
							fw2.write(matrix[i][0].replace(" ", "_")+" "+matrix[0][j].replace(" ", "_")+" "+0+"\n");
							}
					}
					fw.write(",");			

				}
				if(matrix[i][completeSize]!=null){
					fw.write(matrix[i][completeSize]);
					if (i >0&&(i!=completeSize)){
						fw2.write(matrix[i][0].replace(" ", "_")+" "+matrix[0][completeSize].replace(" ", "_")+" "+matrix[i][completeSize]+"\n");
						}
				}
				else{
					fw.write(Integer.toString(0));
					if(i>0&&(i!=completeSize)){
					fw2.write(matrix[i][0].replace(" ", "_")+" "+matrix[0][completeSize].replace(" ", "_")+" "+0+"\n");
					}
				}
				fw.write("\n");
				
			}
			for (int j = 0; j < completeSize;j++){
				if(matrix[completeSize][j]!= null){
					fw.write(matrix[completeSize][j]);
					if(j>0&&j!=completeSize){
					fw2.write(matrix[completeSize][0].replace(" ", "_")+" "+matrix[0][j].replace(" ", "_")+" "+matrix[completeSize][j]+"\n");
					}
				}
				else{
					fw.write(Integer.toString(0));
					if(j>0&&j!=completeSize){
						fw2.write(matrix[completeSize][0].replace(" ", "_")+" "+matrix[0][j].replace(" ", "_")+" "+0+"\n");
						}
				}
				fw.write(",");
			}
			if(matrix[completeSize][completeSize]!=null){
				
			fw.write(matrix[completeSize][completeSize]);
			
				fw2.write(matrix[completeSize][0].replace(" ", "_")+" "+matrix[0][completeSize].replace(" ", "_")+" "+matrix[completeSize][completeSize]+"\n");
				
			}
			else{
				fw.write(Integer.toString(0));
				
					fw2.write(matrix[completeSize][0].replace(" ", "_")+" "+matrix[0][completeSize].replace(" ", "_")+" "+0+"\n");
					
			}
			fw.close();
			fw2.close();
		}
		else if (argsInput2.contains("2")){
			System.out.println("Please enter a directory and file name to save the Matrix to (with a .csv extension), or leave blank for the default directory");
			String input3 = in.readLine();
			List<String> argsInput3 = Arrays.asList(input3.split("\\s+"));
			String tempDir = "/Users/DodionTwo/Documents/EnronEmails/AdjacencyMatrixCc.csv";
			if (!(argsInput3.get(0).length()==0)){
				tempDir = argsInput3.get(0);
				}
			FileWriter fw = new FileWriter(tempDir,false);
			FileWriter fw2 = new FileWriter("/Users/DodionTwo/Documents/EnronEmails/EnronTimeFromRecieverTag(Cc)ForHierarchy.txt",false);
			fw.flush();
			fw2.flush();
			List<String> completeList = new ArrayList<String>();
			SortedSet<String> completeSet = new TreeSet<String>();
			completeSet.addAll(emailAliasLink.values());
			//completeSet.addAll(AllRecipients);
			completeList.addAll(completeSet);
			
			Collections.sort(completeList);
		//	String[] completeArray = completeList.toArray(new String[0]);
			
			int completeSize = completeList.size();
			String[][] matrix = new String[completeSize+1][completeSize+1];
			matrix[0][0] = "";
		
			for (int i = 0; i< completeSize;i++){
				matrix[i+1][0] = completeList.get(i).replace(" ","_");
				matrix[0][i+1] = completeList.get(i).replace(" ","_");
			}
			
			System.out.println("Processing To Adjacency Matrix");
			
			//second attempt for Calculation
			for (String stri: commsMap.keySet()){
				ArrayList<ArrayList<String>> st = commsMap.get(stri);
				
				int index =1;
					

					ArrayList<String> toList = st.get(index);
					int len = toList.size();
					for (int l = 0; l < len; l++){
						String rec = toList.get(l);
						if (completeList.indexOf(rec)!=completeList.indexOf(stri)){
						if(rec.length()!=0){
							int yIndex = completeList.indexOf(rec);
							int xIndex = completeList.indexOf(stri);
							String tmp =  matrix[xIndex+1][yIndex+1];
							
							if (tmp == null){
								tmp = Integer.toString(1);
								//tmp = Integer.toString(1);
								}
							else {				 
								//System.out.println("tmp = '"+tmp+"'");
								int tmpInt = Integer.parseInt(tmp);
								tmp = Integer.toString(tmpInt +1);
								}
							matrix[xIndex+1][yIndex+1] = tmp;
							}
						}
					}
				}
			
			
			for (int i = 0; i <completeSize; i++){
				
				for (int j = 0; j < completeSize;j++){
					if (matrix[i][j]!=null){
						fw.write(matrix[i][j]);
						if((i>0)&&(j>0)&&(i!=j)){
							fw2.write(matrix[i][0].replace(" ", "_")+" "+matrix[0][j].replace(" ", "_")+" "+matrix[i][j]+"\n");

						}
					}
					else{
						fw.write(Integer.toString(0));
						if((i>0)&&(j>0)&&(i!=j)){
							fw2.write(matrix[i][0].replace(" ", "_")+" "+matrix[0][j].replace(" ", "_")+" "+0+"\n");
							}
					}
					fw.write(",");			

				}
				if(matrix[i][completeSize]!=null){
					fw.write(matrix[i][completeSize]);
					if (i >0&&(i!=completeSize)){
						fw2.write(matrix[i][0].replace(" ", "_")+" "+matrix[0][completeSize].replace(" ", "_")+" "+matrix[i][completeSize]+"\n");
						}
				}
				else{
					fw.write(Integer.toString(0));
					if(i>0&&(i!=completeSize)){
					fw2.write(matrix[i][0].replace(" ", "_")+" "+matrix[0][completeSize].replace(" ", "_")+" "+0+"\n");
					}
				}
				fw.write("\n");
				
			}
			for (int j = 0; j < completeSize;j++){
				if(matrix[completeSize][j]!= null){
					fw.write(matrix[completeSize][j]);
					if(j>0&&j!=completeSize){
					fw2.write(matrix[completeSize][0].replace(" ", "_")+" "+matrix[0][j].replace(" ", "_")+" "+matrix[completeSize][j]+"\n");
					}
				}
				else{
					fw.write(Integer.toString(0));
					if(j>0&&j!=completeSize){
						fw2.write(matrix[completeSize][0].replace(" ", "_")+" "+matrix[0][j].replace(" ", "_")+" "+0+"\n");
						}
				}
				fw.write(",");
			}
			if(matrix[completeSize][completeSize]!=null){
				
			fw.write(matrix[completeSize][completeSize]);
			
				fw2.write(matrix[completeSize][0].replace(" ", "_")+" "+matrix[0][completeSize].replace(" ", "_")+" "+matrix[completeSize][completeSize]+"\n");
				
			}
			else{
				fw.write(Integer.toString(0));
				
					fw2.write(matrix[completeSize][0].replace(" ", "_")+" "+matrix[0][completeSize].replace(" ", "_")+" "+0+"\n");
					
			}
			fw.close();
			fw2.close();
		}
		
		else if (argsInput2.contains("3")) {
			System.out.println("Please enter a directory and file name to save the Matrix to (with a .csv extension), or leave blank for the default directory");
			String input3 = in.readLine();
			List<String> argsInput3 = Arrays.asList(input3.split("\\s+"));
			String tempDir = "/Users/DodionTwo/Documents/EnronEmails/AdjacencyMatrixBcc.csv";

			if (!(argsInput3.get(0).length()==0)){
				tempDir = argsInput3.get(0);
				}
			FileWriter fw = new FileWriter(tempDir,false);
			FileWriter fw2 = new FileWriter("/Users/DodionTwo/Documents/EnronEmails/EnronTimeFromRecieverTag(BCc)ForHierarchy.txt",false);
			fw.flush();
			fw2.flush();
			List<String> completeList = new ArrayList<String>();
			SortedSet<String> completeSet = new TreeSet<String>();
			completeSet.addAll(emailAliasLink.values());
			//completeSet.addAll(AllRecipients);
			completeList.addAll(completeSet);
			
			Collections.sort(completeList);
		//	String[] completeArray = completeList.toArray(new String[0]);
			
			int completeSize = completeList.size();
			String[][] matrix = new String[completeSize+1][completeSize+1];
			matrix[0][0] = "";
		
			for (int i = 0; i< completeSize;i++){
				matrix[i+1][0] = completeList.get(i).replace(" ","_");
				matrix[0][i+1] = completeList.get(i).replace(" ","_");
			}
			
			System.out.println("Processing To Adjacency Matrix");
			
			//second attempt for Calculation
			for (String stri: commsMap.keySet()){
				ArrayList<ArrayList<String>> st = commsMap.get(stri);
				
				int index =2;
					

					ArrayList<String> toList = st.get(index);
					int len = toList.size();
					for (int l = 0; l < len; l++){
						String rec = toList.get(l);
						if (completeList.indexOf(rec)!=completeList.indexOf(stri)){
						if(rec.length()!=0){
							int yIndex = completeList.indexOf(rec);
							int xIndex = completeList.indexOf(stri);
							String tmp =  matrix[xIndex+1][yIndex+1];
							
							if (tmp == null){
								tmp = Integer.toString(1);
								//tmp = Integer.toString(1);
								}
							else {				 
								//System.out.println("tmp = '"+tmp+"'");
								int tmpInt = Integer.parseInt(tmp);
								tmp = Integer.toString(tmpInt +1);
								}
							matrix[xIndex+1][yIndex+1] = tmp;
							}
						}
					}
				}
			
			
			for (int i = 0; i <completeSize; i++){
				
				for (int j = 0; j < completeSize;j++){
					if (matrix[i][j]!=null){
						fw.write(matrix[i][j]);
						if((i>0)&&(j>0)&&(i!=j)){
							fw2.write(matrix[i][0].replace(" ", "_")+" "+matrix[0][j].replace(" ", "_")+" "+matrix[i][j]+"\n");

						}
					}
					else{
						fw.write(Integer.toString(0));
						if((i>0)&&(j>0)&&(i!=j)){
							fw2.write(matrix[i][0].replace(" ", "_")+" "+matrix[0][j].replace(" ", "_")+" "+0+"\n");
							}
					}
					fw.write(",");			

				}
				if(matrix[i][completeSize]!=null){
					fw.write(matrix[i][completeSize]);
					if (i >0&&(i!=completeSize)){
						fw2.write(matrix[i][0].replace(" ", "_")+" "+matrix[0][completeSize].replace(" ", "_")+" "+matrix[i][completeSize]+"\n");
						}
				}
				else{
					fw.write(Integer.toString(0));
					if(i>0&&(i!=completeSize)){
					fw2.write(matrix[i][0].replace(" ", "_")+" "+matrix[0][completeSize].replace(" ", "_")+" "+0+"\n");
					}
				}
				fw.write("\n");
				
			}
			for (int j = 0; j < completeSize;j++){
				if(matrix[completeSize][j]!= null){
					fw.write(matrix[completeSize][j]);
					if(j>0&&j!=completeSize){
					fw2.write(matrix[completeSize][0].replace(" ", "_")+" "+matrix[0][j].replace(" ", "_")+" "+matrix[completeSize][j]+"\n");
					}
				}
				else{
					fw.write(Integer.toString(0));
					if(j>0&&j!=completeSize){
						fw2.write(matrix[completeSize][0].replace(" ", "_")+" "+matrix[0][j].replace(" ", "_")+" "+0+"\n");
						}
				}
				fw.write(",");
			}
			if(matrix[completeSize][completeSize]!=null){
				
			fw.write(matrix[completeSize][completeSize]);
			
				fw2.write(matrix[completeSize][0].replace(" ", "_")+" "+matrix[0][completeSize].replace(" ", "_")+" "+matrix[completeSize][completeSize]+"\n");
				
			}
			else{
				fw.write(Integer.toString(0));
				
					fw2.write(matrix[completeSize][0].replace(" ", "_")+" "+matrix[0][completeSize].replace(" ", "_")+" "+0+"\n");
					
			}
			fw.close();
			fw2.close();
		}
	}

	private static void adjacencyMatrix() throws IOException{
		// TODO Auto-generated method stub
		//comms.keySet();
		
		System.out.println("Please enter a directory and file name to save the Matrix to (with a .csv extension), or leave blank for the default directory");
		String input3 = in.readLine();
		List<String> argsInput3 = Arrays.asList(input3.split("\\s+"));
		String tempDir = "/Users/DodionTwo/Documents/EnronEmails/AdjacencyMatrix.csv";
		if (!(argsInput3.get(0).length()==0)){			
			tempDir = argsInput3.get(0);
		}
		FileWriter fw = new FileWriter(tempDir,false);
		FileWriter fw2 = new FileWriter("/Users/DodionTwo/Documents/EnronEmails/EnronTimeFromRecieverTag(ToCcBcc)ForHierarchy.txt",false);
		fw.flush();
		fw2.flush();
		List<String> completeList = new ArrayList<String>();
		SortedSet<String> completeSet = new TreeSet<String>();
		completeSet.addAll(commsMap.keySet());
		completeSet.addAll(AllRecipients);
		completeList.addAll(completeSet);
		
		Collections.sort(completeList);
	//	String[] completeArray = completeList.toArray(new String[0]);
		
		int completeSize = completeList.size();
		String[][] matrix = new String[completeSize+1][completeSize+1];
		matrix[0][0] = "";
	
		for (int i = 0; i< completeSize;i++){
			matrix[i+1][0] = completeList.get(i).replace(" ","_");
			matrix[0][i+1] = completeList.get(i).replace(" ","_");
		}
		System.out.println("Adding entry");
		
		//second attempt for Calculation
		for (String stri: commsMap.keySet()){
			ArrayList<ArrayList<String>> st = commsMap.get(stri);
			
			for (int index = 0;index < 3; index++){
				

				ArrayList<String> toList = st.get(index);
				int len = toList.size();
				for (int l = 0; l < len; l++){
					String rec = toList.get(l);
					if (completeList.indexOf(rec)!=completeList.indexOf(stri)){
					if(rec.length()!=0){
						int yIndex = completeList.indexOf(rec);
						int xIndex = completeList.indexOf(stri);
						String tmp =  matrix[xIndex+1][yIndex+1];
						
						if (tmp == null){
							tmp = Integer.toString(3 - (index));
							//tmp = Integer.toString(1);
							}
						else {				 
							//System.out.println("tmp = '"+tmp+"'");
							int tmpInt = Integer.parseInt(tmp);
							tmp = Integer.toString(tmpInt +3 -(index));
							}
						matrix[xIndex+1][yIndex+1] = tmp;
						}
					}
				}
				}
			}
		
		
		for (int i = 0; i <completeSize; i++){
			
			for (int j = 0; j < completeSize;j++){
				if (matrix[i][j]!=null){
					fw.write(matrix[i][j]);
					if((i>0)&&(j>0)&&(i!=j)){
						fw2.write(matrix[i][0].replace(" ", "_")+" "+matrix[0][j].replace(" ", "_")+" "+matrix[i][j]+"\n");

					}
				}
				else{
					fw.write(Integer.toString(0));
					if((i>0)&&(j>0)&&(i!=j)){
						fw2.write(matrix[i][0].replace(" ", "_")+" "+matrix[0][j].replace(" ", "_")+" "+0+"\n");
						}
				}
				fw.write(",");			

			}
			if(matrix[i][completeSize]!=null){
				fw.write(matrix[i][completeSize]);
				if (i >0&&(i!=completeSize)){
					fw2.write(matrix[i][0].replace(" ", "_")+" "+matrix[0][completeSize].replace(" ", "_")+" "+matrix[i][completeSize]+"\n");
					}
			}
			else{
				fw.write(Integer.toString(0));
				if(i>0&&(i!=completeSize)){
				fw2.write(matrix[i][0].replace(" ", "_")+" "+matrix[0][completeSize].replace(" ", "_")+" "+0+"\n");
				}
			}
			fw.write("\n");
			
		}
		for (int j = 0; j < completeSize;j++){
			if(matrix[completeSize][j]!= null){
				fw.write(matrix[completeSize][j]);
				if(j>0&&j!=completeSize){
				fw2.write(matrix[completeSize][0].replace(" ", "_")+" "+matrix[0][j].replace(" ", "_")+" "+matrix[completeSize][j]+"\n");
				}
			}
			else{
				fw.write(Integer.toString(0));
				if(j>0&&j!=completeSize){
					fw2.write(matrix[completeSize][0].replace(" ", "_")+" "+matrix[0][j].replace(" ", "_")+" "+0+"\n");
					}
			}
			fw.write(",");
		}
		if(matrix[completeSize][completeSize]!=null){
			
		fw.write(matrix[completeSize][completeSize]);
		
			fw2.write(matrix[completeSize][0].replace(" ", "_")+" "+matrix[0][completeSize].replace(" ", "_")+" "+matrix[completeSize][completeSize]+"\n");
			
		}
		else{
			fw.write(Integer.toString(0));
			
				fw2.write(matrix[completeSize][0].replace(" ", "_")+" "+matrix[0][completeSize].replace(" ", "_")+" "+0+"\n");
				
		}
		fw.close();
		fw2.close();
	}

	private static void topWordsByEmail() throws IOException {
		System.out.println("Please indicate the file number of the email you wish to process ");
		String input2 = in.readLine();
		List<String> argsInput2 = Arrays.asList(input2.split("\\s+"));
		int index = (int) Float.parseFloat(argsInput2.get(0));
		System.out.println("Please indicate how many words you wish to return");
		String input3 = in.readLine();
		List<String> argsInput3 = Arrays.asList(input3.split("\\s+"));
		int number = (int) Float.parseFloat(argsInput3.get(0));

		String tempstr = "NEED TO DO";
				//contents[index];

		tempstr = removeStopWords(tempstr);
		String[] words = tempstr.replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase().split("\\s+");
		List<String> wordList = Arrays.asList(words);
		Set<String> mySet = new HashSet<String>(wordList);

		Map<String,Integer> temp = new HashMap<String, Integer>();
		for(String s: mySet){
			temp.put(s, Collections.frequency(wordList,s));
		}

		
		
		
		SortedSet<Entry<String, Integer>> sorted = entriesSortedByValues(temp);
		System.out.println(sorted);
		System.out.print("Top "+number+" entries for email "+index+" = [");
		
		Iterator<Entry<String, Integer>> it = sorted.iterator();
		for(int i = 0; i < number-1;i++){
			if (it.hasNext()){
			Entry<String, Integer> s = it.next();
			System.out.print(s.getKey()+", ");}
			
		}
		Entry<String, Integer> s = it.next();
		System.out.print(s.getKey());
		System.out.println("]");
	
		System.out.println("home ...");
	}

	private static void setDirectory() throws IOException {
		System.out.println("Please indicate the directory you wish to use ");
		String input2 = in.readLine();
		List<String> argsInput2 = Arrays.asList(input2.split("\\s+"));
		curDirectory = new File(argsInput2.get(0));
		System.out.println("new directory = "+curDirectory);
		
		System.out.println("home ...");		
	}

	private static void currentDirectory() {
		System.out.println(curDirectory);
		System.out.println("home ...");
	}

	private static void getAllMIDs() {
		System.out.print(fileNum +" MIDs = [");
		for (int i = 0; i < fileNum;i++){
	//			System.out.print(msgIds[i]+"; ");
		}
		System.out.println("]");
		System.out.println("home ...");
	}

	private static void contents() throws IOException {

		System.out.println("Please indicate where you would like the data to be stored\n 0 = home \n 1 = In-line \n 2 = To-file");
		String input2 = in.readLine();
		List<String> argsInput2 = Arrays.asList(input2.split("\\s+"));
		
		if(argsInput2.contains("1")){
		System.out.println("Total emails = "+fileNum);
		System.out.print(fileNum+" emails = [");
				for (int i = 0; i < fileNum;i++){
						System.out.print("NEED TO DO"//contents[i]+"; "
								);
				}
				System.out.println("]");
		}
		
		else if(argsInput2.contains("2")){
			System.out.println("total number of senders = "+commsMap.size());
			System.out.println("Please enter a directory and file name to save the contacts to, or leave blank for the default directory");
			String input3 = in.readLine();
			List<String> argsInput3 = Arrays.asList(input3.split("\\s+"));
			String tempDir = "/Users/DodionTwo/Documents/EnronEmails/Contents";

			if (!(argsInput3.get(0).length()==0)){
				tempDir = argsInput3.get(0);
			}
			System.out.print(fileNum+" emails ");
			for (int i = 0; i < fileNum;i++){
				String curTempDir = tempDir+"/"+i+".txt";
				FileWriter fw = new FileWriter(curTempDir,false);
				fw.write("NEED TO DO");
				//fw.write(contents[i]);
				fw.close();
			}
		}
		

			System.out.println("home ...");


			
	}

	private static void getContent() throws IOException {
		System.out.println("Please indicate the file number of the date you wish to look up ");
		String input2 = in.readLine();
		List<String> argsInput2 = Arrays.asList(input2.split("\\s+"));
		int index = (int) Float.parseFloat(argsInput2.get(0));
		System.out.println("Content for message "+index+" = "+"NEED TO DO"//contents[index]
		);		
		System.out.println("home ...");
	}

	private static void getMID() throws IOException {
		System.out.println("Please indicate the file number of the date you wish to look up ");
		String input2 = in.readLine();
		List<String> argsInput2 = Arrays.asList(input2.split("\\s+"));
		int index = (int) Float.parseFloat(argsInput2.get(0));		
	//	System.out.println("MID["+index+"] = "+msgIds[index]);		
		System.out.println("home ...");
	}

	private static void getDate() throws IOException {
		System.out.println("Please indicate the file number of the date you wish to look up ");
		String input2 = in.readLine();
		List<String> argsInput2 = Arrays.asList(input2.split("\\s+"));
		int index = (int) Float.parseFloat(argsInput2.get(0));
	//	System.out.println("date["+index+"] = "+dates[index]);		
		System.out.println("home ...");
	}

	private static void getAllDates() {
		//dates = clean(dates);
		System.out.println("Date size = "+fileNum);
		System.out.print(fileNum+" dates = [");
				for (int i = 0; i < fileNum;i++){
				//	if (dates[i]!=null){
			//			System.out.print(dates[i]+"; ");
				//	}
				}
				System.out.println("]");
				System.out.println("home ...");		
	}

	private static void getAllCommsMIDs() throws IOException {
		System.out.println("Please indicate how you want the results to be saved by typing the corresponding number\n 0 = home \n 1= In-line results \n 2 = Save to File ");
		String input2 = in.readLine();
		List<String> argsInput2 = Arrays.asList(input2.split("\\s+"));
		
		if(argsInput2.contains("1")){
			for (String s:msgLink.keySet()){
			if (s.contains("enron")){
				System.out.println(s+" = "+ Arrays.deepToString(msgLink.get(s).toArray()));
			}
			
		}
		}
		
		if(argsInput2.contains("2")){
			for (String s:msgLink.keySet()){
			if (s.contains("enron")){
				System.out.println(s+" = "+ msgLink.get(s).size());
			}
			
		}
		}

			System.out.println("home ...");

	}

	private static void processFiles() throws IOException {
		if(filesProcessed==false){
			File[] files = curDirectory.listFiles();
			fileNum = 0;
			
			processFiles(files);

		filesProcessed = true;}
		else{
			System.out.println("Already sorted the files before. Are you sure you wish to reprocess the files?");
			String input2 = in.readLine();
			List<String> argsInput2 = Arrays.asList(input2.split("\\s+"));
			if(argsInput2.contains("yes")){
				//files2 = new ArrayList<File>();		
				//msgIds = new String[totalMessages];
			//	topics = new String[totalMessages];
			//	dates = new String[totalMessages];
				//contents = new String[totalMessages];
				commsMap = new HashMap<String,ArrayList<ArrayList<String>>>();;
				msgLink = new HashMap<String, List<String>>();
				fileNum = 0;
				File[] files = curDirectory.listFiles();
				processFiles(files);
			}
		}
		
		System.out.println("Processed "+fileNum+ " files");
		System.out.println("home ...");
	//	System.out.println(commsMap);
	}

	private static void contacts() throws IOException {
		System.out.println("Please indicate how you want the results to be saved by typing the corresponding number\n 0 = home \n 1= Email communications in-line results \n 2 = Email communications save to File \n 3 = Contact names in-line \n 4 = Contact names to file");
		String input2 = in.readLine();
		List<String> argsInput2 = Arrays.asList(input2.split("\\s+"));
		
		if(argsInput2.contains("1")){
			for (String s:commsMap.keySet()){
				if (s.contains("enron")){
					int totalEmails = commsMap.get(s).get(0).size()+commsMap.get(s).get(1).size()+commsMap.get(s).get(2).size();
					System.out.print(totalEmails+ " recipients for "+s+" = [");
					List<String> toRecipients =  commsMap.get(s).get(0);
					List<String> ccRecipients =  commsMap.get(s).get(1);
					List<String> bccRecipients =  commsMap.get(s).get(2);
					for (String r : toRecipients){
						System.out.print(r+", ");
						}
					for (String r : ccRecipients){
						System.out.print(r+", ");
						}
					
					for (String r : bccRecipients){
						System.out.print(r+", ");
						}
					
					System.out.print("]");
					System.out.print("\n");
					}
				}
			}
		
		if(argsInput2.contains("2")){
			System.out.println("total number of senders = "+commsMap.size());
			System.out.println("Please enter a directory and file name to save the contacts to, or leave blank for the default directory");
			String input3 = in.readLine();
			List<String> argsInput3 = Arrays.asList(input3.split("\\s+"));
			String tempDir = "/Users/DodionTwo/Documents/EnronEmails/contactCommunications.txt";
			if (!(argsInput3.get(0).length()==0)){
				tempDir = argsInput3.get(0);
			}
		
			FileWriter fw = new FileWriter(tempDir,false);
			fw.flush();
			fw.write("Enron Email Communications \n \n");
			for (String s:commsMap.keySet()){
				if (s.contains("enron")){
					int totalEmails = commsMap.get(s).get(0).size()+commsMap.get(s).get(1).size()+commsMap.get(s).get(2).size();
					fw.append(totalEmails+ " recipients for "+s+" = [");
					List<String> toRecipients =  commsMap.get(s).get(0);
					List<String> ccRecipients =  commsMap.get(s).get(1);
					List<String> bccRecipients =  commsMap.get(s).get(2);
					for (String r : toRecipients){
						if(r.contains("enron")){
							fw.append(r+", ");
						}
					}
					for (String r : ccRecipients){
						if(r.contains("enron")){
							fw.append(r+", ");
						}
					}
					for (String r : bccRecipients){
						if(r.contains("enron")){
							fw.append(r+", ");
						}
					}
					fw.append("]");
					fw.append("\n");
				}
				
			}
			fw.close();
		}
		if (argsInput2.contains("3")){
			System.out.println(commsMap.keySet());
		}
		if (argsInput2.contains("4")){
			System.out.println("Please enter a directory and file name to save the contacts to, or leave blank for the default directory");
			String input3 = in.readLine();
			List<String> argsInput3 = Arrays.asList(input3.split("\\s+"));
			String tempDir = "/Users/DodionTwo/Documents/EnronEmails/ContactNames.txt";
			if (!(argsInput3.get(0).length()==0)){
				tempDir = argsInput3.get(0);
			}

			printCollection(commsMap.keySet(), tempDir,"Enron Email Contacts");
			
		}
		
		System.out.println("home ...");
	}

	// If we need to do remove null entries
/*	private static String[] clean(final String[] v) {
	    List<String> list = new ArrayList<String>(Arrays.asList(v));
	    list.removeAll(Collections.singleton(null));
	    return list.toArray(new String[list.size()]);
	} */
	
	 private static void printCollection(Collection<String> c, String tempDir, String FirstSentence) throws IOException{
		 FileWriter fw = new FileWriter(tempDir,false);
			fw.flush();   	      
			fw.write(FirstSentence+" \n \n");
			fw.write("[");
			Iterator<String> it = c.iterator();
	        while ( it.hasNext() ){
	        	
	            fw.write(it.next());
	            if (it.hasNext()){
	            	fw.write(", ");
	            }
	        }
	      fw.write("]");
	      fw.close();
	    }
		

	static <K,V extends Comparable<? super V>>	SortedSet<Map.Entry<K,V>> entriesSortedByValues(Map<K,V> map) {
	    SortedSet<Map.Entry<K,V>> sortedEntries = new TreeSet<Map.Entry<K,V>>(
	        new Comparator<Map.Entry<K,V>>() {
	            @Override public int compare(Map.Entry<K,V> e1, Map.Entry<K,V> e2) {
	            	int res = e2.getValue().compareTo(e1.getValue());
                    return res != 0 ? res : 1;
	            }
	        }
	    );
	    sortedEntries.addAll(map.entrySet());
	    return sortedEntries;
	}
	
	}


