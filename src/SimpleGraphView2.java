
import edu.uci.ics.jung.algorithms.cluster.EdgeBetweennessClusterer;
import edu.uci.ics.jung.algorithms.layout.AggregateLayout;
import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.algorithms.layout.KKLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.layout.ISOMLayout;
//import edu.uci.ics.jung.algorithms.cluster.ClusterSet;
import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.FRLayout2;
import edu.uci.ics.jung.algorithms.layout.util.Relaxer;
import edu.uci.ics.jung.graph.DirectedSparseMultigraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseMultigraph;
import edu.uci.ics.jung.visualization.Layer;
//import edu.uci.ics.jung.visualization.decorators.
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.EditingModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position;
import edu.uci.ics.jung.visualization.transform.MutableTransformer;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Paint;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSlider;
import javax.swing.JToggleButton;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.functors.ChainedTransformer;
import org.apache.commons.collections15.functors.ConstantTransformer;
import org.apache.commons.collections15.functors.MapTransformer;
import org.apache.commons.collections15.map.LazyMap;



/**
 *
 * @author Dr. Greg M. Bernstein
 */
public class SimpleGraphView2 {
	static List<String> updatedNames = new ArrayList<String>();

	public final static Color[] similarColors =
		{
			new Color(210, 134, 134),
			new Color(135, 137, 211),
			new Color(134, 206, 189),
			new Color(206, 176, 134),
			new Color(194, 204, 134),
			new Color(145, 214, 134),
			new Color(133, 178, 209),
			new Color(103, 148, 255),
			new Color(60, 220, 220),
			new Color(30, 250, 100)
		};
	
	//vv = new VisualizationViewer<Number,Number>(layout);
	static Map<GraphElements.MyVertex,Paint> vertexPaints = 
			LazyMap.<GraphElements.MyVertex,Paint>decorate(new HashMap<GraphElements.MyVertex,Paint>(),
					new ConstantTransformer(Color.white));
		static Map<GraphElements.MyEdge,Paint> edgePaints =
		LazyMap.<GraphElements.MyEdge,Paint>decorate(new HashMap<GraphElements.MyEdge,Paint>(),
				new ConstantTransformer(Color.blue));
	static SimpleGraphView2 sgv = new SimpleGraphView2();   
	private static final Object DEMOKEY = "DEMOKEY";
//	GraphElements.MyVertex x = new GraphElements.MyVertex("BOO");
	static AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge> layout = new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new ISOMLayout<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g));
	static AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge> FRlayout =  new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new FRLayout<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g));
	static AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge> KKlayout = new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new KKLayout<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g));
static VisualizationViewer<GraphElements.MyVertex, GraphElements.MyEdge> vv = 
            new VisualizationViewer<GraphElements.MyVertex, GraphElements.MyEdge>(layout);
	static Transformer blankLabelTransformer = new ChainedTransformer<String,String>(new Transformer[]{
            new ToStringLabeller<String>(),
            new Transformer<String,String>() {
            public String transform(String input) {
                return "<html><font color=\"black\">";
            }}});
//	private static boolean edgesVisible = false;
	//private static boolean verticesVisible = true;
    static Graph<GraphElements.MyVertex, GraphElements.MyEdge> g;
    /** Creates a new instance of SimpleGraphView */
    public SimpleGraphView2() {
    	// This builds the graph
        // Layout<V, E>, VisualizationComponent<V,E>
        
        // Graph<V, E> where V is the type of the vertices and E is the type of the edges
        // Note showing the use of a SparseGraph rather than a SparseMultigraph
        g = new DirectedSparseMultigraph<GraphElements.MyVertex, GraphElements.MyEdge>();
        // Add some vertices. From above we defined these to be type Number.
       GraphElements.MyVertex vertex1 = new GraphElements.MyVertex("Vertex1");
       g.addVertex(vertex1);
       GraphElements.MyVertex vertex2 = new GraphElements.MyVertex("Vertex2");
       g.addVertex(vertex2);
       GraphElements.MyVertex vertex3 = new GraphElements.MyVertex("Vertex3");
       g.addVertex(vertex3);
        
    //    g.addVertex((Number)2);
     //   g.addVertex((Number)3);
        int indices = 15;
        char edges = 20;
        for(int j = 4; j < indices; j++){
        	GraphElements.MyVertex vertexTemp = new GraphElements.MyVertex("Vertex"+j);
            g.addVertex(vertexTemp);
        	//g.addVertex((Number)j);
        	}
        // g.addVertex((Number)1);  // note if you add the same object again nothing changes
        // Add some edges. From above we defined these to be of type String
        // Note that the default is for undirected edges.
        //g.
    //    GraphElements.MyEdge edge1 = new GraphElements.MyEdge("Edge1");
  //      g.addEdge(edge1);
        GraphElements.MyVertex[] ver = new GraphElements.MyVertex[0];
        GraphElements.MyVertex[] verticesArray = g.getVertices().toArray(ver);
      //  System.out.println("Vertex count = "+verticesArray.length);
        
        for (int chr = 1;chr<=edges;chr++){
        	int[] k = rand(indices - 1);
        	GraphElements.MyEdge edge1 = new GraphElements.MyEdge("Edge"+chr);
        	g.addEdge(edge1, verticesArray[k[0]-1], verticesArray[k[1]-1]);
        }
    }
    
    private int[] rand(int indices) {
    	int min = 1;
    	Random random = new Random();
    	int value1 = random.nextInt(indices - min + 1) + min;
    	int value2 = random.nextInt(indices - min + 1) + min;
    	while(value2==value1){
    		value2 = random.nextInt(indices - min + 1) + min;
    	}
		// TODO Auto-generated method stub
		int[] values = new int[2];
		values[0] = value1;
		values[1] = value2;
		return values;
	}

	/**
     * @param args the command line arguments
	 * @throws InterruptedException 
     */
    public static void main(String[] args) throws InterruptedException {
        layout.setSize(new Dimension(300,300));
       
       vv.setPreferredSize(new Dimension(350,350));       
        // Setup up a new vertex to paint transformer...
        Transformer<Number,Paint> vertexPaint = new Transformer<Number,Paint>() {
            public Paint transform(Number i) {
                return Color.GREEN;
            }
        };  
        // Set up a new stroke Transformer for the edges
        float dash[] = {10.0f};
        final Stroke edgeStroke = new BasicStroke(1.0f, BasicStroke.CAP_BUTT,
             BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f);
        Transformer<String, Stroke> edgeStrokeTransformer = new Transformer<String, Stroke>() {
            public Stroke transform(String s) {
                return edgeStroke;
            }
        };
       // vv.getRenderContext().setVertexFillPaintTransformer(vertexPaint);
      //  vv.getRenderContext().setEdgeStrokeTransformer(edgeStrokeTransformer);
        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        vv.getRenderer().getVertexLabelRenderer().setPosition(Position.CNTR);        
        
        final JFrame frame = new JFrame("Simple Graph View 2");
        JPanel pnlButton = new JPanel();
       
       frame.setBackground(Color.WHITE);
       JMenuBar menuBar = new JMenuBar();
       JMenu mainMenu = new JMenu("File");
       JMenu viewMenu = new JMenu("Change view");
       JMenu display = new JMenu("Display Settings");
       JMenu toggle = new JMenu("Toggle Labels");
       JCheckBoxMenuItem toggleEdge = new JCheckBoxMenuItem("Edges");
       JCheckBoxMenuItem toggleVertices = new JCheckBoxMenuItem("Vertices", true);
       toggle.add(toggleEdge);
       toggleEdge.getState();
       JRadioButtonMenuItem birdButton = new JRadioButtonMenuItem("birdString");
       birdButton.setMnemonic(KeyEvent.VK_B);
       birdButton.setActionCommand("birdString");
       birdButton.setSelected(true);

       JRadioButtonMenuItem catButton = new JRadioButtonMenuItem("catString");
       catButton.setMnemonic(KeyEvent.VK_C);
       catButton.setActionCommand("catString");

       JRadioButtonMenuItem dogButton = new JRadioButtonMenuItem("dogString");
       dogButton.setMnemonic(KeyEvent.VK_D);
       dogButton.setActionCommand("dogString");

       JRadioButtonMenuItem rabbitButton = new JRadioButtonMenuItem("rabbitString");
       rabbitButton.setMnemonic(KeyEvent.VK_R);
       rabbitButton.setActionCommand("rabbitString");

       JRadioButtonMenuItem pigButton = new JRadioButtonMenuItem("pigString");
       pigButton.setMnemonic(KeyEvent.VK_P);
       pigButton.setActionCommand("pigString");

       //Group the radio buttons.
       ButtonGroup group = new ButtonGroup();
       group.add(birdButton);
       group.add(catButton);
       group.add(dogButton);
       group.add(rabbitButton);
       group.add(pigButton);
		final JToggleButton groupVertices = new JToggleButton("Group Clusters");

       final JSlider edgeBetweennessSlider = new JSlider(JSlider.HORIZONTAL);
       edgeBetweennessSlider.setBackground(Color.WHITE);
		edgeBetweennessSlider.setPreferredSize(new Dimension(210, 50));
		edgeBetweennessSlider.setPaintTicks(true);
		edgeBetweennessSlider.setMaximum(g.getEdgeCount());
		edgeBetweennessSlider.setMinimum(0);
		edgeBetweennessSlider.setValue(0);
		edgeBetweennessSlider.setMajorTickSpacing(10);
		edgeBetweennessSlider.setPaintLabels(true);
		edgeBetweennessSlider.setPaintTicks(true);
		final JPanel eastControls = new JPanel();
		eastControls.setOpaque(true);
		eastControls.setLayout(new BoxLayout(eastControls, BoxLayout.Y_AXIS));
		eastControls.add(Box.createVerticalGlue());
		eastControls.add(edgeBetweennessSlider);
		final String COMMANDSTRING = "Edges removed for clusters: ";

		final String eastSize = COMMANDSTRING + edgeBetweennessSlider.getValue();

		final TitledBorder sliderBorder = BorderFactory.createTitledBorder(eastSize);
		eastControls.setBorder(sliderBorder);
		//eastControls.add(eastSize);
		eastControls.add(Box.createVerticalGlue());
		JButton scramble = new JButton("Restart");
		scramble.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Layout layout = vv.getGraphLayout();
				layout.initialize();
				Relaxer relaxer = vv.getModel().getRelaxer();
				if(relaxer != null) {
					relaxer.stop();
					relaxer.prerelax();
					relaxer.relax();
				}
			}

		});
		JPanel south = new JPanel();
		JPanel grid = new JPanel(new GridLayout(2,1));
		grid.add(scramble);
		grid.add(groupVertices);
		south.add(grid);
		south.add(eastControls);
		frame.add(south, BorderLayout.SOUTH);
		groupVertices.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
					try {
						clusterAndRecolor(layout, edgeBetweennessSlider.getValue(), 
								similarColors, e.getStateChange() == ItemEvent.SELECTED);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					vv.repaint();
			}});


		clusterAndRecolor(layout, 0, similarColors, groupVertices.isSelected());

		edgeBetweennessSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider) e.getSource();
				if (!source.getValueIsAdjusting()) {
					int numEdgesToRemove = source.getValue();
					try {
						clusterAndRecolor(layout, numEdgesToRemove, similarColors,
								groupVertices.isSelected());
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					sliderBorder.setTitle(
						COMMANDSTRING + edgeBetweennessSlider.getValue());
					eastControls.repaint();
					vv.validate();
					vv.repaint();
				}
			}
		});
       
       ActionListener viewMenuListener = new ActionListener() {
     	  
 	      public void actionPerformed(ActionEvent event) {
 	    	//  System.out.println(event.getActionCommand());
 	    	  if (event.getActionCommand()=="catString"){
					 //layout = new FRLayout(sgv.g);	
					// vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT);
					 layout = new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new FRLayout<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g));
					 vv.setGraphLayout(layout);
					
					 try {
							clusterAndRecolor(layout, edgeBetweennessSlider.getValue(), 
										similarColors, groupVertices.isSelected());
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							

							e.printStackTrace();
						}
					 MutableTransformer layout2 = vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT);    	          
						vv.updateUI();

					 Point2D tmp = vv.getCenter();
					 
					layout2.setTranslate(tmp.getX(), tmp.getY());
					vv.updateUI();
 	    	  }
 	    	  if (event.getActionCommand() == "rabbitString"){
					// layout = new ISOMLayout(sgv.g);	
					// vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT);
					layout = new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new ISOMLayout<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g)); 
 	    		  vv.setGraphLayout(layout);
 	    		 try {
						clusterAndRecolor(layout, edgeBetweennessSlider.getValue(), 
									similarColors, groupVertices.isSelected());
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block

						e.printStackTrace();
					}
 	    		 MutableTransformer layout2 = vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT);    	          
				 Point2D tmp = vv.getCenter();
				layout2.setTranslate(tmp.getX(), tmp.getY());
				vv.updateUI();
 	    	  }
 	    	 if (event.getActionCommand() == "dogString"){
					// layout = new ISOMLayout(sgv.g);	
					// vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT);
 	    		layout = new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new KKLayout<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g)); 
					 vv.setGraphLayout(layout);
					 try {
						clusterAndRecolor(layout, edgeBetweennessSlider.getValue(), 
									similarColors, groupVertices.isSelected());
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block

						e.printStackTrace();
					}
					 MutableTransformer layout2 = vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT);    	          
					 Point2D tmp = vv.getCenter();
					layout2.setTranslate(tmp.getX(), tmp.getY());
					vv.updateUI();
	    	  }
 	    	if (event.getActionCommand() == "pigString"){
				// layout = new ISOMLayout(sgv.g);	
				// vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT);
	    		layout = new AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge>(new FRLayout2<GraphElements.MyVertex, GraphElements.MyEdge>(sgv.g)); 
				
	    		vv.setGraphLayout(layout);
	    		vv.setLocation(0, 0);
				 try {
					clusterAndRecolor(layout, edgeBetweennessSlider.getValue(), 
								similarColors, groupVertices.isSelected());
				} catch (InterruptedException e) {
				//	vv.CENTER_ALIGNMENT;
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 MutableTransformer layout2 = vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.LAYOUT);    	          
				 vv.setAlignmentX(vv.CENTER_ALIGNMENT);
					vv.setAlignmentY(vv.CENTER_ALIGNMENT);
				 Point2D tmp = vv.getCenter();
				layout2.setTranslate(tmp.getX(), tmp.getY());
				
				vv.updateUI();
    	  }
 	      }
 	    };

       //Register a listener for the radio buttons.
       birdButton.addActionListener(viewMenuListener);
       catButton.addActionListener(viewMenuListener);
       dogButton.addActionListener(viewMenuListener);
       rabbitButton.addActionListener(viewMenuListener);
       pigButton.addActionListener(viewMenuListener);
       
      // viewMenu.add(group);
       viewMenu.add(birdButton);
       viewMenu.add(catButton);
       viewMenu.add(dogButton);
       viewMenu.add(rabbitButton);
       viewMenu.add(pigButton);
       
       ActionListener edgeMenuListener = new ActionListener() {
    	  
    	      public void actionPerformed(ActionEvent event) {
    	    	  
    	    //	  toggleEdges();
    	    	  vv.updateUI();
    	        AbstractButton aButton = (AbstractButton) event.getSource();
    	        boolean selected = aButton.getModel().isSelected();
    	        String newLabel;
    	        Icon newIcon;
    	        if (selected) {
					 vv.getRenderContext().setEdgeLabelTransformer(new ToStringLabeller());
				//	 FRlayout = new FRLayout(sgv.g);	
					 
				//	 vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.FRLAYOUT);
					 vv.updateUI();

    	          newLabel = "Edges";
    	        } else {
    	          newLabel = "Edges";
					 vv.getRenderContext().setEdgeLabelTransformer(blankLabelTransformer);
					
					
					//layout2.setTranslate(g.getVertices()., );
					// layout2.translate(tmp.getX(), tmp.getY());
					 vv.updateUI();
    	        }
    	        aButton.setText(newLabel);
    	      }
    	    };
    	    ActionListener VertexMenuListener = new ActionListener() {
    	    	  
      	      public void actionPerformed(ActionEvent event) {
      	    	  
      	    //	  toggleEdges();
      	    	  vv.updateUI();
      	        AbstractButton aButton = (AbstractButton) event.getSource();
      	        boolean selected = aButton.getModel().isSelected();
      	        String newLabel;
      	        Icon newIcon;
      	        if (selected) {
  					 vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
  					 vv.updateUI();
      	          newLabel = "Vertices";
      	        } else {
      	          newLabel = "Vertices";
  					 vv.getRenderContext().setVertexLabelTransformer(blankLabelTransformer);

      	          vv.updateUI();
      	        }
      	        aButton.setText(newLabel);
      	      }
      	    };
       
       toggleEdge.addActionListener(edgeMenuListener);
       toggleVertices.addActionListener(VertexMenuListener);
       toggle.add(toggleVertices);
      // menu.setMnemonic(KeyEvent.VK_A);
       display.add(toggle);
   //    menu.getAccessibleContext().setAccessibleDescription(
            //   "The only menu in this program that has menu items");
       menuBar.add(mainMenu);
       menuBar.add(viewMenu);
       menuBar.add(display);
       //frame.add(pnlButton, BorderLayout.SOUTH);
       EditingModalGraphMouse gm = new EditingModalGraphMouse(vv.getRenderContext(), 
               GraphElements.MyVertexFactory.getInstance(),
              GraphElements.MyEdgeFactory.getInstance()); 
       gm.setMode(ModalGraphMouse.Mode.TRANSFORMING);
       PopupVertexEdgeMenuMousePlugin myPlugin = new PopupVertexEdgeMenuMousePlugin();
       gm.remove(gm.getPopupEditingPlugin());
       gm.add(myPlugin);
       vv.setGraphMouse(gm);
       JPopupMenu edgeMenu = new MyMouseMenus.EdgeMenu(frame);
       JPopupMenu vertexMenu = new MyMouseMenus.VertexMenu();
       myPlugin.setEdgePopup(edgeMenu);
       myPlugin.setVertexPopup(vertexMenu);
       vv.getRenderContext().setVertexFillPaintTransformer(MapTransformer.<GraphElements.MyVertex,Paint>getInstance(vertexPaints));
		vv.getRenderContext().setVertexDrawPaintTransformer(new Transformer<GraphElements.MyVertex,Paint>() {
			public Paint transform(GraphElements.MyVertex v) {
				if(vv.getPickedVertexState().isPicked(v)) {
					return Color.cyan;
				} else {
					return Color.BLACK;
				}
			}
		});

		vv.getRenderContext().setEdgeDrawPaintTransformer(MapTransformer.<GraphElements.MyEdge,Paint>getInstance(edgePaints));

		vv.getRenderContext().setEdgeStrokeTransformer(new Transformer<GraphElements.MyEdge,Stroke>() {
               protected final Stroke THIN = new BasicStroke(1);
               protected final Stroke THICK= new BasicStroke(2);
               public Stroke transform(GraphElements.MyEdge e)
               {
                   Paint c = edgePaints.get(e);
                   if (c == Color.LIGHT_GRAY)
                       return THIN;
                   else 
                       return THICK;
               }
           });
       JMenu modeMenu = gm.getModeMenu();
       modeMenu.setText("Mouse Mode");
       menuBar.add(modeMenu);
       frame.setJMenuBar(menuBar);
       modeMenu.setPreferredSize(new Dimension(120,20));
       gm.setMode(ModalGraphMouse.Mode.TRANSFORMING);
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       frame.getContentPane().add(vv);
     //  BetweennessCentrality ranker = new BetweennessCentrality(g);
       //ranker.evaluate();
       
   //    EdgeBetweennessClusterer<>
       int numberOfEdgesToRemove = 0;
      // EdgeBetweennessClusterer bc = new EdgeBetweennessClusterer(numberOfEdgesToRemove);
     //  Set clusterSet = bc.transform(g);
    //   List edges = bc.getEdgesRemoved();
       //EdgeBetweennessClusterer<g.getEdges(),g>  bc = new EdgeBetweennessClusterer<g.getEdges(),g.getVertices()>(1);
     //  Set<Set<MyNode>> sets = EBC1.transform(g);
    //   ranker.printRankings(true,true);
     //  System.out.println(vv.size());
       //frame.add(menuBar, BorderLayout.NORTH);
       frame.pack();
       frame.setVisible(true);     
       
      
       
       
    }
    
    public static void clusterAndRecolor(AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge> layout,
    		int numEdgesToRemove,
    		Color[] colors, boolean groupClusters) throws InterruptedException {
    //	System.out.println("GROUP AND CLUSTER!");
    		//Now cluster the vertices by removing the top 50 edges with highest betweenness
    		//		if (numEdgesToRemove == 0) {
    		//			colorCluster( g.getVertices(), colors[0] );
    		//		} else {
    		
    		Graph<GraphElements.MyVertex, GraphElements.MyEdge> g = layout.getGraph();
            layout.removeAll();
            vv.repaint();
            vv.updateUI();
     //       Thread.sleep(1910);
    		EdgeBetweennessClusterer<GraphElements.MyVertex, GraphElements.MyEdge> clusterer =
    			new EdgeBetweennessClusterer<GraphElements.MyVertex, GraphElements.MyEdge>(numEdgesToRemove);
    		Set<Set<GraphElements.MyVertex>> clusterSet = clusterer.transform(g);
    		List<GraphElements.MyEdge> edges = clusterer.getEdgesRemoved();

    		int i = 0;
    		//Set the colors of each node so that each cluster's vertices have the same color
    		for (Iterator<Set<GraphElements.MyVertex>> cIt = clusterSet.iterator(); cIt.hasNext();) {

    			Set<GraphElements.MyVertex> vertices = cIt.next();
    			Color c = colors[i % colors.length];

    			colorCluster(vertices, c);
    			if(groupClusters == true) {
    				groupCluster(layout, vertices);
    			}
    			i++;
    		}
    		for (GraphElements.MyEdge e : g.getEdges()) {

    			if (edges.contains(e)) {
    				edgePaints.put(e, Color.lightGray);
    			} else {
    				edgePaints.put(e, Color.black);
    			}
    		}

    	}

    	private static void colorCluster(Set<GraphElements.MyVertex> vertices, Color c) {
    		//System.out.println(vertexPaints);
    		//System.out.println("COLOURING");
    		for (GraphElements.MyVertex v : vertices) {
    			vertexPaints.put(v, c);
    			//System.out.println(v.intValue()+", "+c.toString());
    		}
    	//	System.out.println(vertexPaints);
    		vv.repaint();
    	}
    	
    	private static void groupCluster(AggregateLayout<GraphElements.MyVertex, GraphElements.MyEdge> layout, Set<GraphElements.MyVertex> vertices) {
    	//	System.out.println("GROUPING");
    		if(vertices.size() < layout.getGraph().getVertexCount()) {
    			Point2D center = layout.transform(vertices.iterator().next());
    			Graph<GraphElements.MyVertex, GraphElements.MyEdge> subGraph = SparseMultigraph.<GraphElements.MyVertex, GraphElements.MyEdge>getFactory().create();
    			for(GraphElements.MyVertex v : vertices) {
    				subGraph.addVertex(v);
    			}
    			Layout<GraphElements.MyVertex, GraphElements.MyEdge> subLayout = 
    				new CircleLayout<GraphElements.MyVertex, GraphElements.MyEdge>(subGraph);
    			subLayout.setInitializer(vv.getGraphLayout());
    			subLayout.setSize(new Dimension(40,40));

    			layout.put(subLayout,center);
    			vv.repaint();
    		}
    	}

    
}