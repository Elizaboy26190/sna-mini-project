import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
//import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
//boo
public class Test {
	static Map<String, ArrayList<ArrayList<String>>> commsMap = new HashMap<String,ArrayList<ArrayList<String>>>();
	static SortedSet<String> AllRecipients =  new TreeSet<String>();
	//static SortedSet<String> complete =  new TreeSet<String>();
	static List<String> completeList = new ArrayList<String>();
	static Set<String> senders = commsMap.keySet();

	public static void main(String[] args) throws IOException {
	

	addEntries(6);
	//System.out.println(commsMap);
	completeList.addAll(senders);
	completeList.addAll(AllRecipients);
	Collections.sort(completeList);
//	int boo = 0;
	//for (String s : completeList){
	//	System.out.println(boo+" = "+s);
	//	boo++;
	//}
	totalCounts();
	System.out.println("Done ...");
	}


private static void totalCounts() throws IOException {		
	int completeSize = completeList.size();	
	String[][] matrix = new String[completeSize+1][completeSize+1];
	matrix[0][0] = "";
		
		for (int k = 0; k < completeSize;k++){
			matrix[k+1][0] = completeList.get(k);
			matrix[0][k+1] = completeList.get(k);
		}

	
	
		for (String stri: commsMap.keySet()){
			ArrayList<ArrayList<String>> st = commsMap.get(stri);
			
			for (int index = 0;index < 3; index++){
				ArrayList<String> toList = st.get(index);
				int len = toList.size();
				for (int l = 0; l < len; l++){
					String rec = toList.get(l);
					
					if(rec.length()!=0){
						int yIndex = completeList.indexOf(rec);
						int xIndex = completeList.indexOf(stri);
						String tmp =  matrix[xIndex+1][yIndex+1];
						
						if (tmp == null){
							tmp = Integer.toString(3 - (index));
							}
						else {				 
							//System.out.println("tmp = '"+tmp+"'");
							int tmpInt = Integer.parseInt(tmp);
							tmp = Integer.toString(tmpInt +3 -(index));
							}
						matrix[xIndex+1][yIndex+1] = tmp;
						}
					}
				}
			}
	//	System.out.println(Arrays.deepToString(matrix));
		
		String dir = "AdjMatrix.csv";
		FileWriter fw = new FileWriter(dir,false);
		fw.flush();
		for (int i = 0; i <completeList.size(); i++){
			
			for (int j = 0; j < completeList.size();j++){
				if (matrix[i][j]!=null){
					fw.write(matrix[i][j]);
				}
				else{
					fw.write(Integer.toString(0));
				}
				fw.write(",");			

			}
			if(matrix[i][completeSize]!=null){
			fw.write(matrix[i][completeSize]);
			}
			else{
				fw.write(Integer.toString(0));
			}
			fw.write("\n");
			
		}
		for (int j = 0; j < completeSize;j++){
			if(matrix[completeSize][j]!= null){
				fw.write(matrix[completeSize][j]);
			}
			else{
				fw.write(Integer.toString(0));
			}
			fw.write(",");
		}
		if(matrix[completeSize][completeSize]!=null){
		fw.write(matrix[completeSize][completeSize]);
		}
		else{
			fw.write(Integer.toString(0));
		}
		fw.close();	
		}

private static void addEntries(int i) {
	
	for (int j = 1; j<=i; j++){
		ArrayList<String> to = new ArrayList<String>();
		ArrayList<String> cc = new ArrayList<String>();
		ArrayList<String> bcc = new ArrayList<String>();
		ArrayList<ArrayList<String>> comms = new ArrayList<ArrayList<String>>();
	System.out.println();
	System.out.println("calculating for sender"+j);
	//	comms.clear();
		to.clear();
		cc.clear();
		bcc.clear();
		
		to.add("Alfie"+j);
		to.add("Bob"+j);
		to.add("Claire"+j);
	
		cc.add("David"+j);
		cc.add("Eve"+j);
		cc.add("Fred"+j);
		
		bcc.add("Geoff"+j);
		bcc.add("Harry"+j);
		bcc.add("Isabel"+j);
		
		if (j==1){
			to.add("Extra1");
			to.add("Extra3");
			to.add("Extra2");
			cc.add("extra4");
			cc.add("extra1");
			bcc.add("extra3");
			bcc.add("extra2");
			bcc.add("extra1");
		}
		if (j==2){
			cc.add("extra2");
			cc.add("extra5");
			cc.add("extra2");
			cc.add("extra4");
			bcc.add("extra1");
			bcc.add("extra6");
			to.add("extra3");
		}
		
		if (j==3){
			cc.add("extra3");
			cc.add("extra2");
			bcc.add("extra6");
			bcc.add("extra1");
			bcc.add("extra5");
			bcc.add("extra2");
			to.add("extra2");
			to.add("extra4");
			to.add("extra3");
			to.add("extra6");
		}
		
		if (j==4){
			to.add("extra4");
			to.add("extra5");
			cc.add("extra2");
			cc.add("extra1");
			cc.add("extra1");
			cc.add("extra2");
			cc.add("extra4");
			bcc.add("extra2");
			bcc.add("extra3");
			bcc.add("extra5");
			bcc.add("extra2");
			bcc.add("extra1");
		}
		
		if (j==5){
			to.add("extra6");
			to.add("extra2");
			cc.add("extra3");
			to.add("extra1");
			to.add("extra4");
			to.add("extra6");
			bcc.add("extra2");
			bcc.add("extra1");
			bcc.add("extra3");
			bcc.add("extra2");
			
		}
		
		AllRecipients.addAll(to);
	//	System.out.println(AllRecipients);
		AllRecipients.addAll(cc);
	//	System.out.println(AllRecipients);
		AllRecipients.addAll(bcc);
		//System.out.println(AllRecipients);
		
		System.out.print("  Printing to ...");
				System.out.println(to);

		System.out.print("  Printing cc ...");
		System.out.println(cc);
		
		System.out.print("  Printing bcc ...");
		System.out.println(bcc);
		
		comms.clear();
		comms.add(to);
		comms.add(cc);
		comms.add(bcc);		

		System.out.print("  Printing comms for sender"+j+" ...");
		System.out.println("  "+comms);
		commsMap.put("sender"+j,comms);

		}
	}
}